<?php

header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0');

require_once('class/Config.php');

?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Комплектация</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="application/javascript" src="js/audio.js"></script>
    <script type="application/javascript" src="js/cell.js"></script>
    <script type="application/javascript" src="js/product.js"></script>
    <script type="application/javascript" src="js/print.js"></script>
    <script type="application/javascript" src="js/rtc.js"></script>
    <script type="application/javascript" src="js/task.js"></script>
    <script type="application/javascript">
let CurrentStorekeeper

try {
    CurrentStorekeeper = JSON.parse(localStorage.getItem("CurrentStorekeeper"));
}
catch (Exception) {
    location = "aaa.php";
}

if (! CurrentStorekeeper) {
    location = "aaa.php";
}

// Переместить в состояние
let CurrentPicking

//todo: Кладовщик выполняет задания и эти задания можно будет передавать другим.
// Не реализовано
let currentTask = {
    id: '',
    dateOfCreation: '',
    dateOfChange: '',
    operation: 'picking',
    data: {},
    executor: {},
    status: ''
}

//todo: Сохранять текущее состояние в одной переменной.
// Не реализовано
let currentState = {
    order: {},
    product: {},
    sourceCell: {},
    picking: {}
}

const OPERATION_SCAN_ORDER = 'сканирование заказа';
const OPERATION_SCAN_PRODUCT = 'сканирование товара';
const OPERATION_SCAN_CELL = 'сканирование ячейки';
const OPERATION_OPEN_CELL = 'открытие ячейки';
const OPERATION_SELECT_CELL = 'выбор ячейки';
const OPERATION_GET_PRODUCTS_FROM_CELL = 'отбор товара из ячейки';
const OPERATION_PACK_CARGO = 'Упаковка товаров';

let currentOperation;

let div_date = document.getElementById('date');
let div_barcode = document.getElementById('orderBarcode');
let div_number = document.getElementById('number');
//let div_items = document.getElementById('items');

let currentOrderBarcode;    // undefined
let CurrentOrder;

let currentOrderItemIndex;
let currentProductStockItemIndex;
let CurrentOrderItem;
let addedQuantityOfProduct = 0; // Количество добавляемого товара до сканирования ячейки

let sourceCellBarcode;
let SourceCell;
let CurrentSourceCell;

//let CurrentSourceCell = {
//    code: "000016",
//    name: "Угол",
//    address: "POL-01-1-01",
//    barcode: "00000015POL01101"
//}

let destinationCellBarcode;
let DestinationCell;
let CurrentDestinationCell = {
    barcode: '00000015OTG01101'
};

let currentProductBarcode;
let CurrentProduct;

let BarcodeInput;

let OrderBlockViewSwitch;
let OrderBlockContent;

let ItemListTableViewSwitch;
let ItemListBlockContent;
let ItemListTable;

let PickingRowQueue = [];
let handlePickingRowQueueInProcess = false;
let pickingCanBeFinished = false;

// Таблица грузовых мест
let PackageSetBlockViewSwith;
let PackageSetBlockContent;
let PackageSetTable;
let TemporaryPackageSet = [];   // При переупаковке сюда копируется изначальный набор мест и в случае отмены он возвращается обратно.

// Инициализация документа
document.addEventListener('DOMContentLoaded', function() {
    rtc_init()
    BarcodeInput = document.getElementById('barcode');

    OrderBlockViewSwitch = document.getElementById('OrderBlockViewSwitch');
    OrderBlockViewSwitch.addEventListener('click', handleOrderBlockViewSwitchClick);
    OrderBlockContent = document.getElementById('OrderBlockContent');

    ItemListTableViewSwitch = document.getElementById('ItemListTableViewSwitch');
    ItemListTableViewSwitch.addEventListener('click', handleItemListTableViewSwitchClick);
    ItemListBlockContent = document.getElementById('ItemListBlockContent');
    ItemListTable = document.getElementById('ItemListTable');

    PackageSetBlockViewSwith = document.getElementById('PackageSetBlockViewSwith');
    PackageSetBlockViewSwith.addEventListener('click', handlePackageSetBlockViewSwithClick);
    PackageSetBlockContent = document.getElementById('PackageSetBlockContent');
    PackageSetTable = document.getElementById('PackageSetTable');

    let input_barcode = document.getElementById('barcode');
    input_barcode.focus();

    //logMainVariables();
    document.getElementById('cancel').addEventListener('click', handleCancelClick);
    document.getElementById('startPicking').addEventListener('click', handleStartPickingClick);
    document.getElementById('repack').addEventListener('click', handleRepackClick);
    document.getElementById('printOrderLabelSet').addEventListener('click', handlePrintOrderLabelClick);
    document.getElementById('finishPicking').addEventListener('click', function (event) {
        event.preventDefault();
        event.target.style.display = 'none';
        remote_finishPickingForOrder(CurrentOrder.barcode, CurrentStorekeeper.barcode);
    });
    document.getElementById('cancelPicking').addEventListener('click', function (event) {
        event.preventDefault();

        event.target.style.display = 'none';
        //hideOrderBlock();
        hideOrderBlockContent();
        hideItemListBlockContent();
        hidePackageSetBlockContent();

        document.getElementById('cancelPickingBlock').style.display = 'block'
    });

    document.getElementById('cancelWithReasonNoProduct').addEventListener('click', function (event) {
        event.preventDefault();

        remote_cancelPickingForOrder(CurrentOrder.barcode, CurrentStorekeeper.barcode);

        document.getElementById('cancelPickingBlock').style.display = 'none';
        //remote_cancelPickingForOrder(CurrentOrder.barcode, CurrentStorekeeper.barcode);
    });

    document.getElementById('cancelCancelling').addEventListener('click', function (event) {
        event.preventDefault();

        document.getElementById('cancelPickingBlock').style.display = 'none';
        handleOrder(CurrentOrder);
    });

    CurrentOperation = getCurrentOperation()
    CurrentOrder = getCurrentOrder()
    CurrentPicking = getCurrentPicking()
    if (! CurrentPicking) {
        CurrentPicking = {
            barcode: ''
        }
    }

    let candidateOrderBarcode = localStorage.getItem("picking_candidateOrderBarcode")

    if (! candidateOrderBarcode) {
        try {
            let urlParams = new URLSearchParams(location.search)
            candidateOrderBarcode = urlParams.get('orderBarcode')
        }
        catch (ex) {
            showError(ex.message)
            //logToDiv('candidateOrderBarcode = ' + (candidateOrderBarcode ? candidateOrderBarcode : ''))
        }
    }

    if (CurrentOrder) {
        if (
            candidateOrderBarcode
            && candidateOrderBarcode !== CurrentOrder.barcode
        ) {
            showError('Вы ещё комплектуете заказ ' + CurrentOrder.number + '.')
        }

        //console.log("Заказ инициирован.", CurrentOrder);
        showOrderBlock();
        handleOrder(CurrentOrder);

        currentOrderItemIndex = getCurrentOrderItemIndex();
        if (currentOrderItemIndex != undefined) {
            CurrentOrderItem = CurrentOrder.Items[currentOrderItemIndex];
        }

        //showOrderItemListPickingElements();

        //console.log('CurrentOrderItem', CurrentOrderItem); // номер индекса или null

        if (CurrentOrderItem) {
            if (CurrentOrderItem.Product.type == "good") {
                currentProductStockItemIndex = getCurrentProductStockItemIndex();

                //console.log('currentProductStockItemIndex', currentProductStockItemIndex); // номер индекса или null
                if (currentProductStockItemIndex === -1) {
                    CurrentSourceCell = getDefaultCell()
                }
                else if (currentProductStockItemIndex != undefined) {
                    CurrentSourceCell = CurrentOrderItem.Product.StockItems[currentProductStockItemIndex].Cell;

                    //console.log('CurrentSourceCell', CurrentSourceCell);
                    highlightCurrentProductStockItemCell();
                }
            }

            handleOrderItemActualQuantityChange();
        }
    }
    else if (candidateOrderBarcode) {
        localStorage.removeItem("picking_candidateOrderBarcode")
        handleBarcode(candidateOrderBarcode)
    }
});

// ЭТА ФУНКЦИЯ НУЖНА ПРИЛОЖЕНИЮ ДЛЯ АНДРОИД
function send() {
    //console.log(this.event.keyCode);
    if (this.event.keyCode !== 13) {
        return false;
    }

    let input_barcode = document.getElementById("barcode");

    let barcode = (input_barcode.value).trim(); // Например, 19062300001
    input_barcode.value = '';

    handleBarcode(barcode);
}

let barcodeHandlingLocked = false;

const lockBarcodeHandling = function () {
    hideError();
    barcodeHandlingLocked = true;
    document.getElementById('standbyIndicator').style.display = 'block';
}

const unlockBarcodeHandling = function () {
    document.getElementById('standbyIndicator').style.display = 'none';
    //hideError();
    barcodeHandlingLocked = false;
}

function handleBarcode(barcode) {
    console.log(currentOperation);

    // TEST
    //currentOperation = OPERATION_PACK_CARGO;

    hideError();

    if (barcodeHandlingLocked)
        showError('Ввод временно заблокирован');

    if (currentOperation == OPERATION_SCAN_ORDER) {
        findOrderByBarcode(barcode);
    }
    else if (currentOperation == OPERATION_SCAN_PRODUCT) {
        //findProductByBarcode();
        findProductInOrderByBarcode(CurrentOrder, barcode);
    }
    else if (currentOperation == OPERATION_SCAN_CELL) {
        console.log('Scan cell', CurrentOrderItem.Product, barcode);
        findCellInProductByBarcode(CurrentOrderItem.Product, barcode);
    }
    else if (currentOperation == OPERATION_PACK_CARGO) {
        barcode = barcode.toLowerCase();
        if (
            barcode == 'add row'
            || barcode == 'фвв кщц'
        ) {
            //let packageWeightInput = document.getElementById('packageWeight');
            //packageWeightInput.value = "";
            //packageWeightInput.style.display = 'inline';
            ////packageWeightInput.focus();
            //packageWeightInput.select();

            const callback = weight => {
                if (weight <= 0) {
                    weight = 0
                }

                let packageBarcode;

                if (CurrentOrder.Contractor.name == 'Москва') {
                    //packageBarcode = CurrentOrder.barcode + (CurrentOrder.PackageSet.length + 1).toString().padStart(3, '0');
                    packageBarcode = '' + CurrentOrder.barcode + addZero(3, CurrentOrder.PackageSet.length + 1);
                }
                else {
                    packageBarcode = CurrentOrder.barcode
                }

                let Package = {
                    number: CurrentOrder.PackageSet.length + 1,
                    barcode: '' + packageBarcode,
                    weight: weight,
                    length: 0,
                    width: 0,
                    height: 0
                }

                CurrentOrder.PackageSet.push(Package);

                //populateOrderPackageSetTable(CurrentOrder);
                if (Package.weight <= 0) {
                    Package.weight = 0
                    addPackageToPackageSetTable(Package, true)
                }
                else {
                    addPackageToPackageSetTable(Package, false)
                }

                updatePackageSetCount()
                setCurrentOrder(CurrentOrder)
            }

            remote_getWeightFromScales(callback)
        }
        else if (
            barcode == 'confirm'
            || barcode == 'сщташкь'
        ) {
            if (CurrentOrder.PackageSet.length == 0) {
                showError('Добавьте хотя бы одно грузовое место.');
                return;
            }

            document.getElementById('cancel').style.display = 'none';
            remote_updateOrderPackageSet(CurrentOrder.barcode, CurrentStorekeeper.barcode, CurrentOrder.PackageSet);
        }
        else {
            showError("Отсканируйте штрихкод добавления места или завершения комплектации.");
        }
    }

    //logMainVariables();
}

const handleWeightFromScales = (weight, Package) => {

}

const remote_getWeightFromScales = callback => {
    let configString = localStorage.getItem('config')

    if (! configString)
        return

    let config = JSON.parse(configString)

    if (! config.scalesServerUrl) {
        //todo: убрать КОСТЫЛЬ
        config.scalesServerUrl = 'https://192.168.4.35'
    }

    try {
        lockBarcodeHandling()

        var XHR = new XMLHttpRequest();
        XHR.open('GET', `${config.scalesServerUrl}/?type=json`)
        XHR.send()
        XHR.onload = function () {
            unlockBarcodeHandling()

            if (XHR.status != 200) {
                showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText)
                callback(0)
                return
            }

            var Response = JSON.parse(XHR.responseText)

            if (
                Response.status
                && Response.status !== "ok"
            ) {
                showError(Response.error)
                callback(0)
                return;
            }

            callback(Response.weight)
            //inputEl.value = Response.weight
        };
        XHR.onerror = function (event) {
            unlockBarcodeHandling()

            console.log('При получении веса произошла ошибка:<br />')
            console.log(event.target.status)
            callback(0)
            showError(event.target.status)
        };
    }
    catch (Exception) {
        unlockBarcodeHandling()

        callback(0)
        showError(Exception.message);
    }
}

const addZero = function (digitsLength, source) {
    let text = source + '';
    while(text.length < digitsLength)
        text = '0' + text;

    return text;
};

const remote_updateOrderPackageSet = function (orderBarcode, storekeeperBarcode, PackageSet) {
    console.log("Посылаю запрос на обновление списка грузовых мест.");

    lockBarcodeHandling();

    //let postData = {
    //    'orderBarcode': CurrentOrder.barcode,
    //    'pickerBarcode': CurrentStorekeeper.barcode
    //};

    let postData = {
        'orderBarcode': orderBarcode,
        'storekeeperBarcode': storekeeperBarcode,
        'PackageSet': PackageSet
    };

    let XHR = new XMLHttpRequest();

    XHR.open('POST', '/api/json/picking.php?method=updateOrderPackageSet');
    XHR.send(JSON.stringify(postData));
    XHR.onload = function () {
        unlockBarcodeHandling()

        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText)
        }
        else {
            let Response = JSON.parse(XHR.responseText);

            console.log(Response);

            if (Response.code !== "ok") {
                showError(Response.description);
                return;
            }

            console.log("Упаковка завершена.");

            // Печать только из рабочей базы
            if (location.host !== 'esb')
                printOrderLabel(CurrentOrder)

            setCurrentOperation(OPERATION_SCAN_ORDER)
            hideOrderBlock()
            showOrderBlockContent()
            showItemListBlockContent()

            removeCurrentOperation()
            removeCurrentOrder()

            popTaskFromStack()

            let success = document.getElementById('success')
            success.style.display = 'block'
            setTimeout(() => {
                success.style.display = 'none'
                cleanForm()
                history.back()
            }, 1000)
        }
    };
    XHR.onerror = function (event) {
        unlockBarcodeHandling();
        showError('Ошибка сети. Подтвердите заново,');
        console.log("ОШИБКА завершения этапа упаковки.");
    }
};

const updatePackageSetCount = function () {
    let PackageSetCount = document.getElementById('PackageSetCount');

    if (CurrentOrder.PackageSet)
        PackageSetCount.innerHTML = CurrentOrder.PackageSet.length;
    else
        PackageSetCount.innerHTML = 0;
};

function logMainVariables() {
    console.log('CurrentOrder', CurrentOrder);
    console.log('CurrentOrderItem', CurrentOrderItem);
    console.log('CurrentSourceCell', CurrentSourceCell);
}

function cleanForm() {
    let div_date = document.getElementById('date');
    div_date.innerHTML = '';

    let div_barcode = document.getElementById('orderBarcode');
    div_barcode.innerHTML = '';

    let div_number = document.getElementById('number');
    div_number.innerHTML = '';

    //var div_items = document.getElementById('items');
    //div_items.innerHTML = '';

    let input_barcode = document.getElementById('barcode');
    input_barcode.value = '';
    input_barcode.focus();

    for (let i = ItemListTable.rows.length - 1; i >= 0; i--) {
        ItemListTable.deleteRow(i);
    }

    for (let i = PackageSetTable.rows.length - 1; i >= 0; i--) {
        PackageSetTable.deleteRow(i);
    }

    //removeCurrentOrder()
    //removeCurrentOperation()
}

function showError(message) {
    let element = document.getElementById('error');

    playErrorBeep();

    element.innerHTML = message;
    element.style.display = 'block';
}

function hideError() {
    let element = document.getElementById('error');

    element.style.display = 'none';
    element.innerHTML = '';
}

function showOrderBlock() {
    document.getElementById("OrderBlock").style.display = "block";
}

function hideOrderBlock() {
    document.getElementById("OrderBlock").style.display = "none";
}

function findOrderByBarcode(barcode) {
    if (barcode === '') {
        showError('Укажите номер заказа');

        return;
    }

    lockBarcodeHandling();

    let XHR = new XMLHttpRequest();

    XHR.open('GET', 'api/json/order.php?barcode=' + barcode);
    XHR.send();
    XHR.onload = function () {
        unlockBarcodeHandling();

        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);

            return;
        }

        let Response = JSON.parse(XHR.responseText);

        console.log(Response);

        if (Response.code !== 'ok') {
            showError(Response.description);

            return;
        }

        CurrentOrder = Response.Order;

        for (let i = CurrentOrder.Items.length - 1; i >= 0; i--) {
            if (CurrentOrder.Items[i].Product.type == 'service') {
                CurrentOrder.Items.splice(i, 1);

                continue;
            }

            CurrentOrder.Items[i].plannedQuantity = CurrentOrder.Items[i].actualQuantity;
            CurrentOrder.Items[i].actualQuantity = 0;
        }

        if (CurrentOrder.Items.length == 0) {
            showError('У заказа нет ни товаров, ни документов. Нечего собирать.');

            return;
        }

        // Заказ надо сохранять только если его начали комплектовать
        //setCurrentOrder(CurrentOrder);

        handleOrder(CurrentOrder);
    };
    XHR.onerror = function (event) {
        unlockBarcodeHandling();
        console.log('Ошибка при получении заказа.');
    };
}

const populateOrderBlockContent = function (Order) {
    let div_date = document.getElementById('date');
    let div_barcode = document.getElementById('orderBarcode');
    let div_number = document.getElementById('number');
    let elem_customerName = document.getElementById('customerName');
    let elem_contractorName = document.getElementById('contractorName');
    let div_type = document.getElementById('type')
    let div_status = document.getElementById('status')
    let commentEl = document.getElementById('comment')

    div_date.innerHTML = Order.date;
    div_number.innerHTML = Order.number;
    div_barcode.innerHTML = Order.barcode;
    elem_customerName.innerHTML = Order.Customer.name;
    elem_contractorName.innerHTML = Order.Contractor.name;
    div_type.innerHTML = Order.type;
    div_status.innerHTML = Order.status + (Order.substatus ? ' (' + Order.substatus + ')' : '')
    commentEl.innerHTML = Order.comment ? Order.comment : 'Нет комментария.'

    if ((Order.status).includes("ОТМЕНЁН")) {
        div_status.style.backgroundColor = "red";
        div_status.style.color = "white";
    }
    else if (
        Order.substatus == 'принят'
        || Order.substatus == 'комплектация'
    ) {
        div_status.style.backgroundColor = '#64ff64';
    }
    else if (Order.substatus == 'собран') {
        div_status.style.backgroundColor = '#ffff64';
    }
    else {
        div_status.style.backgroundColor = "white";
        div_status.style.color = "black";
    }
};

const populateOrderItemListTable = function (Order) {
    for (let i = 0; i < Order.Items.length; i++) {
        let Item = Order.Items[i];

        let productId = 'product_' + Item.Product.barcode;

        let ItemTr = document.createElement('tr');
        ItemTr.id = productId;

        let ProductTd = document.createElement('td');
        ProductTd.appendChild(document.createTextNode(Item.Product.name));
        ProductTd.appendChild(document.createElement('br'));

        if (Item.Product.StockItems) {
            let StockItemListTable = document.createElement('table');
            StockItemListTable.classList.add('elementForPicking');

            for (let y = 0; y < Item.Product.StockItems.length; y++) {
                let StockItem = Item.Product.StockItems[y];

                let cellId = 'cell_' + StockItem.Cell.barcode;

                let StockItemTr = document.createElement('tr');
                StockItemTr.id = productId + '_' + cellId;
                StockItemTr.style.backgroundColor = '#eee';

                let CellNameTd = document.createElement('td');
                CellNameTd.innerHTML = StockItem.Cell.name;
                StockItemTr.appendChild(CellNameTd);

                let CellQuantityTd = document.createElement('td');
                CellQuantityTd.innerHTML = StockItem.quantity;
                StockItemTr.appendChild(CellQuantityTd);

                StockItemListTable.appendChild(StockItemTr);
            }

            ProductTd.appendChild(StockItemListTable);
        }

        ItemTr.appendChild(ProductTd);

        let bgcolor = undefined;
        if (Item.actualQuantity == 0) {
            bgcolor = '#f00';
        }
        else if (Item.actualQuantity < Item.plannedQuantity) {
            bgcolor = '#ff0';
        }
        else {
            bgcolor = '#0f0';
        }

        let ItemPlannedQuantityTd = document.createElement('td');
        ItemPlannedQuantityTd.innerHTML = Item.plannedQuantity;

        ItemTr.appendChild(ItemPlannedQuantityTd);

        let ItemActualQuantityTd = document.createElement('td');
        ItemActualQuantityTd.className +=" elementForPicking";
        //ItemActualQuantityTd.classList.add('elementForPicking');  // это не во всех браузерах работает
        ItemActualQuantityTd.style.backgroundColor = bgcolor;

        //todo: ПЕРЕДЕЛАТЬ!!!!
        ItemActualQuantityTd.innerHTML = '<input id="' + productId + '_actual" value="' + Item.actualQuantity + '" size="3" maxlength="3" class="actual_quantity" data-product-id="' + Item.Product.barcode + '" disabled />';

        ItemTr.appendChild(ItemActualQuantityTd);

        ItemListTable.appendChild(ItemTr);
    }

    let elements = document.querySelectorAll(".actual_quantity");
    for (let i = 0; i < elements.length; i++) {
        elements[i].onchange = function(ev) {
            console.log('Ручная установка количества товара.');
            console.log('ACTUAL: ', ev.target.dataset.productId);
            console.log('ACTUAL: ', ev.target.value);
            console.log('ACTUAL typeof: ', typeof(ev.target.value));

            manualSetActualQuantity(parseInt(ev.target.value));
        };
    }
};

function handleOrder(Order) {
    console.log('Обработка заказа');
    cleanForm();

    populateOrderBlockContent(Order);
    populateOrderItemListTable(Order);
    populateOrderPackageSetTable(Order);

    currentOperation = getCurrentOperation();

    if (currentOperation == OPERATION_SCAN_ORDER) {
        if (canStartPicking()) {
            document.getElementById('startPicking').style.display = 'inline-block';
        }
        else
            document.getElementById('startPicking').style.display = 'none';

        //if (canRepack()) {
        //    document.getElementById('startPicking').style.display = 'inline-block';
        //}

        if (Order.substatus == "собран") {
            document.getElementById('repack').style.display = 'inline-block';
            document.getElementById('printOrderLabelSet').style.display = 'inline-block';
        }
        else {
            document.getElementById('repack').style.display = 'none';
            document.getElementById('printOrderLabelSet').style.display = 'none';
        }

        showOrderBlockContent();
        showItemListBlockContent();
        showOrderBlock();
    }
    else if (currentOperation == OPERATION_PACK_CARGO) {
        startPacking();
    }
    else {
        showOrderBlock()
        startPicking()
    }
}

function manualSetActualQuantity(quantity) {
    hideError();

    if (! CurrentOrderItem) {
        showError('Не указан текущий товар.');

        return;
    }

    if (typeof(quantity) == 'string') {
        quantity = parseInt(quantity);
    }

    let elemActualQuantityInput = document.querySelector('#product_' + CurrentOrderItem.Product.barcode + ' input');

    if (quantity < 1) {
        showError('Количество единиц товара не может быть меньше нуля.');
        elemActualQuantityInput.value = CurrentOrderItem.actualQuantity;

        return;
    }
    else if (quantity > CurrentOrderItem.plannedQuantity) {
        showError('Вы указываете ' + quantity + ' единиц &quot;' + CurrentOrderItem.Product.name + '&quot; , а нужно ' + CurrentOrderItem.plannedQuantity + '.');
        elemActualQuantityInput.value = CurrentOrderItem.actualQuantity;

        return;
    }
    else {
        CurrentOrderItem.actualQuantity = quantity;
    }

    handleOrderItemActualQuantityChange();
}

function handleOrderItemActualQuantityChange() {
    let elemActualQuantity = document.querySelector('#product_' + CurrentOrderItem.Product.barcode + ' > td:nth-child(3)');
    let elemActualQuantityInput = document.querySelector('#product_' + CurrentOrderItem.Product.barcode + ' input');

    // Прокрутка окна к текущему элементу списка
    let selectedPosX = 0;
    let selectedPosY = 0;

    selectedPosX += elemActualQuantity.offsetLeft;
    selectedPosY += elemActualQuantity.offsetTop;

    window.scrollTo(selectedPosX, selectedPosY);

    if (
        CurrentSourceCell == null
        && CurrentOrderItem.Product.type == 'good'
    ) {
        if (CurrentOrderItem.Product.StockItems.length == 1) {
            currentProductStockItemIndex = 0;
            CurrentSourceCell = CurrentOrderItem.Product.StockItems[0].Cell;
            setCurrentProductStockItemIndex(currentProductStockItemIndex);
            highlightCurrentProductStockItemCell();
        }
        // Если товары лежат в разных ячейках, то надо выбрать ячейку.
        else if (CurrentOrderItem.Product.StockItems.length > 1) {
            setCurrentOperation(OPERATION_SCAN_CELL);
        }
    }

    if (CurrentOrderItem.actualQuantity < CurrentOrderItem.plannedQuantity) {
        elemActualQuantityInput.value = CurrentOrderItem.actualQuantity;
        elemActualQuantityInput.disabled = false;
        elemActualQuantity.style.backgroundColor = '#ff0';
    }
    else if (CurrentOrderItem.actualQuantity == CurrentOrderItem.plannedQuantity) {
        elemActualQuantityInput.value = CurrentOrderItem.actualQuantity;
        elemActualQuantityInput.disabled = true;
        elemActualQuantity.style.backgroundColor = '#0f0';

        if (CurrentOrderItem.Product.type == 'good') {
            removeHighlightCurrentProductStockItemCell();

            let PickingRow = {
                rowNumber: 0,                                           // 0 - для созданных вручную комплектаций
                productBarcode: CurrentOrderItem.Product.barcode,
                orderBarcode: CurrentOrder.barcode,
                sourceCellBarcode: CurrentSourceCell.barcode,
                destinationCellBarcode: CurrentDestinationCell.barcode,
                plannedQuantity: CurrentOrderItem.plannedQuantity,
                actualQuantity: CurrentOrderItem.actualQuantity
            };

            // Помещаем набранную строку в очередь на отправку. Это позволит продолжить отбор товаров, сохранить строку
            // комплектации в случае обрыва связи. Отправлять
            PickingRowQueue.push(PickingRow);

            handlePickingRowQueue();
            //remote_addRowToPicking(CurrentPicking.barcode, CurrentStorekeeper.barcode, PickingRow);
        }

        // Проверить собран ли весь заказ.
        // Если собран, то перейти к упаковке грузовых мест.
        if (currentOrderIsAssembled()) {
            pickingCanBeFinished = true;

            remote_finishPickingForOrder(CurrentOrder.barcode, CurrentStorekeeper.barcode);
        }

        CurrentOrderItem = undefined;
        CurrentSourceCell = undefined;

        removeCurrentOrderItemIndex();
        removeCurrentProductStockItemIndex();
    }

    BarcodeInput.focus();
}

//function handleProduct(Product) {
//    console.log("Начинаю работу с товаром.");
//    console.log(Product);
//    if (! currentProduct) {
//        currentProduct = Product;
//    }
//
//    if (initializationInProgress) {
//        console.log("Инициализация данных продолжается.");
//
//        initCurrentCellItemQuantity();
//
//        return;
//    }
//
//    // Проверяю наличие такого товара в ячейке отправителе.
//    // Если такого товара нет в ячейке отправителе, то тогда показываю ошибку.
//    // Если есть, то проверяю не превышает ли количество отбираемых количество доступных. Такое возможно, если:
//    // - несколько раз отсканировали одну единицу товара
//    // - неправильно вручную указали количество отбираемого товара
//    // Если отбираемое количество превышает доступное, то приравниваю количство отбираемого к количеству доступного и
//    // выдаю ошибку.
//    // Иначе инкрементирую или увеличиваю на указанное количество текущий товар
//
//
//    //TODO: Получать количество товара для ячеек склада, а не только для товаров ячейки кладовщика.
//    if (currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL) {
//        currentCellItemAvailableQuantity = getSourceCellItemAvailableQuantityForProductBarcode(Product.barcode);
//
//        var avaliableCellItem = null;
//        for (var i = 0; i < myCellItems.length; i++) {
//            if (myCellItems[i].barcode == Product.barcode) {
//                avaliableCellItem = myCellItems[i];
//            }
//        }
//
//        if (! avaliableCellItem) {
//            showError("Товара " + Product.name + " у вас на руках нет.");
//
//            return;
//        }
//
//        if (avaliableCellItem.quantity < (currentCellItemQuantity + 1)) {
//            showError("Запрошено единиц товара: " + (currentCellItemQuantity + 1) + ". На руках только: " + avaliableCellItem.quantity + ".");
//
//            return;
//        }
//    }
//
//    if (currentProductBarcode == null) {
//        currentProduct = Product;
//
//        setCurrentProductBarcode(Product.barcode);
//
//        //setView();
//    }
//
//    if (Product.barcode == currentProductBarcode) {
//        currentCellItemQuantity += 1;
//        localStorage.setItem('currentCellItemQuantity', currentCellItemQuantity);
//
//        setView();
//    }
//    else {
//        showError('Это другой товар.');
//    }
//}

function findProductInOrderByBarcode(Order, productBarcode) {
    if (! Order.Items) {
        showError('В заказе нет товаров.');

        return;
    }

    if (
        CurrentOrderItem
        && CurrentOrderItem.Product.barcode != productBarcode
    ) {
        showError('Вы сканируете другой товар, но ещё не набрали ' + CurrentOrderItem.Product.name);

        return;
    }

    var productFound = false;
    for (var i = 0; i < Order.Items.length; i++) {
        let Product = Order.Items[i].Product;
        let OrderItem = Order.Items[i];

        if (OrderItem.Product.barcode == productBarcode) {
            if (OrderItem.actualQuantity == OrderItem.plannedQuantity) {
                showError('Этот товар уже скомплектован.');

                return;
            }

            currentOrderItemIndex = i;
            CurrentOrderItem = OrderItem;
            CurrentProduct = Product;
            //console.log(CurrentProduct);

            setCurrentOrderItemIndex(currentOrderItemIndex)

            productFound = true

            //addedQuantityOfProduct = 1;
            //CurrentOrderItem.actualQuantity += 1;
            //setCurrentOrder(CurrentOrder)

            // КОСТЫЛЬ
            // 2020-06-02 Туманов
            // На складе бардак, поэтому сейчас все товары берём из ячейки по умолчанию.
            if (! CurrentSourceCell) {
                CurrentSourceCell = getDefaultCell()
                currentProductStockItemIndex = -1
                setCurrentProductStockItemIndex(currentProductStockItemIndex)
            }

            // 2020-06-02 Туманов
            // Блокирую до наведения порядка на складе.
            //if (CurrentOrderItem.actualQuantity == 0) {
            //    console.log('Первое сканирование продукта');
            //    if (CurrentOrderItem.Product.type == 'good') {
            //        console.log('Этот продукт является товаром');
            //
            //        //CurrentSourceCell = {
            //        //    code: "000016",
            //        //    name: "Угол",
            //        //    address: "POL-01-1-01",
            //        //    barcode: "00000015POL01101"
            //        //}
            //
            //        // КОСТЫЛЬ
            //        // Некоторые товары не были своевременно приняты и их ещё нет на остатках, хотя их уже собирают.
            //        // Это неправильно, но в данный момент нельзя тормозить процесс.
            //        if (! CurrentOrderItem.Product.StockItems) {
            //            CurrentSourceCell = getDefaultCell()
            //            currentProductStockItemIndex = -1
            //            setCurrentProductStockItemIndex(currentProductStockItemIndex)
            //        }
            //        else if (
            //            CurrentOrderItem.Product.StockItems.length == 1
            //            && CurrentOrderItem.Product.StockItems[0].Cell.barcode != '00000015OTG01101'
            //        ) {
            //            console.log('Товар лежит в одной ячейке и это не ячейка OTG. Выбираю и подсвечиваю ячейку.')
            //            CurrentSourceCell = CurrentOrderItem.Product.StockItems[0].Cell
            //            currentProductStockItemIndex = 0
            //            setCurrentProductStockItemIndex(currentProductStockItemIndex)
            //
            //            highlightCurrentProductStockItemCell()
            //        }
            //        else if (CurrentOrderItem.Product.StockItems.length == 2) {
            //            console.log('Товар лежит в двух ячейках. Если среди них одна это OTG, то берём другую.');
            //            let cellIndex = -1;
            //            if (CurrentOrderItem.Product.StockItems[0].Cell.barcode == '00000015OTG01101')
            //                cellIndex = 1;
            //            else if (CurrentOrderItem.Product.StockItems[1].Cell.barcode == '00000015OTG01101')
            //                cellIndex = 0;
            //            else {
            //                console.log('Ни одна из ячеек не является OTG. Надо сканировать ячейку.');
            //                setCurrentOperation(OPERATION_SCAN_CELL);
            //                return;
            //            }
            //
            //            CurrentSourceCell = CurrentOrderItem.Product.StockItems[cellIndex].Cell;
            //            currentProductStockItemIndex = cellIndex;
            //            setCurrentProductStockItemIndex(currentProductStockItemIndex);
            //
            //            highlightCurrentProductStockItemCell();
            //        }
            //        else {
            //            setCurrentOperation(OPERATION_SCAN_CELL);
            //            return;
            //        }
            //    }
            //}

            CurrentOrderItem.actualQuantity += 1;
            setCurrentOrder(CurrentOrder);
            handleOrderItemActualQuantityChange();

            break;
        }
    }

    if (! productFound) {
        showError('Не могу найти товар с ШК ' + productBarcode + ' в заказе ' + Order.number);
    }
}

// МЕГАКОСТЫЛЬ
const getDefaultCell = () => {
    return {
        code: "000016",
        name: "Угол",
        address: "POL-01-1-01",
        barcode: "00000015POL01101"
    };
}

function currentOrderIsAssembled() {
    let Item;
    for (let i = 0; i < CurrentOrder.Items.length; i++) {
        Item = CurrentOrder.Items[i];
        if (Item.plannedQuantity != Item.actualQuantity) {
            return false;
        }
    }

    return true;
}

function findCellInProductByBarcode(Product, barcode) {
    console.log('findCellInProductByBarcode()', Product);

    if (barcode == '00000015OTG01101') {
        showError("Запрещено комплектовать заказ из ячейки отгрузки.");
        return;
    }

    if (barcode == CurrentOrderItem.Product.barcode) {
        showError('Товар ' + CurrentOrderItem.Product.name + ' лежит в нескольких ячейках. Сейчас отсканируйте ячейку, из которой берёте товар.');
        return;
    }

    if (! CurrentOrderItem.Product.StockItems) {
        showError('У товара ' + CurrentOrderItem.Product.name + ' не указаны ячейки.');
        return;
    }

    let cellFound = false;
    for (let i = 0; i < CurrentOrderItem.Product.StockItems.length; i++) {
        let StockItem = Product.StockItems[i];

        if (StockItem.Cell.barcode == barcode) {
            CurrentSourceCell = StockItem.Cell;

            setCurrentOperation(OPERATION_SCAN_PRODUCT);

            currentProductStockItemIndex = i;
            setCurrentProductStockItemIndex(currentProductStockItemIndex);

            highlightCurrentProductStockItemCell();
            cellFound = true;

            CurrentOrderItem.actualQuantity += 1;
            setCurrentOrder(CurrentOrder);

            break;
        }
    }

    if (! cellFound) {
        showError('Товар ' + Product.name + ' не лежит в ячейке с ШК ' + barcode + '.');
    }
    else {
        handleOrderItemActualQuantityChange();
    }
}

// ВРЕМЕННО БЛОКИРУЮ
const highlightCurrentProductStockItemCell = () => {
    //document.getElementById('product_' + CurrentOrderItem.Product.barcode + '_cell_' + CurrentSourceCell.barcode).style.backgroundColor = '#ff0';
}

// ВРЕМЕННО БЛОКИРУЮ
const removeHighlightCurrentProductStockItemCell = () => {
    //if (! CurrentOrderItem)
    //    console.log('removeHighlightCurrentProductStockItemCell()', "Не указан CurrentOrderItem");
    //else if (! CurrentSourceCell)
    //    console.log('removeHighlightCurrentProductStockItemCell()', "Не указан CurrentSourceCell");
    //else
    //    document.getElementById('product_' + CurrentOrderItem.Product.barcode + '_cell_' + CurrentSourceCell.barcode).style.backgroundColor = '#eee';
}

const getCurrentOperation = function () {
    if (! currentOperation) {
        currentOperation = localStorage.getItem("picking_CurrentOperation");

        if (! currentOperation) {
            currentOperation = OPERATION_SCAN_ORDER;
        }
    }

    return currentOperation;
};

const updateCurrentCommand = function () {
    CommandElement = document.getElementById('currentCommand');

    if (currentOperation == OPERATION_SCAN_ORDER)
        CommandElement.innerHTML = 'Отсканируйте заказ';
    else if (currentOperation == OPERATION_SCAN_PRODUCT)
        CommandElement.innerHTML = 'Отсканируйте товар';
    else if (currentOperation == OPERATION_SCAN_CELL)
        CommandElement.innerHTML = 'Отсканируйте ячейку';
    else if (currentOperation == OPERATION_PACK_CARGO)
        CommandElement.innerHTML = 'Подтвердите заказ или добавьте место';
    else
        CommandElement.innerHTML = currentOperation;
};

const setCurrentOperation = function (operation) {
    localStorage.setItem("picking_CurrentOperation", operation);
    currentOperation = operation;

    updateCurrentCommand();
};

const removeCurrentOperation = function () {
    localStorage.removeItem("picking_CurrentOperation");
    currentOperation = undefined;
};

const getCurrentOrder = function () {
    if (! CurrentOrder)
        CurrentOrder = JSON.parse(localStorage.getItem("picking_CurrentOrder"));
    return CurrentOrder;
};

function setCurrentOrder(Order) {
    localStorage.setItem("picking_CurrentOrder", JSON.stringify(Order));
    CurrentOrder = Order;
}

function removeCurrentOrder() {
    localStorage.removeItem("picking_CurrentOrder");
}

function getCurrentOrderItemIndex() {
    return JSON.parse(localStorage.getItem("picking_currentOrderItemIndex"));
}

function setCurrentOrderItemIndex(index) {
    localStorage.setItem("picking_currentOrderItemIndex", JSON.stringify(index));
}

function removeCurrentOrderItemIndex() {
    localStorage.removeItem("picking_currentOrderItemIndex");
}

function getCurrentProductStockItemIndex() {
    return JSON.parse(localStorage.getItem("picking_currentProductStockItemIndex"));
}

function setCurrentProductStockItemIndex(index) {
    localStorage.setItem("picking_currentProductStockItemIndex", JSON.stringify(index));
}

function removeCurrentProductStockItemIndex() {
    localStorage.removeItem("picking_currentProductStockItemIndex");
}

const getCurrentPicking = () => {
    return JSON.parse(localStorage.getItem("picking_currentPicking"))
}

const setCurrentPicking = picking => {
    localStorage.setItem("picking_currentPicking", JSON.stringify(picking))
}

const removeCurrentPicking = () => {
    localStorage.removeItem("picking_currentPicking")
}

// ГРУЗОВЫЕ МЕСТА

const populateOrderPackageSetTable = function (Order) {
    for (let i = PackageSetTable.rows.length - 1; i >= 0; i--) {
        PackageSetTable.deleteRow(i);
    }

    if (
        Order.PackageSet
        && Order.PackageSet.length > 0
    ) {
        //let PackageSetTable = document.getElementById('PackageSetTable');

        for (let i = 0; i < Order.PackageSet.length; i++) {
            let Package = Order.PackageSet[i];

            addPackageToPackageSetTable(Package);
        }

        updatePackageSetCount();
    }
};

const addPackage = function (Package) {
    CurrentOrder.PackageSet.push(Package);
};

const deletePackage = function (index) {
    //let Table = document.getElementById('PackageSetTable')
    PackageSetTable.deleteRow(index)
    CurrentOrder.PackageSet.splice(index, 1)

    for (let i = index; i < PackageSetTable.rows.length; i++) {
        CurrentOrder.PackageSet[i].number = i + 1
        //CurrentOrder.PackageSet[i].barcode = CurrentOrder.barcode + CurrentOrder.PackageSet[i].number.toString().padStart(3, '0')
        CurrentOrder.PackageSet[i].barcode = CurrentOrder.barcode + addZero(3, CurrentOrder.PackageSet[i].number)
        //packageBarcode = '' + CurrentOrder.barcode + addZero(3, CurrentOrder.PackageSet.length + 1)
        PackageSetTable.rows[i].cells[0].innerHTML = CurrentOrder.PackageSet[i].number
    }

    setCurrentOrder(CurrentOrder)
};

const addPackageToPackageSetTable = function (Package, focused = false) {
    let PackageTr = document.createElement('tr')

    let numberTd = document.createElement('td')
    numberTd.innerHTML = Package.number;
    PackageTr.appendChild(numberTd);

    let weightTd = document.createElement('td');
    let weightInput = document.createElement('input');
    weightInput.setAttribute('type', 'number');
    weightInput.setAttribute('placeholder', 'Укажите вес');
    if (getCurrentOperation() == OPERATION_SCAN_ORDER) {
        weightInput.readOnly = true;
    }
    weightInput.value = Package.weight;
    weightInput.style.width = "70px";
    weightInput.addEventListener('change', function (event) {
        let index = event.target.closest('tr').rowIndex;
        let newWeight = event.target.value;
        let oldWeight = CurrentOrder.PackageSet[index].weight;

        if (newWeight > 0 && newWeight <= 50) {
            CurrentOrder.PackageSet[index].weight = parseFloat(newWeight);
            BarcodeInput.focus();
            setCurrentOrder(CurrentOrder);
        }
        else {
            if (newWeight <= 0)
                showError("Вес не может быть меньше нуля.");
            else if (newWeight > 50)
                showError("У нас вес не может быть больше 50 кг.");

            CurrentOrder.PackageSet[index].weight = oldWeight;
            event.target.select();
        }
    })
    weightTd.appendChild(weightInput);
    weightTd.appendChild(document.createTextNode(' кг'));
    PackageTr.appendChild(weightTd)

    //remote_getWeightFromScales(weightTd)

    //let dimensionsTd = document.createElement('td');
    //dimensionsTd.innerHTML = Package.length + 'x' + Package.width + 'x' + Package.height;
    //PackageTr.appendChild(dimensionsTd);

    let DeleteTd = document.createElement('td');
    DeleteTd.innerHTML = 'Удалить';
    DeleteTd.style.backgroundColor = '#ffc8c8';
    if (getCurrentOperation() == OPERATION_SCAN_ORDER) {
        DeleteTd.style.display = 'none';
    }
    DeleteTd.addEventListener('click', function (event) {
        //console.log(event.target.parentNode.rowIndex);
        let ClosestTr = event.target.closest('tr');
        //if (CurrentOrder.PackageSet.length == 1) {
        //    showError("Одно грузовое место должно быть.");
        //
        //    ClosestTr.querySelector('input').select();
        //    //event.target.focus;
        //
        //    return;
        //}
        deletePackage(ClosestTr.rowIndex);
        updatePackageSetCount();
        BarcodeInput.focus();
    });
    PackageTr.appendChild(DeleteTd);

    PackageTr.onclick = function (event) {
        console.log('Выбрано место ' + Package.barcode);
    };

    PackageSetTable.appendChild(PackageTr);

    if (focused) {
        weightInput.focus();
        weightInput.select();
    }
};

const showOrderBlockContent = function () {
    OrderBlockContent.style.display = 'block';
};

const hideOrderBlockContent = function () {
    OrderBlockContent.style.display = 'none';
};

const showItemListBlockContent = function () {
    ItemListBlockContent.style.display = 'block';
};

const hideItemListBlockContent = function () {
    ItemListBlockContent.style.display = 'none';
};

const showPackageSetBlockContent = function () {
    PackageSetBlockContent.style.display = 'block';
};

const hidePackageSetBlockContent = function () {
    PackageSetBlockContent.style.display = 'none';
};

const canRepack = function () {
    return (
        CurrentOrder.status === "В ПРОЦЕССЕ"
        && CurrentOrder.substatus === 'собран'
    );
};

const handleStartPickingClick = function (event) {
    event.preventDefault()

    if (! canStartPicking()) {
        showError('Статус заказа должен быть либо "НОВЫЙ", либо "В ПРОЦЕССЕ принят", либо "В ПРОЦЕССЕ комплектация".')
        return
    }

    remote_startPickingForOrder(CurrentOrder.barcode, CurrentStorekeeper.barcode)
};

const handleCancelClick = function (event) {
    event.preventDefault();

    CurrentOrder.PackageSet = TemporaryPackageSet.slice();
    TemporaryPackageSet.length = 0;
    //populateOrderPackageSetTable(CurrentOrder);

    setCurrentOperation(OPERATION_SCAN_ORDER);

    handleOrder(CurrentOrder);

    event.target.style.display = 'none';
};

const handleRepackClick = function (event) {
    event.preventDefault();

    // Копирую значения одного массива в другой.
    TemporaryPackageSet = CurrentOrder.PackageSet.slice();

    document.getElementById('cancel').style.display = 'inline-block';
    startPacking();
};

const handlePrintOrderLabelClick = function (event) {
    event.preventDefault();
    //alert('Печать этикеток');
    printOrderLabel(CurrentOrder);
};

const showOrderItemListPickingElements = function () {
    let ElementForPickingList = document.querySelectorAll('.elementForPicking');

    if (ElementForPickingList.length) {
        for (let i = 0; i < ElementForPickingList.length; i++) {
            let element = ElementForPickingList[i];

            if (element.localName == 'table')
                element.style.display = 'table';
            else if (element.localName == 'td')
                element.style.display = 'table-cell';
            else if (element.localName == 'div')
                element.style.display = 'block';
            else
                element.style.display = 'inline';
        }
    }
};

const canStartPicking = function () {
    return (
        CurrentOrder.status === "НОВЫЙ"
        || (
            CurrentOrder.status === "В ПРОЦЕССЕ"
            && (
                CurrentOrder.substatus === 'принят'
                || CurrentOrder.substatus === 'комплектация'
            )
        )
    )
}

const enoughAvailable = function () {
    let result = false;
    if (CurrentOrder.Items.length) {
        result = true;

        for (let i = 0; i < CurrentOrder.Items.length; i++) {
            let Item = CurrentOrder.Items[i];

            if (Item.Product.type != 'good')
                continue;

            let plannedQuantity = Item.plannedQuantity;
            let avaliableQuantity = 0;

            // 2020-05-15 Туманов
            // В случае, если товара нет на остатках, то у него даже не было элемента StockItems.
            // TODO: Исправить
            if (
                Item.Product.StockItems
                && Item.Product.StockItems.length
            ) {
                for (let y = 0; y < Item.Product.StockItems.length; y++) {
                    StockItem = Item.Product.StockItems[y];

                    if (StockItem.barcode == '00000015OTG01101')
                        continue;

                    avaliableQuantity += StockItem.quantity;

                    if (avaliableQuantity >= plannedQuantity)
                        break;
                }
            }

            if (avaliableQuantity < plannedQuantity) {
                result = false;
                showError('Невозможно начать комплектацию, т.к. товара ' + Item.Product.name + " недостаточно.");
                break;
            }
        }
    }

    return result;
}

const startPicking = function () {
    showOrderItemListPickingElements();
    document.getElementById('barcode').focus()

    document.getElementById('cancelPicking').style.display = 'inline-block'

    setCurrentOperation(OPERATION_SCAN_PRODUCT);

    showItemListBlockContent();

    hideOrderBlockContent();
    hidePackageSetBlockContent()

    currentTask.dateOfChange = new Date();
    currentTask.data.order = {
        barcode: CurrentOrder.barcode
    }
    currentTask.executor = CurrentStorekeeper
    currentTask.status = 'inProgress'

    if (
        isMoscowPickup()
        && wsConnection
        && wsConnection.readyState === WebSocket.OPEN
    ) {
        wsConnection.send(JSON.stringify(currentTask))
    }

    //pushTaskToStack(currentTask)

    document.getElementById('startPicking').style.display = 'none';
}

const remote_startPickingForOrder = function (orderBarcode, pickerBarcode) {
    console.log("Посылаю запрос на создание комплектации.");

    //let postData = {
    //    'orderBarcode': CurrentOrder.barcode,
    //    'pickerBarcode': CurrentStorekeeper.barcode
    //};

    // Временно отключаю. Пока остатки не сведём!
    //if (! enoughAvailable()) {
    //    return
    //}

    lockBarcodeHandling()

    let postData = {
        'orderBarcode': orderBarcode,
        'pickerBarcode': pickerBarcode
    };

    let XHR = new XMLHttpRequest();

    XHR.open('POST', '/api/json/picking.php?method=startPickingForOrder');
    XHR.send(JSON.stringify(postData));
    XHR.onload = function () {
        unlockBarcodeHandling()

        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
            return;
        }

        var Response = JSON.parse(XHR.responseText);

        console.log(Response);

        if (Response.code !== "ok") {
            if (Response.barcode) {
                CurrentPicking.barcode = Response.barcode;
            }
            else {
                showError(Response.description);

                return;
            }
        }
        CurrentPicking.barcode = Response.barcode;

        setCurrentPicking(CurrentPicking)

        console.log("Комплектация создана.")

        if (CurrentOrder.status === 'НОВЫЙ') {
            CurrentOrder.status = "В РАБОТЕ"
            CurrentOrder.substatus = "комплектация"

            populateOrderBlockContent(CurrentOrder)
        }

        setCurrentOrder(CurrentOrder)

        startPicking()
    };
    XHR.onerror = function (event) {
        console.log("ОШИБКА создания комплектации.");
        unlockBarcodeHandling()
    }
};

const handlePickingRowQueue = function () {
    if (PickingRowQueue.length == 0) {
        return;
    }

    handlePickingRowQueueInProcess = true;

    PickingRow = PickingRowQueue.pop();

    remote_addRowToPicking(CurrentPicking.barcode, CurrentStorekeeper.barcode, PickingRow);
};

const remote_addRowToPicking = function (documentBarcode, storekeeperBarcode, PickingRow) {
    console.log("Посылаю запрос на создание комплектации.", PickingRow)

    lockBarcodeHandling()

    //let postData = {
    //    'documentBarcode': CurrentPicking.barcode,
    //    'storekeeperBarcode': CurrentStorekeeper.barcode,
    //    'RowOfProductMovement': {
    //
    //    }
    //};

    let postData = {
        'documentBarcode': documentBarcode,
        'storekeeperBarcode': storekeeperBarcode,
        'RowOfProductMovement': PickingRow
    };

    let XHR = new XMLHttpRequest();

    XHR.open('POST', '/api/json/picking.php?method=addRowToPicking');
    XHR.send(JSON.stringify(postData));
    XHR.onload = function () {
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
            // Вернуть строку отбора обратно в очередь.
            PickingRowQueue.push(PickingRow)

            handlePickingRowQueueInProcess = false
            unlockBarcodeHandling()

            return
        }

        let Response = JSON.parse(XHR.responseText);

        console.log(Response);

        if (Response.code !== "ok") {
            showError(Response.description);
            // Вернуть строку отбора обратно в очередь.
            PickingRowQueue.push(PickingRow);

            handlePickingRowQueueInProcess = false;
            unlockBarcodeHandling();

            return;
        }

        console.log("Добавление строки в комплектацию выполнено.");

        if (PickingRowQueue.length > 0) {
            handlePickingRowQueue();
        }
        else {
            handlePickingRowQueueInProcess = false;
            unlockBarcodeHandling();

            if (pickingCanBeFinished) {
                remote_finishPickingForOrder(CurrentOrder.barcode, CurrentStorekeeper.barcode);
            }
        }
    };
    XHR.onerror = function (event) {
        console.log("ОШИБКА добавления строки в комплектацию.");

        // Вернуть строку отбора обратно в очередь.
        PickingRowQueue.push(PickingRow);

        handlePickingRowQueueInProcess = false;
        unlockBarcodeHandling();
    }
};

const remote_finishPickingForOrder = function (orderBarcode, pickerBarcode) {
    console.log("Посылаю запрос на завершение комплектации.");

    if (handlePickingRowQueueInProcess) {
        return;
    }

    if (PickingRowQueue.length > 0) {
        handlePickingRowQueue()
    }

    lockBarcodeHandling()

    //let postData = {
    //    'orderBarcode': CurrentOrder.barcode,
    //    'pickerBarcode': CurrentStorekeeper.barcode
    //};

    let postData = {
        'orderBarcode': orderBarcode,
        'pickerBarcode': pickerBarcode
    };

    let XHR = new XMLHttpRequest();

    XHR.open('POST', '/api/json/picking.php?method=finishPickingForOrder');
    XHR.send(JSON.stringify(postData));
    XHR.onload = function () {
        unlockBarcodeHandling();

        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
            document.getElementById('finishPicking').style.display = 'inline-block';
            return;
        }

        let Response = JSON.parse(XHR.responseText);

        console.log(Response);

        if (Response.code !== "ok") {
            showError(Response.description);
            document.getElementById('finishPicking').style.display = 'inline-block';
            return;
        }

        pickingCanBeFinished  = false;
        console.log("Комплектация завершена.");

        currentTask.dateOfChange = new Date();
        currentTask.status = 'done'

        if (
            isMoscowPickup()
            && wsConnection
            && wsConnection.readyState === WebSocket.OPEN
        ) {
            wsConnection.send(JSON.stringify(currentTask))
        }

        removeCurrentPicking()

        startPacking();
    };
    XHR.onerror = function (event) {
        unlockBarcodeHandling();
        console.log("ОШИБКА завершения комплектации.");
        // Резервный вариант
        document.getElementById('finishPicking').style.display = 'inline-block';
    }
};

const remote_cancelPickingForOrder = function (orderBarcode, pickerBarcode) {
    console.log("Посылаю запрос на завершение комплектации.");

    if (handlePickingRowQueueInProcess) {
        return;
    }

    //if (PickingRowQueue.length > 0) {
    //    handlePickingRowQueue();
    //}

    lockBarcodeHandling();

    //let postData = {
    //    'orderBarcode': CurrentOrder.barcode,
    //    'pickerBarcode': CurrentStorekeeper.barcode
    //};

    let postData = {
        'orderBarcode': orderBarcode,
        'pickerBarcode': pickerBarcode
    };

    let XHR = new XMLHttpRequest();

    XHR.open('POST', '/api/json/picking.php?method=cancelPickingForOrder');
    XHR.send(JSON.stringify(postData));
    XHR.onload = function () {
        unlockBarcodeHandling();

        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
            return;
        }

        let Response = JSON.parse(XHR.responseText);

        console.log(Response);

        if (Response.code !== "ok") {
            showError(Response.description);
            return;
        }

        console.log("Отмена комплектации завершена.");
        setCurrentOperation(OPERATION_SCAN_ORDER);
        hideOrderBlock();
        showOrderBlockContent();
        showItemListBlockContent();
        showPackageSetBlockContent()

        removeCurrentOperation()
        removeCurrentOrder()
        removeCurrentPicking()

        currentTask.dateOfChange = new Date();
        currentTask.status = 'canceled'

        if (
            isMoscowPickup()
            && wsConnection
            && wsConnection.readyState === WebSocket.OPEN
        ) {
            wsConnection.send(JSON.stringify(currentTask))
        }

        popTaskFromStack()

        let success = document.getElementById('success')
        success.style.display = 'block'
        setTimeout(() => {
            success.style.display = 'none'
            cleanForm()
            history.back()
        }, 1000)
    };
    XHR.onerror = function (event) {
        unlockBarcodeHandling();
        console.log("ОШИБКА отмены комплектации.");
    }
}

const isMoscowPickup = () => {
    return CurrentOrder.Contractor.name === 'Москва'
        && CurrentOrder.type.toLowerCase() === 'самовывоз'
        && CurrentOrder.subtype.toLowerCase() === 'покупателем'
}

const startPacking = function () {
    hideOrderBlockContent();
    hideItemListBlockContent();
    showPackageSetBlockContent();
    document.getElementById('cancelPicking').style.display = 'none';

    setCurrentOperation(OPERATION_PACK_CARGO);

    for (let i = 0; i < PackageSetTable.rows.length; i++) {
        PackageSetTable.rows[i].cells[1].firstChild.readOnly = false;
        PackageSetTable.rows[i].cells[2].style.display = 'table-cell';
    }

    document.getElementById('repack').style.display = 'none';
    document.getElementById('printOrderLabelSet').style.display = 'none';
    document.getElementById('barcode').focus();
}

const handleOrderBlockViewSwitchClick = function (event) {
    if (OrderBlockContent.style.display == 'none')
        OrderBlockContent.style.display = 'block';
    else
        OrderBlockContent.style.display = 'none';
}

const handleItemListTableViewSwitchClick = function (event) {
    if (ItemListBlockContent.style.display == 'none')
        ItemListBlockContent.style.display = 'block';
    else
        ItemListBlockContent.style.display = 'none';
}

const handlePackageSetBlockViewSwithClick = function (event) {
    if (PackageSetBlockContent.style.display == 'none')
        PackageSetBlockContent.style.display = 'block';
    else
        PackageSetBlockContent.style.display = 'none';
}

const logToDiv = value => {
    let logDiv = document.getElementById('log')

    switch (typeof  value) {
        case 'string':
        case 'number':
        case 'boolean':
            logDiv.innerHTML += `${value}<br>`
            break
        case 'object':
        case 'function':
            logDiv.innerHTML += JSON.stringify(value) + '<br>'
            break
        default:
            logDiv.innerHTML += typeof value + '<br>'
    }
}
    </script>
</head>
<body>
<header></header>
<main style="margin-left: 1%; margin-right: 1%;">
    <h2>Комплектация заказа</h2>
        <div id="BarcodeBlock">
        <div id="standbyIndicator" style="background-color: #ffc8c8; display: none;">Сетевая операция</div>
        <div id="currentCommand">Отсканируйте ШК заказа</div>
        <input id="barcode" type="text" name="barcode" onkeypress="send()" />
    </div>

    <div id="success" style="background-color: rgb(199, 232, 172); display: none; padding: 1%;">Успешно</div>
    <div id="error" style="display: none;"></div>

    <div id="OrderBlock" style="display: none;">
        <h3 id="OrderBlockViewSwitch">Заказ</h3>
        <div id="OrderBlockContent">
            <table class="no_borders">
                <tr>
                    <td>дата:</td>
                    <td><b id="date"></b></td>
                </tr>
                <tr>
                    <td>номер:</td>
                    <td><b id="number"></b></td>
                </tr>
                <tr>
                    <td>штрихкод:</td>
                    <td><b id="orderBarcode"></b></td>
                </tr>
                <tr>
                    <td>заказчик:</td>
                    <td><b id="customerName"></b></td>
                </tr>
                <tr>
                    <td>подрядчик:</td>
                    <td><b id="contractorName"></b></td>
                </tr>
                <tr>
                    <td>вид:</td>
                    <td><b id="type"></b></td>
                </tr>
                <tr>
                    <td>статус:</td>
                    <td><b id="status"></b></td>
                </tr>
            </table>

            <div class="order__comment">
                <div class="order__comment-title">Комментарий:</div>
                <div id="comment" class="order__comment-text comment__text"></div>
            </div>

            <a id="startPicking" href="#" class="button" style="width: 96%; display: none; max-width: 300px;">Собрать</a>
        </div>

        <h3 id="ItemListTableViewSwitch">Товары</h3>
        <div id="ItemListBlockContent">
            <table id="ItemListTable"></table>
            <a id="finishPicking" href="#" class="button" style="width: 96%; display: none;">Завершить комплектацию</a>
        </div>

        <h3 id="PackageSetBlockViewSwith">Грузовые места</h3>
        <div id="PackageSetBlockContent">
            <div>Количество: <span id="PackageSetCount">0</span></div>
            <table id="PackageSetTable"></table>
        </div>

        <div id="ControlBlock">
            <a id="cancel" href="#" class="button" style="width: 96%; display: none;">Отмена</a>
            <!--<a id="startPicking" href="#" class="button" style="width: 96%; display: none;">Собрать</a>-->
            <a id="repack" href="#" class="button" style="width: 96%; display: none;">Переупаковать</a>
            <a id="printOrderLabelSet" href="#" class="button" style="width: 96%; display: none;">Распечатать этикетки</a>
            <a id="cancelPicking" href="#" class="button" style="width: 96%; display: none;">Отменить комплектацию</a>
        </div>

        <div id="cancelPickingBlock" style="display: none;">
            <h3>Укажите причину отмены</h3>
            <a id="cancelWithReasonNoProduct" href="#" class="button" style="width: 96%;">Нет товара</a>
            <a id="cancelCancelling" href="#" class="button" style="width: 96%;">Вернуться к комплектации</a>
        </div>
    </div>
    <div id="log"></div>
</main>
<footer></footer>
</body>
</html>
