<meta charset="utf-8" />
<?php

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

$Docs = [];

try {
    $soapClientOptions = [
        'login' => Config::REMOTE_DB_USER,
        'password' => Config::REMOTE_DB_PASSWORD,
        'cache_wsdl' => WSDL_CACHE_NONE
    ];

    $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

    //$result = $SoapClient->GetMovingGoodsDocs();
    $result = $SoapClient->GetWarehouseDocuments();

    preDump($result); exit;
    /*object(stdClass)#2 (1) {
  ["return"]=>
  object(stdClass)#3 (3) {
    ["code"]=>
    string(2) "ok"
    ["description"]=>
    string(0) ""
    ["Documents"]=>
    array(2) {
      [0]=>
      object(stdClass)#4 (3) {
        ["date"]=>
        string(19) "2019-11-03T19:18:23"
        ["number"]=>
        string(9) "000000004"
        ["type"]=>
        string(20) "Размещение"
      }
      [1]=>
      object(stdClass)#5 (3) {
        ["date"]=>
        string(19) "2019-11-04T17:33:31"
        ["number"]=>
        string(9) "000000005"
        ["type"]=>
        string(20) "Перекладка"
      }
    }
  }
}
     */

    if (
        isset($result->return)
        && isset($result->return->code)
    ) {
        if ($result->return->code == 'OK') {
            if (isset($result->return->Docs)) {
                if (isset($result->return->Docs->Doc->date))
                    $Docs = [$result->return->Docs->Doc];
                else
                    $Docs = $result->return->Docs->Doc;
            }
        }
    }

    //if (
    //    isset($result->return)
    //    && isset($result->return->code)
    //) {
    //    if ($result->return->code === 0) {
    //        //$Order = new ru\trde\Order();
    //
    //        if (
    //            isset($result->return)
    //            && isset($result->return->Product)
    //        ) {
    //            $WsProduct = $result->return->Product;
    //
    //            //$Order->initFromWsOrder($WsOrder);
    //        }
    //        else {
    //            die(json_encode(['code' => 1, 'description' => 'Товар найден, но данные не получены', 'Product' => null], JSON_UNESCAPED_UNICODE));
    //        }
    //
    //        $response = [
    //            'code' => $result->return->code,
    //            'description' => $result->return->description,
    //            'Product' => [
    //                'barcode' => $result->return->Product->barcode,
    //                'name' => $result->return->Product->name,
    //                'owner' => $result->return->Product->owner,
    //                'balance' => $result->return->Product->balance
    //            ]
    //        ];
    //
    //        die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
    //    }
    //    else {
    //        die(json_encode(['code' => 1, 'description' => $result->return->description, 'Product' => null], JSON_UNESCAPED_UNICODE));
    //    }
    //}
    //else {
    //    die(json_encode(['code' => 1, 'description' => 'Не получен код ответа сервера', 'Product' => null], JSON_UNESCAPED_UNICODE));
    //}
}
catch (SoapFault $fault) {
    die(json_encode(['code' => 1, 'description' => 'Исключение: ' . $fault->getMessage(), 'Product' => null], JSON_UNESCAPED_UNICODE));
}
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <title>Остаток товара</title>
    <style type="text/css">
* {
    margin: 0;
    padding: 0;
}

table {
    border-collapse: collapse;
}

td {
    border: 1px solid black;
    padding: 5px;
}

.button {
    color: #fff;
    text-decoration: none;
    display: inline-block;
    background-color: #1a73e8;
}

.button:hover {
    background-color: #2b84f9;
}
    </style>
</head>
<body>
<a href="." class="button" style="width: 96%; padding: 1%; margin: 1%;">Вернуться в основное меню</a>
<div style="margin-left: 1%; margin-right: 1%;">
    <p>Документы</p>
    <table>
<?php foreach ($Docs as $Document) { ?>
        <tr>
            <td><a href="relocation_doc.php?doc=<?= $Document->number ?>"><?= $Document->type ?> <?= $Document->number ?></a></td>
        </tr>
<?php } ?>
    </table>
</div>
</body>
</html>
