<?php

require_once 'class/Config.php';

// Получаем из GET код контрагента и пытаемся его идентиицировать
// Если контрагент не опознан, то возвращаем ошибку авторизации
// Ключ Гермес - 10254B5C8CCC643EE150B48044C37D50
if (
    ! isset($_GET['action'])
    || empty($_GET['action'])
    || (
        $_GET['action'] != 'printOrderLabel'
        && $_GET['action'] != 'printCellLabel'
    )
) {
    //returnError('В запросе не указан параметр \'key\', необходимый для идентификации.');
    //die('Не указано желаемое действие');
}

$postData = file_get_contents('php://input');
$Request = json_decode($postData, false);

//print_r($Request);

//die('test');
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'printOrderLabel') {
        $orderPlace = "^XA
^FX Определяем верхний левый край для печати и кодировку
^LH0,0
^CI28

^FT60,80
^AG
^FDMSK-300420^FS

^FT60,140
^A0,30,24
^FDОтправитель:^FS

^FT240,140
^A0,30,24
^FDwww.mirinkub.ru^FS

^FT60,250
^GB200,80,2^FS

^FT260,250
^GB480,80,2^FS

^FT60,300
^A0,30,24
^FDПолучатель:^FS

^FT240,300
^A0,30,24
^FDИванов И.И.^FS

^FT60,340
^A0,30,24
^FDТелефон:^FS

^FT240,340
^A0,30,24
^FD+7 987 123-45-67^FS

^FT60,380
^A0,30,24
^FDАдрес:^FS

^FT240,380
^A0,30,24
^FDМосква, ул. Чермянская 3, стр. 1^FS

^FT60,510
^BY4
^A0,45,36
^BC,80,Y,N,N,A
^FD20043012345001^FS

^FT580,510
^AG
^FD2/3^FS

^FT780,550
^A0B,30,24
^FDwww.trde.ru  |  ООО Компания Настоящая Доставка^FS

^FX Габариты
^FX FT350,550
^FX A0B,40,30
^FX FDдлина: 50 FS

^FX FT350,370
^FX A0B,40,30
^FX FDширина: 50 FS

^FX FT350,190
^FX A0B,40,30
^FX FDвысота: 50 FS

^FX Вес
^FX FT450,550
^FX A0B,40,30
^FX FDвес: 10 kg FS

^XZ";
        $zpl_barcode = "^XA
^FX Определяем верхний левый край для печати и кодировку
^LH0,0
^CI28

^FT100,120
^BY4
^A0,40,30
^BC,100,Y,N,N,A
^FD{$Request->barcode}^FS

^FT100,240
^A0,40,30
^FD{$Request->Destination->contactFio}^FS

^FT100,280
^A0,40,30
^FD{$Request->Destination->type}: {$Request->Destination->code}^FS

^FT100,320
^A0,40,30
^FD{$Request->Destination->legalName}^FS

^FX Адрес
^FX FB500,10
^FT100,400
^A0,40,30
^FD{$Request->Destination->address}^FS

^FX Габариты
^FX FT350,550
^FX A0B,40,30
^FX FDдлина: 50 FS

^FX FT350,370
^FX A0B,40,30
^FX FDширина: 50 FS

^FX FT350,190
^FX A0B,40,30
^FX FDвысота: 50 FS

^FX Вес
^FX FT450,550
^FX A0B,40,30
^FX FDвес: 10 kg FS

^XZ";
    }
    else if ($_GET['action'] == 'printCellLabel') {
        for ($level = 1; $level <= 1; $level++) {
            for ($sector = 1; $sector <= 1; $sector++) {
                $cell = [
                    "rack" => "POL",
                    "bay" => "01",
                    "level" => $level,
                    "sector" => ("0" . $sector)
                ];

                $up_arrow = "
^FT530,530
^GB1,100,10^FS
^FT500,480
^GD30,50,10,,R^FS
^FT530,480
^GD30,50,10,,L^FS

^FT600,530
^A0,30,48
^FDВВЕРХУ^FS
            ";

                $down_arrow = "
^FT530,530
^GB1,100,10^FS
^FT500,530
^GD30,50,10,,L^FS
^FT530,530
^GD30,50,10,,R^FS

^FT600,530
^A0,30,48
^FDВНИЗУ^FS
            ";

                if ($cell['level'] == "0") {
                    $arrow = $down_arrow;
                }
                else {
                    $arrow = $up_arrow;
                }

                $zpl_cell = "^XA
    ^FX Определяем верхний левый край для печати и кодировку
    ^LH0,0
    ^CI28

    ^FT50,200
    ^BY4
    ^A0,20,48
    ^BC,180,Y,N,N,A
    ^FD00000015{$cell['rack']}{$cell['bay']}{$cell['level']}{$cell['sector']}^FS

    ^FT50,360
    ^CF0,120
    ^FD{$cell['rack']}^FS

    ^FT100,400
    ^A0,20,24
    ^FDстеллаж^FS

    ^FT300,360
    ^CF0,120
    ^FD{$cell['bay']}^FS

    ^FT330,400
    ^A0,20,24
    ^FDпролёт^FS

    ^FT500,360
    ^CF0,120
    ^FD{$cell['level']}^FS

    ^FT510,400
    ^A0,20,24
    ^FDярус^FS

    ^FT650,360
    ^CF0,120
    ^FD{$cell['sector']}^FS

    ^FT680,400
    ^A0,20,24
    ^FDсектор^FS

    {$arrow}

    ^FT50,530
    ^A0,30,36
    ^FDTRDE - основной склад^FS
    ^XZ";

                for ($side = 1; $side <= 1; $side++) {
                    try {
                        $fp = pfsockopen("192.168.5.57", 9100);

                        fputs($fp, $zpl_cell);
                        fclose($fp);
                    }
                    catch (Exception $e) {
                        echo 'Exception: ' . $e->getMessage() . "\n";
                    }
                }
            }
        }

        header("Location: print_zpl.php");
        exit;
    }
    else if ($_GET['action'] == 'printCustomLabel') {
        try {
            // Любой штрихкод
            $barcode = "^XA
            ^BY4,2,200
            ^FO100,100
            ^BC^FDConfirm^FS
            ^XZ";
            $fp = pfsockopen("192.168.4.57", 9100);

            fputs($fp, $barcode);
            fclose($fp);
        }
        catch (Exception $e) {
            echo 'Exception: ' . $e->getMessage() . "\n";
        }
    }
}

$zpl_large_string = "^XA
^PW600
^LL700
^FO40,200,0
^AE,N,15,10
^TBN,300,400
^FDThis is a test string to see if the text is wrapped inside the text block at all
^FS
^XZ";

//$zpl_large_string_2 = "^XA
//^CF0,30,30
//^FO25,50
//^FB350,4,,
//^FDFD command that IS preceded by an FB command.
//^FS
//
//^FX Адрес
//^FB500,10
//^FT350,550
//^FDадрес: $address^FS
//^XZ";

$zpl_qr = "^XA
^FO100,100
^BQN,2,10
^FDMM,AAC-42^FS
^XZ";

$confirm = "^XA

^BY5,2,270
^FO120,150^BC^FDConfirm^FS

^XZ";

$storekeeper = "^XA

^FX Third section with barcode.
^BY5,2,270
^FO180,150^BC^FD01299^FS

^XZ";


//$zpl_image = "^XA
//^FO0,0^IMG:DOWN_ARROW_COLOR.BMP^FS
//^XZ";

//$zpl_image = "^XA^WDZ:*.BMP^FS^XZ";
//$zpl_image = "^XA^FO50,50^XGZ:UP_ARROW_COLOR.BMP^FS^XZ";
//$zpl_image = "^XA^FO50,50^XGZ:DOWN_ARROW_COLOR.BMP^FS^XZ";
//$zpl_image = "^XA^FO50,50^XGZ:LOGO.PNG^FS^XZ";

// Ячейки
// 00000015PRI01101 - PRI-01-1-01
// 00000015FS101101 - FS1-01-1-01

// Товары
// 000419315 - Сервер Asterisk
// 000419316 - Маршрутизатор Asus

// Кладовщики
// 000000001 - Туманов

// Паллеты
// P000000001 - паллета Туманова
//
//try {
//    $fp = pfsockopen("192.168.5.57", 9100);
//    //fputs($fp, $zpl_large_string_2);
//    //fputs($fp, $zpl_barcode);
//    if ($_GET['action'] == 'printOrderLabel') {
//        fputs($fp, $zpl_barcode);
//    }
//    else if ($_GET['action'] == 'printCellLabel') {
//        fputs($fp, $zpl_cell);
//        //fputs($fp, $storekeeper);
//    }
//    else {
//        fputs($fp, $barcode);
//    }
//    //while (($buffer = fgets($fp, 4096)) !== false) {
//    //    echo $buffer;
//    //}
//    //var_dump(fgetc($fp));
//    //echo bin2hex($buffer);
//    //while (false !== ($char = fgetc($fp))) {
//    //    echo bin2hex($char);
//    //}
//    fclose($fp);
//}
//catch (Exception $e) {
//    echo 'Caught exception: ',  $e->getMessage(), "\n";
//}
/*
 *
 */

function transliterate($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

//$address = transliterate('123456, г. Москва, пр-д Серебрякова, д. 2, стр. 1а');
$address = '123456, г. Москва, пр-д Серебрякова, д. 2, стр. 1а';


$zpl_greed = "^XA
^LH0,0

^FX горизонтальные линии
^FO0,80
^GB800,1,1^FS
^FO0,160
^GB800,1,1^FS
^FO0,240
^GB800,1,1^FS
^FO0,320
^GB800,1,1^FS
^FO0,400
^GB800,1,1^FS
^FO0,480
^GB800,1,1^FS
^FO0,560
^GB800,1,1^FS

^FX вертикальные линии
^FO80,0
^GB1,800,1^FS
^FO160,0
^GB1,800,1^FS
^FO240,0
^GB1,800,1^FS
^FO320,0
^GB1,800,1^FS
^FO400,0
^GB1,800,1^FS
^FO480,0
^GB1,800,1^FS
^FO560,0
^GB1,800,1^FS
^FO640,0
^GB1,800,1^FS
^FO720,0
^GB1,800,1^FS

^XZ";

$zpl_data = "
^XA
^FO100,100^ADN,36,20^FD1234567890
^FS
^XZ";

$zpl_hs = "^XA
~HS
^XZ";


// ЗАГРУЗИТЬ ФОНТЫ
// https://shouldertheboulder.com/article/downloading-fonts-to-printers-the-easy-way

// Онлайн-этикетка для ZPL
// http://labelary.com/viewer.html

// РАБОТАЕТ

//// 1. ПОЛУЧИТЬ РАЗМЕР ФАЙЛА
//$filename = 'PTAstraSans-Regular.ttf';  // Шрифт без засечек
////$filename = 'PTAstraSerif-Regular.ttf';  // Шрифт с засечками
//
//echo 'filesize of ' . $filename . ' is ' . filesize($filename) . ' (в байтах).<br />';
//
//// Получить файл как строку в шестнадцатеричном формате.
//// https://stackoverflow.com/questions/7128469/php-viewing-a-file-as-hex
//
//$hex = unpack("h*", file_get_contents($filename));
//
//var_dump($hex);
//$hex = strtoupper(current($hex));
//
//echo 'HEX: ' . $hex;

//echo 'HEX: ' . strtoupper($hex);

$data = 'A B C - А Б В';
//$data = mb_convert_encoding('A B C - А Б В', "Windows-1251");

// ^CI - Select International Set

//3AKA3
//BEC// Транслитерация строк.

//    $font_and_data = "^XA
//~DUR:ASTRSANS,114808,$hex
//^FT50,100
//^CI28
//^A@,20,20,ASTRSANS
//^FD$data^FS
//^XZ";

    $work = "^XA
^CF0,20
^CI28
^FDТестирование^FS
^XZ";

    $print_dir = "^XA
^HWR:*.*
^XZ";

// ^FT - Field Typeset
// Определяет позицию поля относительно позиции метки, определённой командой ^LH.
// ^FTx,y
// x - смещение по оси x (в точках)
// y - смещение по оси y (в точках)

// ^BY - Bar Code Field Default
// Используется для указания размеров и соотношения штрихов.
// ^BYw[,r,h]
// w (width) - ширина штриха
// r (ratio) - отношение толстого штриха к тонкому (по умолчанию 3)
// h (height) - высота штриха

// ^A - Scalable/Bitmapped Font
// ^A - определяет шрифт, используемый текстовым полем. Определяет шрифт дл текущего поля ^FD. Шрифт, определённый ^A
// используется только один раз и если значение ^A не будет указано снова, то будет использоваться значение по умолчанию
// для ^CF
// ^Af[,o,h,w]
// f (font) - указывает на шрифт в принтере (загруженный, EPROM, ранее сохранённые, A-Z0-9)
// o (orientation) - N (normal) - 0, R (rotated) - 90, I (inverted) - 180, B (read from bottom up) - 270. (по умолчанию берётся из ^FW)
// h (height) - высота символа в точках (по умолчанию берётся из ^CF)
// w (width) - ширина символа в точках (по умолчанию берётся из ^CF)

// ^BC - Code 128 Bar Code (Subsets A, B, and C)
// Штрихкод code128. Используется для цифробуквенной идентификации продукции.
// ^BCo,h,f,g,e,m
// o (orientation) - N (0), R (90), I (180), B (270)
// h (height) - высота штрихкода (в точках)
// f - печатать ли интерпретируемую строку Y (да), N (нет) (по умолчанию Y)
// g - печатать интерпретируемую строку над штрихкодом Y (да), N (нет) (по умолчанию N)
// e - печатать контрольный символ Y (да), N (нет) (по умолчанию N)
// m (mode) - режим N (режим не выбран), U (UCC), A (automatic), D (UCC/EAN) (по умолчанию N)

// ^FD Field Data
//
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Печать этикеток</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="application/javascript" src="js/audio.js"></script>
    <script type="application/javascript"></script>
</head>
<body>
<a class="button" href="print_zpl.php?action=printCustomLabel">Печать этикеток для ячеек</a>
</body>
</html>
