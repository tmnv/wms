<?php

require_once 'class/Config.php';

// Получаем из GET код контрагента и пытаемся его идентифицировать.
// Если контрагент не опознан, то возвращаем ошибку авторизации
if (
    ! isset($_GET['action'])
    || empty($_GET['action'])
    || (
        $_GET['action'] != 'printOrderLabel'
        && $_GET['action'] != 'printCellLabel'
    )
) {
    //returnError('В запросе не указан параметр \'key\', необходимый для идентификации.');
    //die('Не указано желаемое действие');
}

$postData = file_get_contents('php://input');
$Request = json_decode($postData, false);

//die('test');
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'printOrderLabel') {
        $Order = $Request;

        $orderDate = $Order->date;
        $orderNumber = trim($Order->number);
        $orderBarcode = $Order->barcode;
        if (isset($Order->Source)) {
            $orderSourceName = $Order->Source->Company->site;
        }
        else {
            $orderSourceName = 'НЕ УКАЗАН';
        }

        if (isset($Order->Destination)) {
            $orderDestinationName = $Order->Destination->Person->name;
            $orderDestinationPhone = $Order->Destination->Person->phone;

            if (isset($Order->Destination->Point))
                $orderDestinationAddress = $Order->Destination->Point->address;
            else
                $orderDestinationAddress = 'НЕ УКАЗАНО';
        }
        else {
            $orderDestinationName = 'НЕ УКАЗАНО';
            $orderDestinationPhone = 'НЕ УКАЗАНО';
            $orderDestinationAddress = 'НЕ УКАЗАНО';
        }
        $orderType = $Order->type == 'delivery' ? 'Д' : 'С';

        if (isset($Order->Contractor)) {
            if (isset($Order->Contractor->name)) {
                $orderContractorName = $Order->Contractor->name;
            }
            elseif (isset($Order->Contractor->Company)) {
                $orderContractorName = $Order->Contractor->Company->name;
            }
            else {
                $orderContractorName = 'НЕ УКАЗАНО';
            }
        }
        else {
            $orderContractorName = 'НЕ УКАЗАНО';
        }

        $orderPackageSetCount = count($Order->PackageSet);

        for ($i = 0; $i < $orderPackageSetCount; $i++) {
            $Package = $Order->PackageSet[$i];

            $packageWeight = $Package->weight . ' кг';
            $packageDimensions = (isset($Package->DimensionSet->length) ? $Package->DimensionSet->length : '0') . 'x' .
                (isset($Package->DimensionSet->width) ? $Package->DimensionSet->width : '0') . 'x' .
                (isset($Package->DimensionSet->height) ? $Package->DimensionSet->height : '0');
            $packageNumber = strval($i + 1) . '/' . $orderPackageSetCount;
            $packageBarcode = $Package->barcode;

            $orderPackageLabel = "^XA
^FX Определяем верхний левый край для печати и кодировку
^LH0,0
^CI28

^FT60,90
^A0,75,60
^FD{$orderNumber}^FS

^FT60,140
^A0,30,24
^FDОтправитель:^FS

^FT240,140
^A0,30,24
^FD{$orderSourceName}^FS

^FT60,230
^GB180,70,2^FS

^FT80,205
^A0,30,24
^FD{$orderDate}^FS

^FT240,230
^GB70,70,2^FS

^FT265,210
^A0,45,36
^FD{$orderType}^FS

^FT310,230
^GB140,70,2^FS

^FT330,190
^A0,30,24
^FD{$packageWeight}^FS

^FT330,220
^A0,30,24
^FD{$packageDimensions}^FS

^FT450,230
^GB290,70,2^FS

^FT470,205
^A0,30,24
^FD{$orderContractorName}^FS

^FT60,280
^A0,30,24
^FDПолучатель:^FS

^FT240,280
^A0,30,24
^FD{$orderDestinationName}^FS

^FT60,320
^A0,30,24
^FDТелефон:^FS

^FT240,320
^A0,30,24
^FD{$orderDestinationPhone}^FS

^FT60,360
^A0,30,24
^FDАдрес:^FS

^FO240,340
^A0,30,24
^TBN,500,60
^FD{$orderDestinationAddress}^FS

^FT60,510
^BY4
^A0,45,36
^BC,80,Y,N,N,A
^FD{$packageBarcode}^FS

^FT580,510
^AG
^FD{$packageNumber}^FS

^FT780,550
^A0B,30,24
^FDwww.trde.ru | ООО Компания Настоящая Доставка^FS

^XZ";

            try {
                //$fp = pfsockopen("192.168.1.82", 9100);
                $fp = pfsockopen("192.168.43.101", 9100);

                fputs($fp, $orderPackageLabel);
                fclose($fp);
            }
            catch (Exception $e) {
                echo 'Exception: ' . $e->getMessage() . "\n";
            }
        }

        die(json_encode(['code' => 'ok', 'description' => '', 'ActionStatusList' => []], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }
}

$zpl_large_string = "^XA
^PW600
^LL700
^FO40,200,0
^AE,N,15,10
^TBN,300,400
^FDThis is a test string to see if the text is wrapped inside the text block at all
^FS
^XZ";
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Печать этикеток заказа</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="application/javascript" src="js/audio.js"></script>
    <script type="application/javascript">
let CurrentOrder = {
    'date': '2020-05-01',
    'number': 'Москва-300420',
    'barcode': '20043012345',
    'type': 'delivery',
    'Source': {
        'Company': {
            'name': 'ИП Зарипова',
            'site': 'www.mirinkub.ru'
        },
        'Person': {
            'name': 'Менеджер ИМ',
            'phone': ''
        }
    },
    'Destination': {
        'Company': {
            'name': '',
            'site': ''
        },
        'Person': {
            'name': 'Иванов И.И.',
            'phone': '8 987 123-45-67'
        },
        'Point': {
            'address': '143430, Московская обл, Красногорский р-н, Козино д, Школьная ул, дом № 89'
        }
    },
    'Contractor': {
        'Company': {
            'name': 'Почта России'
        }
    },
    'PackageSet': [
        {
            'barcode': '20043012345001',
            'weight': 7.5,
            'DimensionSet': {
                'length': 30,
                'width': 20,
                'height': 40
            }
        },
        {
            'barcode': '20043012345002',
            'weight': 4,
            'DimensionSet': {
                'length': 10,
                'width': 10,
                'height': 40
            }
        }
    ]
};

document.addEventListener("DOMContentLoaded", function () {

});

const printOrder = function (Order) {
    if (! Order) {
        Order = CurrentOrder;
    }

    sendOrderToLabelPrinterPrintServer(Order);
};

const sendOrderToLabelPrinterPrintServer = function (Order) {
    let XHR = new XMLHttpRequest();
    XHR.open('POST', 'print_order_zpl.php?action=printOrderLabel');
    XHR.send(JSON.stringify(Order));
    XHR.onload = function () {
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            console.log(Response);

            if (
                Response.code
                && Response.code !== "ok"
            ) {
                showError(Response.description);

                return;
            }

            console.log("Этикетки по заказу напечатаны.");
        }
    };
    XHR.onerror = function (event) {
        console.log('При отправке заказа на печать произошла ошибка');
    };
}
    </script>
</head>
<body>
<a class="button" href="#" onclick="printOrder();">Печать этикеток для заказа</a>
</body>
</html>
