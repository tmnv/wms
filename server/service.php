<?php

require_once('class/Config.php');

// Ничего не кешировать
// https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
header('Cache-Control: no-store');
header('Content-Type: application/json');

require_once('class/Order.php');
require_once('class/Item.php');
require_once('class/Product.php');
require_once('class/Package.php');

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

//use ru\trde;

$barcode = '';
if (isset($_GET['barcode'])) {
    $barcode = htmlspecialchars($_GET['barcode']); // 19062300001
}
else {
    die('Отсканируйте заказ!');
}

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

try {
    $soapClientOptions = [
        'login' => Config::REMOTE_DB_USER,
        'password' => Config::REMOTE_DB_PASSWORD,
        'cache_wsdl' => WSDL_CACHE_NONE
    ];

    $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

    //$result = $SoapClient->GetOrders();
    $result = $SoapClient->GetOrder(['barcode' => $barcode]);

    //preDump($result);

    if (
        ! isset($result->return)
        || ! isset($result->return->code)
    ) {
        die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера', 'Order' => null], JSON_UNESCAPED_UNICODE));
    }

    if ($result->return->code === 'error') {
        die(json_encode(['code' => 'error', 'description' => $result->return->description, 'Order' => null], JSON_UNESCAPED_UNICODE));
    }

    //$Order = new ru\trde\Order();
    //
    //if (
    //    isset($result->return)
    //    && isset($result->return->Order)
    //) {
    //    $WsOrder = $result->return->Order;
    //
    //    $Order->initFromWsOrder($WsOrder);
    //}
    //else {
    //    die(json_encode(['code' => 1, 'description' => 'Заказ найден, но данные не получены', 'Order' => null], JSON_UNESCAPED_UNICODE));
    //}
    //
    //$response = [
    //    'code' => $result->return->code,
    //    'description' => $result->return->description,
    //    'Order' => $Order
    //];

    $Order = $result->return->Order;

    if (
        isset($Order->Items)
        && isset($Order->Items->Item)
    ) {
        $OrderItemList = [];
        if (is_array($Order->Items->Item)) {
            $OrderItemList = $Order->Items->Item;
        }
        else {
            $OrderItemList[] = $Order->Items->Item;
        }

        $Order->Items = $OrderItemList;
    }
    else {
        $Order->Items = [];
    }

    if (
        isset($Order->PackageSet)
        && isset($Order->PackageSet->Package)
    ) {
        $PackageSet = [];
        if (is_array($Order->PackageSet->Package)) {
            $PackageSet = $Order->PackageSet->Package;
        }
        else {
            $PackageSet[] = $Order->PackageSet->Package;
        }

        $Order->PackageSet = $PackageSet;
    }
    else {
        $Order->PackageSet = [];
    }

    die(json_encode(['code' => $result->return->code, 'description' => '', 'Order' => $Order], JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
    //die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
}
catch (SoapFault $fault) {
    die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage(), 'Order' => null], JSON_UNESCAPED_UNICODE));
}
