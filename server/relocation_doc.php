<?php

require_once('class/Config.php');
require_once('class/Good.php');

if (
    isset($_GET['method'])
    && $_GET['method'] == "product.get"
) {
// Ничего не кешировать
// https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
    header('Cache-Control: no-store');
    header('Content-Type: application/json');

    ini_set('soap.wsdl_cache_enabled', 0);
    ini_set('soap.wsdl_cache_ttl', 0);

    //use ru\trde;

    $barcode = '';
    if (isset($_GET['barcode'])) {
        $barcode = htmlspecialchars($_GET['barcode']); // 000419315
    }
    else {
        die('Отсканируйте заказ!');
    }

    function preDump($var) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    try {
        $soapClientOptions = [
            'login' => Config::REMOTE_DB_USER,
            'password' => Config::REMOTE_DB_PASSWORD,
            'cache_wsdl' => WSDL_CACHE_NONE
        ];

        $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

        $result = $SoapClient->GetProduct(['barcode' => $barcode]);

        if (
            isset($result->return)
            && isset($result->return->code)
        ) {
            if ($result->return->code === 0) {
                //$Order = new ru\trde\Order();

                if (
                    isset($result->return)
                    && isset($result->return->Product)
                ) {
                    $WsProduct = $result->return->Product;

                    //$Order->initFromWsOrder($WsOrder);
                }
                else {
                    die(json_encode(['code' => 1, 'description' => 'Товар найден, но данные не получены', 'Product' => null], JSON_UNESCAPED_UNICODE));
                }

                $response = [
                    'code' => $result->return->code,
                    'description' => $result->return->description,
                    'Product' => [
                        'barcode' => $result->return->Product->barcode,
                        'name' => $result->return->Product->name,
                        'owner' => $result->return->Product->owner,
                        'balance' => $result->return->Product->balance
                    ]
                ];

                die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
            }
            else {
                die(json_encode(['code' => 1, 'description' => $result->return->description, 'Product' => null], JSON_UNESCAPED_UNICODE));
            }
        }
        else {
            die(json_encode(['code' => 1, 'description' => 'Не получен код ответа сервера', 'Product' => null], JSON_UNESCAPED_UNICODE));
        }
    }
    catch (SoapFault $fault) {
        die(json_encode(['code' => 1, 'description' => 'Исключение: ' . $fault->getMessage(), 'Product' => null], JSON_UNESCAPED_UNICODE));
    }
}
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <title>Остаток товара</title>
    <style type="text/css">
* {
    margin: 0;
    padding: 0;
}

table {
    border-collapse: collapse;
}

td {
    border: 1px solid black;
    padding: 5px;
}

.button {
    color: #fff;
    text-decoration: none;
    display: inline-block;
    background-color: #1a73e8;
}

.button:hover {
    background-color: #2b84f9;
}
    </style>
    <script type="application/javascript">

var div_date = document.getElementById('date');
var div_barcode = document.getElementById('barcode');
var div_number = document.getElementById('number');
var div_items = document.getElementById('items');

document.addEventListener('DOMContentLoaded', function() {
    var input_barcode = document.getElementById('barcode');
    input_barcode.focus();
});

function send() {
    //console.log(this.event.keyCode);
    if (this.event.keyCode !== 13) {
      return false;
    }

    var input_barcode = document.getElementById("barcode");

    var p_scannedBarcode = document.getElementById('scannedBarcode');
    p_scannedBarcode.innerHTML = '<pre>"' + input_barcode.value + '"</pre>';

    var barcode = (input_barcode.value).trim(); // Например, 000419315

    cleanForm();

    var div_productBarcode = document.getElementById('productBarcode');
    var div_productName = document.getElementById('productName');
    var div_productOwner = document.getElementById('productOwner');
    var div_productBalance = document.getElementById('productBalance');

    if (barcode === '') {
        div_productBarcode.innerHTML = 'Укажите номер заказа';

        return;
    }

    var XHR = new XMLHttpRequest();

    XHR.open('GET', 'product.php?barcode=' + barcode + '&method=product.get');
    XHR.send();

    XHR.onload = function () {
        if (XHR.status != 200) {
            div_productBarcode.innerHTML = 'ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText;
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            if (Response.code !== 0) {
                div_productBarcode.innerHTML = Response.description;

                return;
            }

            var Product = Response.Product;

            div_productBarcode.innerHTML = 'Штрихкод: ' + Product.barcode;
            div_productName.innerHTML = 'Наименование: ' + Product.name;
            div_productOwner.innerHTML = 'Владелец: ' + Product.owner;
            div_productBalance.innerHTML = 'Количество: ' + Product.balance;
            if (Product.balance < 0) {
                div_productBalance.style.backgroundColor = '#f00';
                div_productBalance.style.color = '#fff';
            }

            document.getElementById('productData').style.display = "block";
        }
    }
}

function cleanForm() {
    var div_productBarcode = document.getElementById('productBarcode');
    div_productBarcode.innerHTML = '';

    var div_productName = document.getElementById('productName');
    div_productName.innerHTML = '';

    var div_productOwner = document.getElementById('productOwner');
    div_productOwner.innerHTML = '';

    var div_productBalance = document.getElementById('productBalance');
    div_productBalance.innerHTML = '';

    var input_barcode = document.getElementById('barcode');
    input_barcode.value = '';
    input_barcode.focus();
}
    </script>
</head>
<body>
<a href="." class="button" style="width: 96%; padding: 1%; margin: 1%;">Вернуться в основное меню</a>
<div style="margin-left: 1%; margin-right: 1%;">
    <p>Отсканируйте товар</p>
    <input id="barcode" type="text" name="barcode" onkeypress="send()" /><br />
    <p id="scannedBarcode"></p>
    <!--<button id="send" name="send" onclick="cleanForm()">очистить</button>-->
    <div id="productData" style="display: none;">
        <div id="productBarcode"></div>
        <div id="productName"></div>
        <div id="productOwner"></div>
        <div id="productBalance"></div>
    </div>
</div>
</body>
</html>
