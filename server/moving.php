<?php

// ВСЁ ПЕРЕНЕСЕНО В /api/json/moving.php !

//require_once('class/Config.php');
//
//function preDump($var) {
//    echo '<pre>';
//    var_dump($var);
//    echo '</pre>';
//}
//
//if (isset($_GET['method'])) {
//    // Ничего не кешировать
//    // https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
//    header('Cache-Control: no-store');
//    header('Content-Type: application/json; charset=utf-8');
//
//    ini_set('soap.wsdl_cache_enabled', 0);
//    ini_set('soap.wsdl_cache_ttl', 0);
//
//    $soapClientOptions = [
//        'login' => Config::REMOTE_DB_USER,
//        'password' => Config::REMOTE_DB_PASSWORD,
//        'cache_wsdl' => WSDL_CACHE_NONE
//    ];
//
//    $dbhost = Config::REMOTE_DB_HOST;
//    $dbname = Config::REMOTE_DB_NAME;
//
//    if ($_GET['method'] == "warehouseDocumentItem.add") {
//        $postData = file_get_contents('php://input');
//        $Request = json_decode($postData, false);
//
//        /*
//         * object(stdClass)#1 (6) {
//  ["warehouseDocumentBarcode"]=>
//  string(9) "000000005"
//  ["sourceCellBarcode"]=>
//  string(16) "00000015PRI01101"
//  ["destinationCellBarcode"]=>
//  string(16) "00000015FS101101"
//  ["productBarcode"]=>
//  string(9) "000419315"
//  ["plannedQuantity"]=>
//  int(0)
//  ["actualQuantity"]=>
//  int(5)
//}
//         */
//
//        //use ru\trde;
//
//        //$barcode = '';
//        //if (isset($_GET['barcode'])) {
//        //    $barcode = htmlspecialchars($_GET['barcode']); // 000419315
//        //}
//        //else {
//        //    die(json_encode(['code' => "error", 'description' => "Не указан штрихкод ячейки.", 'Product' => null], JSON_UNESCAPED_UNICODE));
//        //}
//
//        try {
//            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);
//
//            $WarehouseDocumentItem = [
//                "warehouseDocumentBarcode" => $Request->warehouseDocumentBarcode,
//                "sourceCellBarcode" => $Request->sourceCellBarcode,
//                "destinationCellBarcode" => $Request->destinationCellBarcode,
//                "productBarcode" => $Request->productBarcode,
//                "plannedQuantity" => $Request->plannedQuantity,
//                "actualQuantity" => $Request->actualQuantity
//            ];
//
//            $result = $SoapClient->AddWarehouseDocumentItem(['WarehouseDocumentItem' => $WarehouseDocumentItem]);
//
//            if (
//                ! isset($result->return)
//                || ! isset($result->return->code)
//            ) {
//                die(json_encode(['code' => "error", 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
//            }
//
//            if ($result->return->code != "ok") {
//                die(json_encode(['code' => "error", 'description' => $result->return->description], JSON_UNESCAPED_UNICODE));
//            }
//
//            $response = $result->return;
//
//            //$response = [
//            //    'code' => $result->return->code,
//            //    'description' => $result->return->description,
//            //    'Cell' => [
//            //        'code' => $result->return->Cell->code,
//            //        'name' => $result->return->Cell->name,
//            //        'barcode' => $result->return->Cell->barcode
//            //    ]
//            //];
//
//            die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
//        }
//        catch (SoapFault $fault) {
//            die(json_encode(['code' => "error", 'description' => $fault->getMessage()], JSON_UNESCAPED_UNICODE));
//        }
//    }
//    else if ($_GET['method'] == "warehouseDocument.add") {
//        //use ru\trde;
//
//        $barcode = '';
//        if (isset($_GET['barcode'])) {
//            $barcode = htmlspecialchars($_GET['barcode']); // 000419315
//        }
//        else {
//            die(json_encode(['code' => "error", 'description' => "Не указан штрихкод ячейки.", 'Cell' => null], JSON_UNESCAPED_UNICODE));
//        }
//
//
//        try {
//            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);
//
//            $result = $SoapClient->AddWarehouseDocumentByStorekeeperBarcode(['barcode' => $barcode]);
//
//        //die(json_encode(['code' => "ok", 'description' => "", "Document" => ["number" => "0123", "storekeeper" => $barcode]], JSON_UNESCAPED_UNICODE));
//
//            if ($result->return->code == "ok")
//                die(json_encode(['code' => $result->return->code, 'description' => $result->return->description, "Document" => $result->return->Document], JSON_UNESCAPED_UNICODE));
//            else
//                die(json_encode(['code' => $result->return->code, 'description' => $result->return->description, "Document" => null], JSON_UNESCAPED_UNICODE));
//        }
//        catch (SoapFault $fault) {
//            die(json_encode(['code' => "error", 'description' => $fault->getMessage(), "Document" => null], JSON_UNESCAPED_UNICODE));
//        }
//    }
//    else if ($_GET['method'] == "warehouseDocument.list") {
//        try {
//            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);
//
//            $result = $SoapClient->GetWarehouseDocuments();
//    /*
//object(stdClass)#2 (1) {
//  ["return"]=>
//  object(stdClass)#3 (3) {
//    ["code"]=>
//    string(2) "ok"
//    ["description"]=>
//    string(0) ""
//    ["Documents"]=>
//    array(2) {
//      [0]=>
//      object(stdClass)#4 (3) {
//        ["date"]=>
//        string(19) "2019-11-03T19:18:23"
//        ["number"]=>
//        string(9) "000000004"
//        ["type"]=>
//        string(20) "Размещение"
//      }
//      [1]=>
//      object(stdClass)#5 (3) {
//        ["date"]=>
//        string(19) "2019-11-04T17:33:31"
//        ["number"]=>
//        string(9) "000000005"
//        ["type"]=>
//        string(20) "Перекладка"
//      }
//    }
//  }
//}
//     */
//            if (
//                ! isset($result->return)
//                || ! isset($result->return->code)
//            ) {
//                die(json_encode(['code' => "error", 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
//            }
//
//            if ($result->return->code != "ok") {
//                die(json_encode(['code' => "error", 'description' => $result->return->description], JSON_UNESCAPED_UNICODE));
//            }
//
//            $response = $result->return;
//
//            //$response = [
//            //    'code' => $result->return->code,
//            //    'description' => $result->return->description,
//            //    'Cell' => [
//            //        'code' => $result->return->Cell->code,
//            //        'name' => $result->return->Cell->name,
//            //        'barcode' => $result->return->Cell->barcode
//            //    ]
//            //];
//
//            if (! isset($response->Documents)) {
//                die(json_encode(['code' => "ok", 'description' => "", "Documents" => []], JSON_UNESCAPED_UNICODE));
//            }
//
//            if (isset($response->Documents->date)) {
//                $Document = $response->Documents;
//                $response->Documents = [$Document];
//            }
//
//            die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
//
//            //if (
//            //    isset($result->return)
//            //    && isset($result->return->code)
//            //) {
//            //    if ($result->return->code == 'ok') {
//            //        if (isset($result->return->Docs)) {
//            //            if (isset($result->return->Docs->Doc->date))
//            //                $Docs = [$result->return->Docs->Doc];
//            //            else
//            //                $Docs = $result->return->Docs->Doc;
//            //        }
//            //    }
//            //}
//        }
//        catch (SoapFault $fault) {
//            die(json_encode(['code' => "error", 'description' => $fault->getMessage()], JSON_UNESCAPED_UNICODE));
//        }
//    }
//    else if ($_GET['method'] == "warehouseDocument.close") {
//        $barcode = '';
//        if (isset($_GET['barcode'])) {
//            $barcode = htmlspecialchars($_GET['barcode']);
//        }
//        else {
//            die(json_encode(['code' => "error", 'description' => "Не указан штрихкод документа."], JSON_UNESCAPED_UNICODE));
//        }
//
//        try {
//            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);
//
//            $result = $SoapClient->CloseWarehouseDocumentByBarcode(['barcode' => $barcode]);
//
//            die(json_encode(['code' => $result->return->code, 'description' => $result->return->description], JSON_UNESCAPED_UNICODE));
//        }
//        catch (SoapFault $fault) {
//            die(json_encode(['code' => "error", 'description' => $fault->getMessage()], JSON_UNESCAPED_UNICODE));
//        }
//    }
//    else {
//        die(json_encode(['code' => "error", 'description' => ("Получен запрос на выполнение неизвестного метода " . htmlspecialchars($_GET['method']))], JSON_UNESCAPED_UNICODE));
//    }
//}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Перемещение товаров</title>
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script type="application/javascript" src="js/audio.js"></script>
    <script type="application/javascript" src="js/cell.js"></script>
    <script type="application/javascript" src="js/product.js"></script>
    <script type="application/javascript">
// Web Storage
// https://html.spec.whatwg.org/multipage/webstorage.html
// ВНИМАНИЕ! И ключи и значения являются строками.

// IndexedDB
// https://www.w3.org/TR/IndexedDB/

// Сущности:
// WarehouseDocument - складской Документ (дата, номер, Склад, Кладовщик, список Строк документа,)
// WarehouseDocumentItem - Строка складского документа (Ячейка-источник, Ячейка-приёмник, Товар, количество товара)
// Cell - Ячейка (код, наименование, адрес, штрихкод, список Строк ячейки)
// CellItem - Строка ячейки (Товар, количество товара)
// Product - Товар (наименование, штрихкод, владелец, список Строк товара)
// ProductItem - Строка товара (Ячейка, количество товара)
// Warehouse - склад
// Storekeeper - кладовщик
let CurrentStorekeeper;

try {
    CurrentStorekeeper = JSON.parse(localStorage.getItem("CurrentStorekeeper"));
}
catch (Exception) {
    window.location = "aaa.php";
}

if (! CurrentStorekeeper) {
    window.location = "aaa.php";
}

let currentDocument;    // WarehouseDocument

const COMMAND_WAIT = "ждите";
const COMMAND_SCAN_CELL_BARCODE = 'отсканируйте ШК ячейки';
const COMMAND_SELECT_OPERATION_TYPE = 'выберите действие с товаром и открытой ячейкой';
const COMMAND_SCAN_PRODUCT_BARCODE = 'отсканируйте ШК товара';
const COMMAND_SCAN_PRODUCT_OR_CELL_BARCODE = 'отсканируйте ШК товара или ячейки';
const COMMAND_MOVE_TO_NEXT_CELL = 'идите к следующей ячейке';

const OPERATION_INIT_VALUES = 'инициирование начальных значений';
const OPERATION_OPEN_CELL = 'открытие ячейки';
const OPERATION_SELECT_TYPE = 'выбор действия с товаром';
const OPERATION_GET_PRODUCTS_FROM_CELL = 'отбор товара из ячейки';
const OPERATION_PUT_PRODUCTS_IN_CELL = 'размещение товара в ячейке';
const OPERATION_MOVE_PRODUCTS = 'перемещение товаров';
const OPERATION_VIEW_DOCUMENTS = 'просмотр документов';

//const ACTION_SCAN_CELL_BARCODE = 'Отсканируйте ШК ячейки.';
//const ACTION_SCAN_PRODUCT_BARCODE = 'Отсканируйте ШК товара и положите его на поддон.';
//const ACTION_SCAN_PRODUCT_OR_CELL_BARCODE = 'Отсканируйте ШК товара или ячейки.';
//
//const CELL_STATE_OPENED = 'ячейка открыта';
//const CELL_STATE_CLOSED = 'ячейка закрыта';

let input_barcode;
let currentCommand = COMMAND_WAIT;
let currentOperation = OPERATION_INIT_VALUES;

// Структура ячейки
// Cell
//  code    - String
//  name    - String
//  address - String
//  barcode - String
//  Items   - ВАРИАНТЫ //TODO: сравнить по удобству и производительности и выбрать (пока работает вариант Array)
//          - Array [
//      {"barcode": 0123, "name": "Товар", "quantity": 10}
//  }
//          - Object {
//      "barcode": {"barcode": 0123, "name": "Товар", "quantity": 10}
//  }

// Все перемещения товара на складе идут в три этапа:
// 1 этап - взять товар из ячейки склада и положить его в ячейку кладовщика
// 2 этап - переместиться к другой ячейке склада
// 3 этап - положить товар в ячейку склада из ячейки кладовщика

let currentCellBarcode;     // Строка штрихкода текущей ячейки (хранится в localStorage).
let currentCell;            // Объект с данными текущей ячейки.

let CurrentSourceCell;         // Ячейка, из которой берут - ячейка склада либо ячейка кладовщика
let CurrentDestinationCell;    // Ячейка, в которую кладут - ячейка кладовщика либо ячейка склада

let currentProductBarcode;  // Строка штрихкода текущего товара (хранится в localStorage).
let currentProduct;         // Объект с данными текущего товара.
let currentCellItemAvailableQuantity = 0;
let currentCellItemPlannedQuantity = 0;
let currentCellItemQuantity = 0;

let WarehouseDocuments = [];
let myCellItems = [];
let finishedMovements = [];

//console.log(indexedDB); // IDBFactory

let initializationInProgress = true;
let db; // Здесь будет экземпляр IDBDatabase

//console.log('db: ' + db);   // undefined


// Если БД ранее не существовало, то она создаётся - срабатывает onupgradeneeded.
// А если уже имеется, то сразу идёт переход к onsuccess.
// Как посмотреть какие БД уже есть? В DevTools. Там же можно и удалить.



// Таблицы
// docs - документы перемещений товаров
// doc-items - строки товаров с ячейками-источниками и ячейками-приёмниками

window.onkeypress = function (event) {
    if (this.event.keyCode == 13) {
        if (
            event.path[0]
            && event.path[0].tagName == "BODY"
        ) {
            if (currentOperation == OPERATION_SELECT_TYPE) {
                showError("Внимание на экран.");
            }
            else {
                document.getElementById("barcode").focus();
                showError("Повторите последнее действие.");
            }
        }
    }
};

//console.log('history');
//console.log(window.history);

window.onload = function (event) {
    if (
        CurrentStorekeeper.barcode == null
        || CurrentStorekeeper.Cell.barcode == null
    ) {
        window.location = "aaa.php";

        return;
    }

    let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('clear')) {
        localStorage.clear();
        let requestDelete = indexedDB.deleteDatabase("wms");

        requestDelete.onsuccess = function (event) {
            window.location = 'moving.php';
        };

        return;
    }

    document.getElementById('currentCommand').innerHTML = COMMAND_WAIT;

    console.log("Открытие базы данных.");

    let request = window.indexedDB.open("wms");

    //console.log(request);   // Экземпляр IDBOpenDBRequest

    request.onupgradeneeded = function(event) {
        console.log('Изменение структуры базы данных.');

        // База данных ранее не существовала, поэтому создаём object stores и индексы.
        db = event.target.result;

        // Создаём хранилища.
        let storekeepers = db.createObjectStore("storekeepers", {keyPath: "barcode"});
        let warehouseDocuments = db.createObjectStore("warehouseDocuments", {keyPath: "barcode"});

        // warehouseDocumentBarcode
        // sourceCellBarcode
        // destinationCellBarcode
        // productBarcode
        // plannedQuantity
        // actualQuantity
        let warehouseDocumentItems = db.createObjectStore("warehouseDocumentItems", {keyPath: "code"});

        let availableProducts = db.createObjectStore("availableProducts", {keyPath: "barcode"});
        let cells = db.createObjectStore("cells", {keyPath: "barcode"});

        // cellBarcode
        // productBarcode
        // quantity
        var cellItems = db.createObjectStore("cellItems", {keyPath: "barcode"});

        // productBarcode
        // quantity
        var myCellItems = db.createObjectStore("myCellItems", {keyPath: "barcode"});
        var products = db.createObjectStore("products", {keyPath: "barcode"});

        //var getOperations = db.createObjectStore("getOperations", {keyPath: "barcode"});
        //var putOperations = db.createObjectStore("putOperations", {keyPath: "barcode"});

        // Примеры создания индексов.
        //var titleIndex = store.createIndex("by_title", "title", {unique: true});
        //var authorIndex = store.createIndex("by_author", "author");
    };

    request.onsuccess = function(event) {
        console.log('База данных открыта.');

        db = event.target.result;    // экземпляр IDBDatabase

        //console.log("DB version: " + db.version);

        document.getElementById('getProductsFromCell').addEventListener('click', getProductsFromCell);
        document.getElementById('putProductsInCell').addEventListener('click', putProductsInCell);
        document.getElementById('currentCellItemQuantity').addEventListener('change', manualSetCurrentCellItemQuantity);

        input_barcode = document.getElementById("barcode");

        initCurrentOperation();
    };

    request.onerror = function (event) {
        console.log(event);
    };
};

function initCurrentOperation() {
    console.log("Инициализация операции.");

    currentOperation = getCurrentOperation();

    if (! currentOperation) {
        console.log("Установка значения текущей операции по умолчанию.");
        //setCurrentOperation(OPERATION_MOVE_PRODUCTS);
        setCurrentOperation(OPERATION_VIEW_DOCUMENTS);
    }

    handleCurrentOperation();
}

function getCurrentOperation() {
    console.log("Получаю значение текущей операции из локального хранилища.");

    return localStorage.getItem("currentOperation");
}

function setCurrentOperation(operation) {
    console.log("Изменение текущей операции с " + currentOperation + " на " + operation);

    currentOperation = operation
    localStorage.setItem("currentOperation", currentOperation);
}

function handleCurrentOperation() {
    console.log("Начинаю выполнять текущую операцию: " + currentOperation + ".");

    if (currentOperation == OPERATION_VIEW_DOCUMENTS) {
        getWarehouseDocuments();
    }
    else if (
        currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL
        || currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL
    ) {
        initCurrentCell();
    }
    else if (
        currentOperation == OPERATION_MOVE_PRODUCTS
        || currentOperation == OPERATION_OPEN_CELL
    ) {
        currentCommand = COMMAND_SCAN_CELL_BARCODE;
        requestAllMyCellItems();
    }
    else if (currentOperation == OPERATION_SELECT_TYPE) {
        currentCommand = COMMAND_SELECT_OPERATION_TYPE;
        requestAllMyCellItems();
    }
    else {
        showError("Неизвестная операция: " + currentOperation);
    }
}

function initCurrentCell() {
    initCellFromLocalStorage();
}

function getCurrentCellBarcode() {
    console.log("Получение штрихкода текущей ячейки.");

    currentCellBarcode = localStorage.getItem('currentCellBarcode');

    return currentCellBarcode;
}

function setCurrentCellBarcode(barcode) {
    console.log("Изменение штрихкода текущей ячейки с " + currentCellBarcode + " на " + barcode + ".");

    currentCellBarcode = barcode;
    localStorage.setItem("currentCellBarcode", currentCellBarcode);
}

function removeCurrentCellBarcode() {
    console.log("Удаление текущей ячейки " + currentCellBarcode + ".");
    localStorage.removeItem("currentCellBarcode");
    currentCellBarcode = null;
    currentCell = null;

    setView();
}

function initCellFromLocalStorage() {
    console.log("Инициализация текущей ячейки.");
    currentCellBarcode = getCurrentCellBarcode();

    if (currentCellBarcode) {
        console.log("В локальном хранилище найден штрихкод ячейки. Начинаю поиск данных ячейки в локальной базе.");
        findCellByBarcode(currentCellBarcode);
    }
    else {
        console.log("Штрихкод ячейки в локальном хранилище не найден.");

        currentCommand = COMMAND_SCAN_CELL_BARCODE;
        //initCurrentOperation();
        setView();
    }
}

function initCurrentProduct() {
    initProductFromLocalStorage()
}

function getCurrentProductBarcode() {
    console.log("Получение штрихкода текущего товара.");

    return localStorage.getItem('currentProductBarcode');
}

function setCurrentProductBarcode(barcode) {
    console.log("Изменение штрихкода текущего товара с " + currentProductBarcode + " на " + barcode + ".");

    currentProductBarcode = barcode;
    localStorage.setItem("currentProductBarcode", currentProductBarcode);
}

function initProductFromLocalStorage() {
    console.log("Начало инициализации товара.");
    currentProductBarcode = getCurrentProductBarcode();

    if (currentProductBarcode) {
        console.log("В локальном хранилище найден штрихкод товара.");

        findProductByBarcode(currentProductBarcode);
    }
    else {
        console.log("Штрихкод товара в локальном хранилище не найден.");

        initCellItemQuantityFromLocalStorage();
    }
}

function initCurrentCellItemQuantity() {
    console.log("Инициирую значение текущего количества товара.");

    if (getCurrentProductBarcode()) {
        currentCellItemAvailableQuantity = getSourceCellItemAvailableQuantityForProductBarcode(currentProductBarcode);
    }

    initCellItemQuantityFromLocalStorage();
}

function initCellItemQuantityFromLocalStorage() {
    console.log("Поиск количества товара в локальном хранилище.");

    currentCellItemQuantity = localStorage.getItem('currentCellItemQuantity');

    // localStorage возвращает null, если такого ключа нет или возвращает строку, даже если значение является числом.
    if (currentCellItemQuantity == null) {
        console.log("Значение количества товара в локальном хранилище не найдено. Ставлю 0.");
        currentCellItemQuantity = 0;
    }
    else {
        currentCellItemQuantity = parseInt(currentCellItemQuantity);
    }

    setView();
}

function getWarehouseDocuments() {
    console.log("Получаем документы склада.");
    var XHR = new XMLHttpRequest();

    XHR.open('GET', 'api/json/moving.php?method=warehouseDocument.list');

    XHR.send();

    XHR.onload = function () {
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            console.log(Response);

            if (Response.code !== "ok") {
                showError(Response.description);

                return;
            }

            console.log("Данные складских документов, полученные из внешней базы:");
            WarehouseDocuments = Response.Documents;
            console.log(WarehouseDocuments);

            setView();

            // Необходимо сохранить ячейку в локальной базе
            //saveCellInLocalDatabase(Cell);
        }
    };
}

function requestAllMyCellItems() {
    console.log("Беру все записи из хранилища avaliableProducts.");

    var tx = db.transaction(["availableProducts"], "readonly");
    var myCellItemsStore = tx.objectStore("availableProducts");
    var request = myCellItemsStore.openCursor();
    myCellItems = [];

    request.onsuccess = function(event) {
        console.log("Запрос курсора выполнен успешно.");

        var cursor = event.target.result;
        if (cursor) {
            console.log("Открыл курсор:");
            console.log(cursor);
            var myCellItem = cursor.value;

            myCellItems.push(myCellItem);

            cursor.continue();
        }
    };

    tx.oncomplete = function (event) {
        console.log('myCellItems:');
        console.log(myCellItems);

        if (currentProductBarcode) {
            currentCellItemAvailableQuantity = getSourceCellItemAvailableQuantityForProductBarcode(currentProductBarcode);
        }

        // При инициализации я попадаю сюда после инициализации ячейки.
        if (initializationInProgress) {
            initCurrentProduct();
        }
        else {
            setView();
        }
    }
}

function getSourceCellItemAvailableQuantityForProductBarcode(barcode) {
    console.log("Получение доступного кличества товара в ячейке-источнике");

    // Пока для складских ячеек количество не указывается
    if (currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL) {
        return 0;
    }

    console.log('currentCellItemAvailableQuantity');
    console.log(currentCellItemAvailableQuantity);

    console.log('myCellItems');
    console.log(myCellItems);

    var quantity = 0;

    if (currentCellItemAvailableQuantity == 0) {
        for (var i = 0; i < myCellItems.length; i++) {
            if (myCellItems[i].barcode == barcode) {
                quantity = parseInt(myCellItems[i].quantity);
                break;
            }
        }
    }
    else {
        quantity = currentCellItemAvailableQuantity;
    }

    return quantity;
}

function setView() {
    if (initializationInProgress) {
        console.log("Инициализация завершена.");
        initializationInProgress = false;
    }

    console.log("Отрисовка интерфейса.");

    hideError();

    console.log("Текущая операция: " + currentOperation + ".");

    if (currentOperation == OPERATION_VIEW_DOCUMENTS) {
        document.getElementById('currentCommand').innerHTML = "Выберите документ";

        hideBarcodeArea();
        hideCurrentDocument();
        hideWorkTable();
        hideMyCellItems();
        hideFinishedOperations();

        showWarehouseDocumentsArea();
    }
    else if (
        currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL
        || currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL
    ) {
        if (currentCell == null)
            currentCommand = COMMAND_SCAN_CELL_BARCODE;
        else if (
            currentProduct == null
            || currentCellItemQuantity == 0
        )
            currentCommand = COMMAND_SCAN_PRODUCT_BARCODE;
        else
            currentCommand = COMMAND_SCAN_PRODUCT_OR_CELL_BARCODE;

        document.getElementById('currentCommand').innerHTML = currentCommand;
        document.getElementById('closeWarehouseDocument').style.display = 'none';

        if (currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL)
            document.getElementById('currentOperation').innerHTML = "Отбор товара";
        else if (currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL)
            document.getElementById('currentOperation').innerHTML = "Размещение товара";

        if (currentCell)
            document.getElementById('currentCell').innerHTML = currentCell.name;
        else
            document.getElementById('currentCell').innerHTML = "";

        if (currentProductBarcode == null)
            document.getElementById("chooseAnotherCell").style.display = "block";
        else
            document.getElementById("chooseAnotherCell").style.display = "none";

        console.log('currentProduct');
        console.log(currentProduct);
        if (currentProduct)
            document.getElementById('currentProduct').innerHTML = currentProduct.name;
        else
            document.getElementById('currentProduct').innerHTML = "";

        document.getElementById('currentCellItemAvailableQuantity').innerHTML = (currentCellItemAvailableQuantity ? currentCellItemAvailableQuantity : 0);
        document.getElementById('currentCellItemQuantity').value = currentCellItemQuantity;

        hideCellOperationsButtons();
        hideMyCellItems();
        hideFinishedOperations();

        showBarcodeArea();
        showCurrentDocument();
        showWorkTable();
    }
    else if (currentOperation == OPERATION_MOVE_PRODUCTS) {
        document.getElementById('currentCommand').innerHTML = COMMAND_SCAN_CELL_BARCODE;
        if (myCellItems.length > 0)
            document.getElementById('closeWarehouseDocument').style.display = 'none';
        else if (finishedMovements.length == 0)
            document.getElementById('closeWarehouseDocument').style.display = 'none';
        else
            document.getElementById('closeWarehouseDocument').style.display = 'block';

        hideWorkTable();
        hideWarehouseDocumentsArea();

        showBarcodeArea();
        showCurrentDocument();
        showMyCellItems();
        showFinishedOperations();
    }
    else if (currentOperation == OPERATION_SELECT_TYPE) {
        document.getElementById('currentCommand').innerHTML = currentCommand;
        document.getElementById('closeWarehouseDocument').style.display = 'none';

        hideBarcodeArea();
        hideWarehouseDocumentsArea();
        hideWorkTable();
        hideFinishedOperations();

        showCellOperationButtons();
        showCurrentDocument();
        showMyCellItems();
    }
}

function getProductsFromCell(event) {
    console.log(event);

    currentCommand = COMMAND_SCAN_CELL_BARCODE;

    setCurrentOperation(OPERATION_GET_PRODUCTS_FROM_CELL);

    //hideCellOperationsButtons();

    setView();
}

function putProductsInCell(event) {
    currentCommand = COMMAND_SCAN_CELL_BARCODE;

    setCurrentOperation(OPERATION_PUT_PRODUCTS_IN_CELL);

    setView();
}

function manualSetCurrentCellItemQuantity() {
    console.log("Ручная установка количества товара.");

    if (currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL) {
        //currentCellItemAvailableQuantity = getSourceCellItemAvailableQuantityForProductBarcode(Product.barcode);

        var avaliableCellItem = null;
        for (var i = 0; i < myCellItems.length; i++) {
            if (myCellItems[i].barcode == currentProduct.barcode) {
                avaliableCellItem = myCellItems[i];
            }
        }

        //if (! avaliableCellItem) {
        //    showError("Товара " + currentProduct.name + " у вас на руках нет.");
        //
        //    return;
        //}

        if (avaliableCellItem.quantity < (currentCellItemQuantity + 1)) {
            showError("Запрошено единиц товара: " + (currentCellItemQuantity + 1) + ". На руках только: " + avaliableCellItem.quantity + ".");

            document.getElementById("currentCellItemQuantity").value = avaliableCellItem.quantity;
        }
    }
    currentCellItemQuantity = parseInt(document.getElementById("currentCellItemQuantity").value);
    localStorage.setItem("currentCellItemQuantity", currentCellItemQuantity);

    document.getElementById("barcode").focus();
}

function send() {
    //console.log(this.event.keyCode);
    if (this.event.keyCode !== 13) {
      return false;
    }

    var p_scannedBarcode = document.getElementById('scannedBarcode');
    p_scannedBarcode.innerHTML = '<pre>"' + input_barcode.value + '"</pre>';

    var barcode = (input_barcode.value).trim(); // Например, 000419315
    input_barcode.value = '';
    input_barcode.focus();

    handleBarcode(barcode);
}

function showError(message) {
    var element = document.getElementById('error');

    playErrorBeep();

    element.innerHTML = message;
    element.style.display = 'block';
}

function hideError() {
    var element = document.getElementById('error');

    element.style.display = 'none';
    element.innerHTML = '';
}

function showCellOperationButtons() {
    document.getElementById('cellOperationsButtons').style.display = 'block';
}

function hideCellOperationsButtons() {
    document.getElementById('cellOperationsButtons').style.display = 'none';

    document.getElementById('barcode').focus();
}

// ЗДЕСЬ БЫЛ КОД ДЛЯ РАБОТЫ С ЯЧЕЙКАМИ

function handleCell(Cell) {
    console.log("Работаю с ячейкой.");

    if (initializationInProgress) {
        console.log("Устанавливаю текущую ячейку.");
        currentCell = Cell;

        requestAllMyCellItems();

        // После инициализации ячейки надо инициализировать операцию, но пока оставляю товар.
        //initCurrentProduct();
    }
    else if (currentCellBarcode == null) {
        console.log("Текущая ячейка ещё не установлена.");

        openCell(Cell);
    }
    else if (Cell.barcode == currentCellBarcode) {
        console.log("Закрываю текущую ячейку");

        closeCell(Cell);
    }
    else if (Cell.barcode != currentCellBarcode) {
        console.log("Найденная ячейка не соответствует текущей открытой ячейке.");
        showError("Вы должны закрыть ячейку " + currentCell.name + " прежде чем открывать ячейку " + Cell.name);
    }
}
/*
if( value ) {
}

will evaluate to true if value is not:

null
undefined
NaN
empty string ("")
0
false
 */
function openCell(Cell) {
    console.log("Открываю ячейку.");

    setCurrentCellBarcode(Cell.barcode);
    currentCell = Cell;

    if (myCellItems.length > 0) {
        console.log("У кладовщика уже есть товары на руках. Показываю ему варианты работы с ячейкой.");
        setCurrentOperation(OPERATION_SELECT_TYPE);
        currentCommand = COMMAND_SELECT_OPERATION_TYPE;
    }
    else {
        console.log("У кладовщика нет товаров на руках. Значить он хочет их взять.");
        setCurrentOperation(OPERATION_GET_PRODUCTS_FROM_CELL);
        currentCommand = COMMAND_SCAN_PRODUCT_BARCODE;
    }

    setView();
}

function showBarcodeArea() {
    document.getElementById("barcodeArea").style.display = "block";
    document.getElementById("barcode").focus();
}

function hideBarcodeArea() {
    document.getElementById("barcodeArea").style.display = "none";
}

function showCurrentDocument() {
    document.getElementById("currentDocumentNumber").innerHTML = localStorage.getItem("currentDocumentBarcode");
    document.getElementById("currentDocument").style.display = "block";
}

function hideCurrentDocument() {
    document.getElementById("currentDocument").style.display = "none";
}

function showMyCellItems() {
    if (myCellItems.length > 0) {
        var tableCellItems = document.querySelector("#myCellItems table");
        console.log("TD");
        console.log(tableCellItems.getElementsByTagName("td"));

        var i;

        for(i = tableCellItems.rows.length - 1; i > 0; i--) {
            tableCellItems.deleteRow(i);
        }

        for (i = 0; i < myCellItems.length; i++) {
            var trCellItem = document.createElement("tr");

            var tdCellItemName = document.createElement("td");
            tdCellItemName.innerText = myCellItems[i].name;
            var tdCellItemQuantity = document.createElement("td");
            tdCellItemQuantity.innerText = myCellItems[i].quantity;

            trCellItem.appendChild(tdCellItemName);
            trCellItem.appendChild(tdCellItemQuantity);

            tableCellItems.appendChild(trCellItem);
        }

        document.querySelector("#myCellItems p").style.display = 'none';
        document.querySelector('#myCellItems table').style.display = 'block';
    }
    else {
        document.querySelector('#myCellItems table').style.display = 'none';
        document.querySelector('#myCellItems p').style.display = 'block';
    }
    document.getElementById('myCellItems').style.display = 'block';
}

function hideMyCellItems() {
    document.getElementById('myCellItems').style.display = 'none';
}

function showWorkTable() {
    //document.getElementById('currentCommand').innerHTML = currentCommand;
    //document.getElementById('currentOperation').innerHTML = currentOperation;
    //if (currentCell)
    //    document.getElementById('currentCell').innerHTML = currentCell.name;
    //if (currentProduct)
    //    document.getElementById('currentProduct').innerHTML = currentProduct.name;
    //document.getElementById('currentCellItemQuantity').value = currentCellItemQuantity;

    document.getElementById('workTable').style.display = 'block';

    input_barcode.focus();
}

function hideWorkTable() {
    document.getElementById('workTable').style.display = 'none';
}

function showWarehouseDocumentsArea() {
    console.log("Генерирую складские документы.");
    var divWarehouseDocuments = document.querySelector("#warehouseDocuments");

    divWarehouseDocuments.innerHTML = "";

    //var h3 = document.createElement("h3");
    //h3.innerHTML = "Список документов";
    //
    //divWarehouseDocuments.appendChild(h3);

    if (WarehouseDocuments) {
        for (var i = 0; i < WarehouseDocuments.length; i++) {
            var Document = WarehouseDocuments[i];
            console.log(Document);
            var divDocument = document.createElement("div");

            divDocument.id = "document_" + Document.number;
            divDocument.className = "warehouseDocument";

            var pName = document.createElement("p");
            pName.innerHTML = Document["type"] + " " + Document.number;

            divDocument.appendChild(pName);

            var pTerminalState = document.createElement("p");
            pTerminalState.innerHTML = "статус: " + Document.terminalStatus;

            divDocument.appendChild(pTerminalState);

            var pStorekeeper = document.createElement("p");
            if (
                Document.Storekeeper
                && Document.Storekeeper.name
            ) {
                pStorekeeper.innerHTML = "кладовщик: " + Document.Storekeeper.name;
            }
            else {
                pStorekeeper.innerHTML = "кладовщик не указан";
            }

            divDocument.appendChild(pStorekeeper);

            divDocument.onclick = function(elem) {
                console.log(elem);
                console.log(this);
                console.log(this.getAttribute("id"));
                var elem_id = this.getAttribute("id");
                var id_parts = elem_id.split("_");
                var documentBarcode = id_parts[1];
                console.log(documentBarcode);
                //alert("TEST");

                localStorage.setItem("currentDocumentBarcode", documentBarcode);
                setCurrentOperation(OPERATION_MOVE_PRODUCTS);
                handleCurrentOperation();
            };

            divWarehouseDocuments.appendChild(divDocument);
        }
    }
    else {
        var p = document.createElement("p");
        p.innerHTML = "Нет документов";

        divWarehouseDocuments.appendChild(p);
    }

        //for (i = 0; i < myCellItems.length; i++) {
        //    var trCellItem = document.createElement("tr");
        //
        //    var tdCellItemName = document.createElement("td");
        //    tdCellItemName.innerText = myCellItems[i].name;
        //    var tdCellItemQuantity = document.createElement("td");
        //    tdCellItemQuantity.innerText = myCellItems[i].quantity;
        //
        //    trCellItem.appendChild(tdCellItemName);
        //    trCellItem.appendChild(tdCellItemQuantity);
        //
        //    tableCellItems.appendChild(trCellItem);
        //}
        /*

<div id="document_000000005" class="warehouseDocument">
    <p>Перемещение 000000005</p>
    <p>статус: на терминале</p>
    <p>кладовщик: Туманов</p>
</div>
         */
    //var elements = document.querySelectorAll(".warehouseDocument");
    //for (var i = 0; i < elements.length; i++) {
    //    elements[i].onclick = function(elem) {
    //        console.log(elem);
    //        console.log(this);
    //        console.log(this.getAttribute("id"));
    //        var elem_id = this.getAttribute("id");
    //        var id_parts = elem_id.split("_");
    //        var documentBarcode = id_parts[1];
    //        console.log(documentBarcode);
    //        //alert("TEST");
    //
    //        localStorage.setItem("currentDocumentBarcode", documentBarcode);
    //        setCurrentOperation(OPERATION_MOVE_PRODUCTS);
    //        handleCurrentOperation();
    //    };
    //}

    document.getElementById('warehouseDocumentsArea').style.display = 'block';
}

function hideWarehouseDocumentsArea() {
    document.getElementById('warehouseDocumentsArea').style.display = 'none';
}

function closeCell(Cell) {
    if (Cell == null) {
        return false;
    }

    var tx = db.transaction("availableProducts", "readwrite");
    var availableProducts = tx.objectStore("availableProducts");

    console.log("Ищу товар в сохранённых ранее товарах, которые уже находятся у кладовщика.");
    var request = availableProducts.get(currentProductBarcode);

    request.onsuccess = function (event) {
        console.log("Запрос товара из списка успешно выполнен.");
        var record = event.target.result;

        //console.log(event);
        // Event {isTrusted: true, type: "success", target: IDBRequest, currentTarget: IDBRequest, eventPhase: 2, …}
        // Если запись товара найдена, то обновляем её, иначе добавляем.

        if (record == undefined) {
            console.log("Запись товара в списке товаров не нашёл. Создаю данные для нового товара.");
            record = {
                "barcode": currentProductBarcode,
                "name": currentProduct.name,
                "quantity": 0
            };
        }

        if (currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL) {
            console.log("Текущая операция отбор товаров, поэтому добавляю количество товара.");
            record.quantity = parseInt(record.quantity) + currentCellItemQuantity;
        }
        else if (currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL) {
            console.log("Текущая операция размещение товаров, поэтому вычитаю количество товара.");
            record.quantity = parseInt(record.quantity) - currentCellItemQuantity;
        }

        if (record.quantity == 0) {
            console.log("Товара " + record.name + " больше нет на руках у кладовшика. Удаляю из локальной базы.");
            availableProducts.delete(record.barcode);
        }
        else {
            console.log("Записываю/перезаписывая запись о товаре в списке товаров.");
            availableProducts.put(record);
        }
    };

    //console.log(request);
    // IDBRequest {source: IDBObjectStore, transaction: IDBTransaction, readyState: "pending", onerror: null, onsuccess: ƒ}

    tx.oncomplete = function (event) {
        console.log('Транзакция обновления списка товаров на руках завершена успешно.');
        //console.log(event);
        // Event {isTrusted: true, type: "complete", target: IDBTransaction, currentTarget: IDBTransaction, eventPhase: 2, …}

        // event.source: IDBObjectStore если использовался put

        //hideWorkTable();
        //showMyCellItems();
        //
        //setCurrentOperation(OPERATION_MOVE_PRODUCTS);
        //
        //currentCommand = COMMAND_SCAN_CELL_BARCODE;
        //
        //currentCellBarcode = null;
        //currentCell = null;
        //localStorage.removeItem('currentCellBarcode');
        //
        //currentProductBarcode = null;
        //currentProduct = null;
        //localStorage.removeItem('currentProductBarcode');
        //
        //currentCellItemAvailableQuantity = 0;
        //currentCellItemPlannedQuantity = 0;
        //currentCellItemQuantity = 0;
        //localStorage.removeItem('currentCellItemQuantity');
        //
        //requestAllMyCellItems();

        addWarehouseDocumentItem();
    };

    tx.onerror = function(event) {
        console.log("TX ERROR");
        console.log(event);
    };
}

function addWarehouseDocumentItem() {
    console.log("Добавляю строку в документ на внешнем сервере.");

    /*
    var loadedJson = {
        "warehouseDocumentBarcode": "000000010",
        "sourceCellBarcode": "00000015PRI01101",
        "destinationCellBarcode": "00000015FS101101",
        "productBarcode": "000419315",
        "plannedQuantity": 0,
        "actualQuantity": 5
    };
     */

    var loadedJson = {
        "warehouseDocumentBarcode": localStorage.getItem("currentDocumentBarcode"),
        "sourceCellBarcode": currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL ? currentCellBarcode : CurrentStorekeeper.Cell.barcode,
        "destinationCellBarcode": currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL ? CurrentStorekeeper.Cell.barcode : currentCellBarcode,
        "productBarcode": currentProductBarcode,
        "plannedQuantity": 0,
        "actualQuantity": currentCellItemQuantity
    };

    var XHR = new XMLHttpRequest();

    XHR.open('POST', 'api/json/moving.php?barcode=' + barcode + '&method=warehouseDocumentItem.add');

    XHR.send(JSON.stringify(loadedJson));

    XHR.onload = function () {
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            if (Response.code !== "ok") {
                showError(Response.description);

                return;
            }

            //hideWorkTable();
            //showMyCellItems();

            if (currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL)
                finishedMovements.push({"SourceCell": currentCell.name, "DestinationCell": "[ячейка кладовщика]", "Product": currentProduct, "quantity": currentCellItemQuantity});
            else
                finishedMovements.push({"SourceCell": "[ячейка кладовщика]", "DestinationCell": currentCell.name, "Product": currentProduct, "quantity": currentCellItemQuantity});

            setCurrentOperation(OPERATION_MOVE_PRODUCTS);

            currentCommand = COMMAND_SCAN_CELL_BARCODE;

            currentCellBarcode = null;
            currentCell = null;
            localStorage.removeItem('currentCellBarcode');

            currentProductBarcode = null;
            currentProduct = null;
            localStorage.removeItem('currentProductBarcode');

            currentCellItemAvailableQuantity = 0;
            currentCellItemPlannedQuantity = 0;
            currentCellItemQuantity = 0;
            localStorage.removeItem('currentCellItemQuantity');

            requestAllMyCellItems();

            //setView();

            //console.log("Данные по ячейке полученные из внешней базы:");
            //Cell = Response.Cell;
            //console.log(Cell);

            // Необходимо сохранить ячейку в локальной базе
            //saveCellInLocalDatabase(Cell);
        }
    };
}

function handleProduct(Product) {
    console.log("Начинаю работу с товаром.");
    console.log(Product);
    if (! currentProduct) {
        currentProduct = Product;
    }

    if (initializationInProgress) {
        console.log("Инициализация данных продолжается.");

        initCurrentCellItemQuantity();

        return;
    }

    // Проверяю наличие такого товара в ячейке отправителе.
    // Если такого товара нет в ячейке отправителе, то тогда показываю ошибку.
    // Если есть, то проверяю не превышает ли количество отбираемых количество доступных. Такое возможно, если:
    // - несколько раз отсканировали одну единицу товара
    // - неправильно вручную указали количество отбираемого товара
    // Если отбираемое количество превышает доступное, то приравниваю количство отбираемого к количеству доступного и
    // выдаю ошибку.
    // Иначе инкрементирую или увеличиваю на указанное количество текущий товар


    //TODO: Получать количество товара для ячеек склада, а не только для товаров ячейки кладовщика.
    if (currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL) {
        currentCellItemAvailableQuantity = getSourceCellItemAvailableQuantityForProductBarcode(Product.barcode);

        var avaliableCellItem = null;
        for (var i = 0; i < myCellItems.length; i++) {
            if (myCellItems[i].barcode == Product.barcode) {
                avaliableCellItem = myCellItems[i];
            }
        }

        if (! avaliableCellItem) {
            showError("Товара " + Product.name + " у вас на руках нет.");

            return;
        }

        if (avaliableCellItem.quantity < (currentCellItemQuantity + 1)) {
            showError("Запрошено единиц товара: " + (currentCellItemQuantity + 1) + ". На руках только: " + avaliableCellItem.quantity + ".");

            return;
        }
    }

    if (currentProductBarcode == null) {
        currentProduct = Product;

        setCurrentProductBarcode(Product.barcode);

        //setView();
    }

    if (Product.barcode == currentProductBarcode) {
        currentCellItemQuantity += 1;
        localStorage.setItem('currentCellItemQuantity', currentCellItemQuantity);

        setView();
    }
    else {
        showError('Это другой товар.');
    }
}

function isProductInCell(Product, Cell) {
    // Заглушка
    return true;
}

function isEnoughProductInCell(Product, Cell) {
    // Заглушка
    return true;
}

function handleBarcode(barcode) {
    console.log("Начинаю работу со штрихкодом " + barcode + ".");

    hideError();

    console.log("Текущая операция: " + currentOperation);

    if (currentOperation == OPERATION_GET_PRODUCTS_FROM_CELL) {
        if (currentCellBarcode == null) {
            console.log("Ячейка ещё не открыта.");
            findCellByBarcode(barcode);

            // Поиск осуществляется асинхронно, поэтому дальнейшие действия будут зависеть
            // от результата поиска.
            return;
        }

        findProductByBarcode(barcode);
    }
    else if (currentOperation == OPERATION_PUT_PRODUCTS_IN_CELL) {
        if (currentCellBarcode == null) {
            console.log("Ячейка ещё не открыта.");
            findCellByBarcode(barcode);

            // Поиск осуществляется асинхронно, поэтому дальнейшие действия будут зависеть
            // от результата поиска.
            return;
        }

        findProductByBarcode(barcode);
    }
    else if (currentOperation == OPERATION_MOVE_PRODUCTS)
        findCellByBarcode(barcode);
}

function addWarehouseDocument() {
    createWarehouseDocumentInRemoteDatabase("123");
}

function createWarehouseDocumentInRemoteDatabase(Document) {
    console.log("Создаём новый складской документ.");

    var XHR = new XMLHttpRequest();

    XHR.open('GET', 'api/json/moving.php?barcode=' + CurrentStorekeeper.barcode + '&method=warehouseDocument.add');

    XHR.send();

    XHR.onload = function () {
        console.log(XHR);
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            if (Response.code !== "ok") {
                showError(Response.description);

                return;
            }

            console.log("Данные нового документа полученные из внешней базы:");
            WarehouseDocument = Response.Document;
            console.log(WarehouseDocument);

            localStorage.setItem("currentDocumentBarcode", WarehouseDocument.number);
            setCurrentOperation(OPERATION_MOVE_PRODUCTS);
            handleCurrentOperation();
        }
    };
}

function saveWarehouseDocumentInLocalDatabase(Document) {

}

function closeWarehouseDocument() {
    closeWarehouseDocumentOnRemoteDatabase();
}

function closeWarehouseDocumentOnRemoteDatabase() {
    console.log("Закрываю складской документ.");

    var XHR = new XMLHttpRequest();

    var currentDocumentBarcode = localStorage.getItem("currentDocumentBarcode");

    if (myCellItems.length > 0) {
        showError("Нельзя закрывать документ при наличии товара на руках.");

        return;
    }

    if (currentDocumentBarcode == null) {
        showError("Не найден штрихкод текущего документа.");

        return;
    }

    XHR.open('GET', 'api/json/moving.php?barcode=' + currentDocumentBarcode + '&method=warehouseDocument.close');

    XHR.send();

    XHR.onload = function () {
        console.log(XHR);
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            if (Response.code !== "ok") {
                showError(Response.description);

                return;
            }

            document.getElementById("success").style.display = "block";

            setTimeout(function () {
                document.getElementById("success").style.display = "none";

                console.log("Удаляю штрихкод текущего документа.");
                localStorage.removeItem("currentDocumentBarcode");

                setCurrentOperation(OPERATION_VIEW_DOCUMENTS);
                handleCurrentOperation();
            }, 1000);
        }
    };
}

function chooseAnotherCell() {
    setCurrentOperation(OPERATION_MOVE_PRODUCTS);
    removeCurrentCellBarcode();

    document.getElementById("chooseAnotherCell").style.display = "none";
}

function showFinishedOperations() {
    var elem_finishedMovements = document.getElementById("finishedMovements");
    elem_finishedMovements.innerHTML = "";

    console.log(finishedMovements);

    if (finishedMovements.length > 0) {
        for (var i = 0; i < finishedMovements.length; i++) {
            console.log(finishedMovements[i]);
            var elem_movementContainer = document.createElement("div");
            elem_movementContainer.style.border = "solid #aaa 1px";
            elem_movementContainer.style.padding = "1%";

            elem_movementContainer.innerHTML = "Из <b>" + finishedMovements[i].SourceCell + "</b> в <b>" + finishedMovements[i].DestinationCell + "</b><br />"
                + "<b>" + finishedMovements[i].Product.name + "</b> в количестве <b>" + finishedMovements[i].quantity + "</b>";

            elem_finishedMovements.appendChild(elem_movementContainer);
        }


        /*


        for (i = 0; i < myCellItems.length; i++) {
            var trCellItem = document.createElement("tr");

            var tdCellItemName = document.createElement("td");
            tdCellItemName.innerText = myCellItems[i].name;
            var tdCellItemQuantity = document.createElement("td");
            tdCellItemQuantity.innerText = myCellItems[i].quantity;

            trCellItem.appendChild(tdCellItemName);
            trCellItem.appendChild(tdCellItemQuantity);

            tableCellItems.appendChild(trCellItem);
        }
         */
    }
    else {
        document.getElementById("finishedMovements").innerHTML = "Перемещений нет.";
    }

    document.getElementById("finishedMovementsArea").style.display = "block";
}

function hideFinishedOperations() {
    document.getElementById("finishedMovementsArea").style.display = "none";
}
    </script>
</head>
<body>
<?php if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' && Config::REMOTE_DB_NAME == 'logistics') : ?>
<h1 style="text-align: center; background-color: red; color: white;">ВНИМАНИЕ! РАБОЧАЯ БАЗА!</h1>
<?php endif; ?>

<div style="margin-left: 1%; margin-right: 1%;">
<div id="messageArea">
    <p id="success" style="text-align: center; background-color: green; color: white; padding: 1%; display: none;">УСПЕШНО</p>
    <p id="error" style="display: none;"></p>
</div>
<h2>Перемещение товаров</h2>
<p id="currentCommand"></p>

<div id="cellOperationsButtons" style="display: none;">
    <button id="getProductsFromCell" class="button" style="width: 49%; margin: 0; margin-right: 1%;">ВЗЯТЬ</button><button id="putProductsInCell" class="button" style="width: 49%; margin: 0; margin-left: 1%;">ПОЛОЖИТЬ</button>
</div>

<div id="barcodeArea">
<input id="barcode" type="text" name="barcode" onkeypress="send()" />
<p id="scannedBarcode"></p>
</div>

<div id="currentDocument" style="display: none;">
<h3>Текущий документ</h3>
<table>
    <!--<tr>-->
    <!--    <td>Тип:</td>-->
    <!--    <td id="currentDocumentType"></td>-->
    <!--</tr>-->
    <tr>
        <td>Номер:</td>
        <td id="currentDocumentNumber"></td>
    </tr>
    <!--<tr>-->
    <!--    <td>Статус:</td>-->
    <!--    <td id="currentDocumentStatus"></td>-->
    <!--</tr>-->
</table>
<!--<button id="returnToList" class="button" style="width: 100%; margin: 0; background-color: red;" onclick="window.history.back();">ОТМЕНА</button>-->
<button id="chooseAnotherCell" class="button" style="width: 100%; margin: 0; display: none;" onclick="chooseAnotherCell();">ВЫБРАТЬ ДРУГУЮ ЯЧЕЙКУ</button>
<!--<button id="workWithDocument" class="button" style="width: 49%; margin-left: 1%;">НАЧАТЬ</button><br />-->
<button id="closeWarehouseDocument" class="button" style="width: 100%; margin: 0; display: none;" onclick="closeWarehouseDocument();">ЗАВЕРШИТЬ ДОКУМЕНТ</button>
</div>

<div id="workTable" style="display: none;">
<table>
    <tr>
        <td>Операция:</td>
        <td id="currentOperation"></td>
    </tr>
    <tr>
        <td>Ячейка:</td>
        <td id="currentCell"></td>
    </tr>
    <tr>
        <td>Товар:</td>
        <td id="currentProduct"></td>
    </tr>
    <tr>
        <td>Кол-во доступно:</td>
        <td id="currentCellItemAvailableQuantity">0</td>
    </tr>
    <tr>
        <td>Кол-во план:</td>
        <td id="currentCellItemPlannedQuantity">0</td>
    </tr>
    <tr>
        <td><label for="currentCellItemQuantity">Кол-во факт:</label></td>
        <td><input id="currentCellItemQuantity" type="number" /></td>
    </tr>
</table>
</div>

<div id="myCellItems" style="display: none; margin-top: 10px;">
<h3>Товары у кладовщика</h3>
<table style="display: none;">
    <tr>
        <th>Товар</th>
        <th>Количество</th>
    </tr>
</table>
<p style="display: none;">Нет товаров.</p>
</div>

<div id="finishedMovementsArea" style="display: none; margin-top: 10px;">
<h3>Выполненные перемещения</h3>
<div id="finishedMovements">

</div>
<p style="display: none;">Нет товаров.</p>
</div>

<div id="warehouseDocumentsArea" style="display: none;">
<h3>Складские документы</h3>
<button id="addWarehouseDocument" class="button" style="width: 100%; margin: 0;" onclick="addWarehouseDocument();">СОЗДАТЬ НОВЫЙ</button>
<p>список:</p>
<div id="warehouseDocuments">
</div>
<!--<div id="document_000000005" class="warehouseDocument">-->
<!--    <p>Перемещение 000000005</p>-->
<!--    <p>статус: на терминале</p>-->
<!--    <p>кладовщик: Туманов</p>-->
<!--</div>-->
</div>

</div>
</body>
</html>
