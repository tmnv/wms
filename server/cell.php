<?php

require_once('class/Config.php');

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

if (isset($_GET['method'])) {
    // Ничего не кешировать
    // https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
    header('Cache-Control: no-store');
    header('Content-Type: application/json');

    ini_set('soap.wsdl_cache_enabled', 0);
    ini_set('soap.wsdl_cache_ttl', 0);

    if ($_GET['method'] == "cell.get") {
        //use ru\trde;

        $barcode = '';
        if (isset($_GET['barcode'])) {
            $barcode = htmlspecialchars($_GET['barcode']); // 000419315
        }
        else {
            die(json_encode(['code' => "error", 'description' => "Не указан штрихкод ячейки.", 'Cell' => null], JSON_UNESCAPED_UNICODE));
        }

        try {
            $soapClientOptions = [
                'login' => Config::REMOTE_DB_USER,
                'password' => Config::REMOTE_DB_PASSWORD,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $result = $SoapClient->GetCellByBarcode(['barcode' => $barcode]);

            //preDump($result);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => "error", 'description' => 'Не получен код ответа сервера', 'Cell' => null], JSON_UNESCAPED_UNICODE));
            }

            if ($result->return->code != "ok") {
                die(json_encode(['code' => "error", 'description' => $result->return->description, 'Cell' => null], JSON_UNESCAPED_UNICODE));
            }

            $response = $result->return;

            //preDump($response);

            //$response = [
            //    'code' => $result->return->code,
            //    'description' => $result->return->description,
            //    'Cell' => [
            //        'code' => $result->return->Cell->code,
            //        'name' => $result->return->Cell->name,
            //        'barcode' => $result->return->Cell->barcode
            //    ]
            //];

            die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => "error", 'description' => 'Исключение: ' . $fault->getMessage(), 'Cell' => null], JSON_UNESCAPED_UNICODE));
        }
    }
}
?><!DOCTYPE html>
<html>
<head>
    <title>Ячейки</title>
</head>
<body>

</body>
</html>
