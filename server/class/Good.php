<?php

namespace ru\trde;

class Good implements \JsonSerializable {
    protected $barcode = '';
    protected $name = '';
    protected $weight = 0;
    protected $length = 0;
    protected $width = 0;
    protected $height = 0;

    public function initFromWsGood(\stdClass $WsGood) {
        try {
            $this->setBarcode($WsGood->barcode);
            $this->setName($WsGood->name);
            $this->setWeight($WsGood->weight);
            //$this->setLength()$WsGood->length);
            //$this->setWidth($WsGood->width);
            //$this->setHeight($WsGood->height);

            $initComplete = true;
        }
        catch (\Exception $Ex) {
            $initComplete = false;
        }

        return $initComplete;
    }

    public function getBarcode() {
        return $this->barcode;
    }

    public function setBarcode($barcode) {
        $this->barcode = $barcode;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    public function getLength() {
        return $this->length;
    }

    public function setLength($length) {
        $this->length = $length;
    }

    public function getWidth() {
        return $this->width;
    }

    public function setWidth($width) {
        $this->width = $width;
    }

    public function getHeight() {
        return $this->height;
    }

    public function setHeight($height) {
        $this->height = $height;
    }

    public function jsonSerialize() {
        return [
            'barcode' => $this->getBarcode(),
            'name' => $this->getName(),
            'weight' => $this->getWeight(),
            'length' => $this->getLength(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight()
        ];
    }
}
