<?php

namespace ru\trde;

class Product implements \JsonSerializable {
    protected $barcode = '';
    protected $name = '';
    protected $weight = 0;
    protected $length = 0;
    protected $width = 0;
    protected $height = 0;
    protected $type = '';
    protected $StockItems = [];

    public function initFromWsProduct(\stdClass $WsProduct) {
        try {
            $this->setBarcode($WsProduct->barcode);
            $this->setName($WsProduct->name);
            $this->setWeight($WsProduct->weight);
            $this->setType($WsProduct->type);
            //$this->setLength()$WsProduct->length);
            //$this->setWidth($WsProduct->width);
            //$this->setHeight($WsProduct->height);

            /*
             *
            ["Product"]=>
            object(stdClass)#9 (5) {
              ["barcode"]=>
              string(9) "000419316"
              ["name"]=>
              string(31) "Маршрутизатор Asus"
              ["balance"]=>
              float(10)
              ["weight"]=>
              float(0.1)
              ["StockItems"]=>
              object(stdClass)#10 (2) {
                ["Cell"]=>
                object(stdClass)#11 (4) {
                  ["code"]=>
                  string(6) "000062"
                  ["name"]=>
                  string(11) "FS1-01-1-01"
                  ["address"]=>
                  string(11) "FS1-01-1-01"
                  ["barcode"]=>
                  string(16) "00000015FS101101"
                }
                ["quantity"]=>
                float(10)
              }
            }

            2 ячейки

            ["Product"]=>
            object(stdClass)#9 (5) {
              ["barcode"]=>
              string(9) "000419316"
              ["name"]=>
              string(31) "Маршрутизатор Asus"
              ["balance"]=>
              float(10)
              ["weight"]=>
              float(0.1)
              ["StockItems"]=>
              array(2) {
                [0]=>
                object(stdClass)#10 (2) {
                  ["Cell"]=>
                  object(stdClass)#11 (4) {
                    ["code"]=>
                    string(6) "000062"
                    ["name"]=>
                    string(11) "FS1-01-1-01"
                    ["address"]=>
                    string(11) "FS1-01-1-01"
                    ["barcode"]=>
                    string(16) "00000015FS101101"
                  }
                  ["quantity"]=>
                  float(7)
                }
                [1]=>
                object(stdClass)#12 (2) {
                  ["Cell"]=>
                  object(stdClass)#13 (4) {
                    ["code"]=>
                    string(6) "000065"
                    ["name"]=>
                    string(11) "PRI-01-1-01"
                    ["address"]=>
                    string(11) "PRI-01-1-01"
                    ["barcode"]=>
                    string(16) "00000015PRI01101"
                  }
                  ["quantity"]=>
                  float(3)
                }
              }
             */

            if (isset($WsProduct->StockItems)) {
                if (is_object($WsProduct->StockItems)) {
                    $StockItem = $WsProduct->StockItems;
                    $WsProduct->StockItems = [];
                    $WsProduct->StockItems[] = $StockItem;

                    $this->StockItems = $WsProduct->StockItems;
                }
                else if (is_array($WsProduct->StockItems)) {
                    $this->StockItems = $WsProduct->StockItems;
                }
                else {
                    // ОШИБКА
                }
            }

            $initComplete = true;
        }
        catch (\Exception $Ex) {
            $initComplete = false;
        }

        return $initComplete;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        if (
            $type == 'good'
            || $type == 'service'
            || $type == 'document'
        ) {
            $this->type = $type;
        }
    }

    public function getBarcode() {
        return $this->barcode;
    }

    public function setBarcode($barcode) {
        $this->barcode = $barcode;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    public function getLength() {
        return $this->length;
    }

    public function setLength($length) {
        $this->length = $length;
    }

    public function getWidth() {
        return $this->width;
    }

    public function setWidth($width) {
        $this->width = $width;
    }

    public function getHeight() {
        return $this->height;
    }

    public function setHeight($height) {
        $this->height = $height;
    }

    public function jsonSerialize() {
        return [
            'barcode' => $this->getBarcode(),
            'name' => $this->getName(),
            'weight' => $this->getWeight(),
            'length' => $this->getLength(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'type' => $this->getType(),
            'StockItems' => $this->StockItems
        ];
    }
}
