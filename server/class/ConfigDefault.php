<?php

/**
 * Это шаблон конфигурационного файла. Необходимо создать копию этого файла с именем Config.php в той же папке,
 * где находится и этот файл.
 */

if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
    header('Cache-Control: no-store');
    header('Content-Type: application/json; charset=utf-8');

    die(json_encode([
        'code' => 'error',
        'description' => 'У вас нет права делать запросы к этому ресурсу.'
    ],JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
}

class Config {
    const REMOTE_DB_HOST = '';
    const REMOTE_DB_NAME = '';
    const REMOTE_DB_USER = '';
    const REMOTE_DB_PASSWORD = '';
}
