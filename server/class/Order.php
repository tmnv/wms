<?php

namespace ru\trde;

class Order implements \JsonSerializable {
    protected $Date = 'null';
    protected $number = '';
    protected $barcode = '';
    //protected $Customer = null;
    protected $customerName = null;     // времянка
    //protected $Contractor = null;
    protected $contractorName = null;   // времянка
    protected $type = '';
    protected $status = '';

    protected $Items = [];

    public function initFromWsOrder(\stdClass $WsOrder) {
        try {
            $this->setDate($WsOrder->date);
            $this->setNumber($WsOrder->number);
            $this->setBarcode($WsOrder->barcode);
            $this->customerName = htmlspecialchars($WsOrder->Customer->name);       // времянка
            $this->contractorName = htmlspecialchars($WsOrder->Contractor->name);   // времянка
            $this->setType($WsOrder->type);
            $this->setStatus($WsOrder->status);

            if (
                isset($WsOrder->Items)
                && isset($WsOrder->Items->Item)
            ) {
                $WsItems = [];
                if (is_array($WsOrder->Items->Item)) {
                    foreach ($WsOrder->Items->Item as $WsItem) {
                        $WsItems[] = $WsItem;
                    }
                }
                else {
                    $WsItems[] = $WsOrder->Items->Item;
                }

                foreach ($WsItems as $WsItem) {
                    $Item = new Item();
                    $Item->initFromWsItem($WsItem);

                    $this->Items[] = $Item;
                }
            }

            $initComplete = true;
        }
        catch (\Exception $Ex) {
            $initComplete = false;
        }

        return $initComplete;
    }

    public function getDate() {
        return $this->Date;
    }

    public function setDate($Date) {
        $this->Date = $Date;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getBarcode() {
        return $this->barcode;
    }

    public function setBarcode($barcode) {
        $this->barcode = $barcode;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function addItem(Item $Item) {
        $this->Items[] = $Item;
    }

    public function getItems() {
        return $this->Items;
    }

    public function toJson() {
        return json_encode($this, JSON_UNESCAPED_UNICODE);
    }

    public function jsonSerialize() {
        $jsonItems = [];
        foreach($this->Items as $Item) {
            $jsonItems[] = $Item->jsonSerialize();
        }

        return [
            'date' => $this->getDate(),
            'number' => $this->getNumber(),
            'barcode' => $this->getBarcode(),
            'Customer' => ['name' => $this->customerName],      // времянка
            'Contractor' => ['name' => $this->contractorName],  // времянка
            'type' => $this->getType(),
            'status' => $this->getStatus(),
            'Items' => $jsonItems
        ];
    }
}
