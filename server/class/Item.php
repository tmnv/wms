<?php

// Item - это одна строка заказа с товаром. Unit - это одна единица товара.

namespace ru\trde;

class Item implements \JsonSerializable {
    //protected $Good = null;
    protected $Product = null;
    protected $price = 0;
    protected $vat = 0;
    protected $plannedQuantity = 0;
    protected $actualQuantity = 0;
    protected $packageNumber = 0;

    public function initFromWsItem(\stdClass $WsItem) {
        try {
            $this->Product = new Product();
            if (isset($WsItem->Product)) {
                $this->Product->initFromWsProduct($WsItem->Product);
            }
            $this->setPrice($WsItem->price);
            $this->setPlannedQuantity($WsItem->plannedQuantity);
            $this->setActualQuantity($WsItem->actualQuantity);
            $this->setPackageNumber($WsItem->packageNumber);

            $initComplete = true;
        }
        catch (\Exception $Ex) {
            $initComplete = false;
        }

        return $initComplete;
    }

    public function getProduct() {
        return $this->Product;
    }

    public function setProduct(Product $Product) {
        $this->Product = $Product;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getVat() {
        return $this->vat;
    }

    public function setVat($vat) {
        $this->vat = $vat;
    }

    public function getPlannedQuantity() {
        return $this->plannedQuantity;
    }

    public function setPlannedQuantity($plannedQuantity) {
        $this->plannedQuantity = $plannedQuantity;
    }

    public function getActualQuantity() {
        return $this->actualQuantity;
    }

    public function setActualQuantity($actualQuantity) {
        $this->actualQuantity = $actualQuantity;
    }

    public function getPackageNumber() {
        return $this->packageNumber;
    }

    public function setPackageNumber($packageNumber) {
        $this->packageNumber = $packageNumber;
    }

    public function jsonSerialize() {
        return [
            'Product' => $this->getProduct()->jsonSerialize(),
            'price' => $this->getPrice(),
            'vat' => $this->getVat(),
            'plannedQuantity' => $this->getPlannedQuantity(),
            'actualQuantity' => $this->getActualQuantity(),
            'packageNumber' => $this->getPackageNumber()
        ];
    }
}
