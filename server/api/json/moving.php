<?php

require_once('class/Config.php');

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

if (isset($_GET['method'])) {
    // Ничего не кешировать
    // https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
    header('Cache-Control: no-store');
    header('Content-Type: application/json; charset=utf-8');

    ini_set('soap.wsdl_cache_enabled', 0);
    ini_set('soap.wsdl_cache_ttl', 0);

    $soapClientOptions = [
    'login' => Config::REMOTE_DB_USER,
    'password' => Config::REMOTE_DB_PASSWORD,
    'cache_wsdl' => WSDL_CACHE_NONE
    ];

    $dbhost = Config::REMOTE_DB_HOST;
    $dbname = Config::REMOTE_DB_NAME;

    if ($_GET['method'] == "warehouseDocumentItem.add") {
        $postData = file_get_contents('php://input');
        $Request = json_decode($postData, false);

        /*
         * object(stdClass)#1 (6) {
  ["warehouseDocumentBarcode"]=>
  string(9) "000000005"
  ["sourceCellBarcode"]=>
  string(16) "00000015PRI01101"
  ["destinationCellBarcode"]=>
  string(16) "00000015FS101101"
  ["productBarcode"]=>
  string(9) "000419315"
  ["plannedQuantity"]=>
  int(0)
  ["actualQuantity"]=>
  int(5)
}
         */

        //use ru\trde;

        //$barcode = '';
        //if (isset($_GET['barcode'])) {
        //    $barcode = htmlspecialchars($_GET['barcode']); // 000419315
        //}
        //else {
        //    die(json_encode(['code' => "error", 'description' => "Не указан штрихкод ячейки.", 'Product' => null], JSON_UNESCAPED_UNICODE));
        //}

        try {
            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);

            $WarehouseDocumentItem = [
            "warehouseDocumentBarcode" => $Request->warehouseDocumentBarcode,
            "sourceCellBarcode" => $Request->sourceCellBarcode,
            "destinationCellBarcode" => $Request->destinationCellBarcode,
            "productBarcode" => $Request->productBarcode,
            "plannedQuantity" => $Request->plannedQuantity,
            "actualQuantity" => $Request->actualQuantity
            ];

            $result = $SoapClient->AddWarehouseDocumentItem(['WarehouseDocumentItem' => $WarehouseDocumentItem]);

            if (
            ! isset($result->return)
            || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => "error", 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            if ($result->return->code != "ok") {
                die(json_encode(['code' => "error", 'description' => $result->return->description], JSON_UNESCAPED_UNICODE));
            }

            $response = $result->return;

            //$response = [
            //    'code' => $result->return->code,
            //    'description' => $result->return->description,
            //    'Cell' => [
            //        'code' => $result->return->Cell->code,
            //        'name' => $result->return->Cell->name,
            //        'barcode' => $result->return->Cell->barcode
            //    ]
            //];

            die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => "error", 'description' => $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
    }
    else if ($_GET['method'] == "warehouseDocument.add") {
        //use ru\trde;

        $barcode = '';
        if (isset($_GET['barcode'])) {
            $barcode = htmlspecialchars($_GET['barcode']); // 000419315
        }
        else {
            die(json_encode(['code' => "error", 'description' => "Не указан штрихкод ячейки.", 'Cell' => null], JSON_UNESCAPED_UNICODE));
        }


        try {
            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);

            $result = $SoapClient->AddWarehouseDocumentByStorekeeperBarcode(['barcode' => $barcode]);

            //die(json_encode(['code' => "ok", 'description' => "", "Document" => ["number" => "0123", "storekeeper" => $barcode]], JSON_UNESCAPED_UNICODE));

            if ($result->return->code == "ok")
                die(json_encode(['code' => $result->return->code, 'description' => $result->return->description, "Document" => $result->return->Document], JSON_UNESCAPED_UNICODE));
            else
                die(json_encode(['code' => $result->return->code, 'description' => $result->return->description, "Document" => null], JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => "error", 'description' => $fault->getMessage(), "Document" => null], JSON_UNESCAPED_UNICODE));
        }
    }
    else if ($_GET['method'] == "warehouseDocument.list") {
        try {
            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);

            $result = $SoapClient->GetWarehouseDocuments();
            /*
        object(stdClass)#2 (1) {
          ["return"]=>
          object(stdClass)#3 (3) {
            ["code"]=>
            string(2) "ok"
            ["description"]=>
            string(0) ""
            ["Documents"]=>
            array(2) {
              [0]=>
              object(stdClass)#4 (3) {
                ["date"]=>
                string(19) "2019-11-03T19:18:23"
                ["number"]=>
                string(9) "000000004"
                ["type"]=>
                string(20) "Размещение"
              }
              [1]=>
              object(stdClass)#5 (3) {
                ["date"]=>
                string(19) "2019-11-04T17:33:31"
                ["number"]=>
                string(9) "000000005"
                ["type"]=>
                string(20) "Перекладка"
              }
            }
          }
        }
             */
            if (
            ! isset($result->return)
            || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => "error", 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            if ($result->return->code != "ok") {
                die(json_encode(['code' => "error", 'description' => $result->return->description], JSON_UNESCAPED_UNICODE));
            }

            $response = $result->return;

            //$response = [
            //    'code' => $result->return->code,
            //    'description' => $result->return->description,
            //    'Cell' => [
            //        'code' => $result->return->Cell->code,
            //        'name' => $result->return->Cell->name,
            //        'barcode' => $result->return->Cell->barcode
            //    ]
            //];

            if (! isset($response->Documents)) {
                die(json_encode(['code' => "ok", 'description' => "", "Documents" => []], JSON_UNESCAPED_UNICODE));
            }

            if (isset($response->Documents->date)) {
                $Document = $response->Documents;
                $response->Documents = [$Document];
            }

            die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442

            //if (
            //    isset($result->return)
            //    && isset($result->return->code)
            //) {
            //    if ($result->return->code == 'ok') {
            //        if (isset($result->return->Docs)) {
            //            if (isset($result->return->Docs->Doc->date))
            //                $Docs = [$result->return->Docs->Doc];
            //            else
            //                $Docs = $result->return->Docs->Doc;
            //        }
            //    }
            //}
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => "error", 'description' => $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
    }
    else if ($_GET['method'] == "warehouseDocument.close") {
        $barcode = '';
        if (isset($_GET['barcode'])) {
            $barcode = htmlspecialchars($_GET['barcode']);
        }
        else {
            die(json_encode(['code' => "error", 'description' => "Не указан штрихкод документа."], JSON_UNESCAPED_UNICODE));
        }

        try {
            $SoapClient = new SoapClient("https://{$dbhost}/{$dbname}/ws/wms?wsdl", $soapClientOptions);

            $result = $SoapClient->CloseWarehouseDocumentByBarcode(['barcode' => $barcode]);

            die(json_encode(['code' => $result->return->code, 'description' => $result->return->description], JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => "error", 'description' => $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
    }
    else {
        die(json_encode(['code' => "error", 'description' => ("Получен запрос на выполнение неизвестного метода " . htmlspecialchars($_GET['method']))], JSON_UNESCAPED_UNICODE));
    }
}
