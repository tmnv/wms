<?php

require_once('../../class/Config.php');

// Ничего не кешировать
// https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
header('Cache-Control: no-store');
header('Content-Type: application/json');

require_once('../../class/Order.php');
require_once('../../class/Item.php');
require_once('../../class/Product.php');
require_once('../../class/Package.php');

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

//use ru\trde;

$barcode = '';
if (isset($_GET['barcode'])) {
    $barcode = htmlspecialchars($_GET['barcode']); // 19062300001
}
else {
    die('Отсканируйте заказ!');
}

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

try {
    $soapClientOptions = [
        'login' => Config::REMOTE_DB_USER,
        'password' => Config::REMOTE_DB_PASSWORD,
        'cache_wsdl' => WSDL_CACHE_NONE
    ];

    $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

    $result = $SoapClient->GetOrder(['barcode' => $barcode]);

    //preDump($result);

    /*
object(stdClass)#2 (1) {
  ["return"]=>
  object(stdClass)#3 (3) {
    ["code"]=>
    int(0)
    ["description"]=>
    string(0) ""
    ["Order"]=>
    object(stdClass)#4 (9) {
      ["date"]=>
      string(10) "2020-01-27"
      ["number"]=>
      string(12) "123-тест"
      ["barcode"]=>
      string(11) "19120700001"
      ["Contractor"]=>
      object(stdClass)#5 (1) {
        ["name"]=>
        string(12) "Москва"
      }
      ["type"]=>
      string(39) "ДОСТАВКА (Покупателю)"
      ["status"]=>
      string(46) "В ПРОЦЕССЕ (комплектация)"
      ["Customer"]=>
      object(stdClass)#6 (1) {
        ["name"]=>
        string(21) "ООО "КоМаБи""
      }
      ["Items"]=>
      object(stdClass)#7 (1) {
        ["Item"]=>
        array(2) {
          [0]=>
          object(stdClass)#8 (6) {
            ["Product"]=>
            object(stdClass)#9 (5) {
              ["barcode"]=>
              string(9) "000419316"
              ["name"]=>
              string(31) "Маршрутизатор Asus"
              ["balance"]=>
              float(10)
              ["weight"]=>
              float(0.1)
              ["StockItems"]=>
              object(stdClass)#10 (2) {
                ["Cell"]=>
                object(stdClass)#11 (4) {
                  ["code"]=>
                  string(6) "000062"
                  ["name"]=>
                  string(11) "FS1-01-1-01"
                  ["address"]=>
                  string(11) "FS1-01-1-01"
                  ["barcode"]=>
                  string(16) "00000015FS101101"
                }
                ["quantity"]=>
                float(10)
              }
            }
            ["price"]=>
            string(3) "100"
            ["vat"]=>
            int(0)
            ["plannedQuantity"]=>
            float(1)
            ["actualQuantity"]=>
            float(2)
            ["packageNumber"]=>
            int(1)
          }
          [1]=>
          object(stdClass)#12 (6) {
            ["Product"]=>
            object(stdClass)#13 (5) {
              ["barcode"]=>
              string(9) "000419315"
              ["name"]=>
              string(21) "Сервер Asterisk"
              ["balance"]=>
              float(10)
              ["weight"]=>
              float(0.2)
              ["StockItems"]=>
              object(stdClass)#14 (2) {
                ["Cell"]=>
                object(stdClass)#15 (4) {
                  ["code"]=>
                  string(6) "000065"
                  ["name"]=>
                  string(11) "PRI-01-1-01"
                  ["address"]=>
                  string(11) "PRI-01-1-01"
                  ["barcode"]=>
                  string(16) "00000015PRI01101"
                }
                ["quantity"]=>
                float(10)
              }
            }
            ["price"]=>
            string(3) "350"
            ["vat"]=>
            int(0)
            ["plannedQuantity"]=>
            float(0)
            ["actualQuantity"]=>
            float(3)
            ["packageNumber"]=>
            int(1)
          }
        }
      }
      ["Packages"]=>
      object(stdClass)#16 (1) {
        ["Package"]=>
        object(stdClass)#17 (6) {
          ["number"]=>
          int(1)
          ["barcode"]=>
          string(16) "19120700001(001)"
          ["weight"]=>
          float(1)
          ["length"]=>
          float(0)
          ["width"]=>
          float(0)
          ["height"]=>
          float(0)
        }
      }
    }
  }
}
     */

    if (
        ! isset($result->return)
        || ! isset($result->return->code)
    ) {
        die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера', 'Order' => null], JSON_UNESCAPED_UNICODE));
    }

    if ($result->return->code !== 'ok') {
        die(json_encode(['code' => 'error', 'description' => $result->return->description, 'Order' => null], JSON_UNESCAPED_UNICODE));
    }

    //$Order = new ru\trde\Order();
    //
    //if (
    //    isset($result->return)
    //    && isset($result->return->Order)
    //) {
    //    $WsOrder = $result->return->Order;
    //
    //    $Order->initFromWsOrder($WsOrder);
    //}
    //else {
    //    die(json_encode(['code' => 1, 'description' => 'Заказ найден, но данные не получены', 'Order' => null], JSON_UNESCAPED_UNICODE));
    //}
    //
    //$response = [
    //    'code' => $result->return->code,
    //    'description' => $result->return->description,
    //    'Order' => $Order
    //];

    $Order = $result->return->Order;

    //preDump($Order);

    if (
        isset($Order->Items)
        && isset($Order->Items->Item)
    ) {
        $OrderItemList = [];
        if (is_array($Order->Items->Item)) {
            $OrderItemList = $Order->Items->Item;
        }
        else {
            $OrderItemList[] = $Order->Items->Item;
        }

        for ($i = 0; $i < count($OrderItemList); $i++) {
            $OrderItem = $OrderItemList[$i];

            if (isset($OrderItem->Product->StockItems)) {
                if (! is_array($OrderItem->Product->StockItems)) {
                    $StockItemSet = $OrderItem->Product->StockItems;

                    $StockItem = $StockItemSet;
                    $StockItemSet = [];
                    $StockItemSet[] = $StockItem;

                    $OrderItem->Product->StockItems = $StockItemSet;
                }
            }
        }

        $Order->Items = $OrderItemList;
    }
    else {
        $Order->Items = [];
    }

    if (
        isset($Order->PackageSet)
        && isset($Order->PackageSet->Package)
    ) {
        $PackageSet = [];
        if (is_array($Order->PackageSet->Package))
            $PackageSet = $Order->PackageSet->Package;
        else
            $PackageSet[] = $Order->PackageSet->Package;

        $Order->PackageSet = $PackageSet;
    }
    else {
        $Order->PackageSet = [];
    }

    die(json_encode(['code' => $result->return->code, 'description' => '', 'Order' => $Order], JSON_UNESCAPED_UNICODE));
    //die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
}
catch (SoapFault $fault) {
    die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage(), 'Order' => null], JSON_UNESCAPED_UNICODE));
}
