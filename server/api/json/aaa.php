<?php

// Описание прецедента
//
// Кладовщик приходит на Склад.
// Кладовщик подходит к компьютеру и выбирает своё имя из списка.
// Система создаёт код для Сессии со случайным значением используя функцию MD5.
// Система показывает его Кладовщику в виде QR-кода на экране монитора.
// Кладовщик сканирует телефоном или ТСД QR-код на экране компьютера склада для регистрации в системе
// Приложение отправляет полученный код Системе для подтверждения начала сессии.
// Система запоминает дату входа, код сессии и IP, с которого пришёл запрос и посылает приложению разрешение начала работы.
// Приложение переходит к главному меню.
// Кладовщик начинает работу.

require_once '../../class/Config.php';

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

if (isset($_GET['method'])) {
    // Ничего не кешировать
    // https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
    header('Cache-Control: no-store');
    header('Content-Type: application/json');

    ini_set('soap.wsdl_cache_enabled', 0);
    ini_set('soap.wsdl_cache_ttl', 0);

    if ($_GET['method'] == "storekeeper.get") {
        //use ru\trde;

        $barcode = '';
        if (isset($_GET['barcode'])) {
            $barcode = htmlspecialchars($_GET['barcode']);
        }
        else {
            die(json_encode(['code' => "error", 'description' => "Не указан номер кладовщика.", 'Storekeeper' => null], JSON_UNESCAPED_UNICODE));
        }

        try {
            $soapClientOptions = [
                'login' => Config::REMOTE_DB_USER,
                'password' => Config::REMOTE_DB_PASSWORD,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $wsdlUrl = 'https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME .'/ws/wms?wsdl';
//            preDump($wsdlUrl);

            // Если получаем ошибку "Class 'SoapClient' not found ...", то значит может быть не установлен
            $SoapClient = new SoapClient($wsdlUrl, $soapClientOptions);
//            preDump($SoapClient);

            $result = $SoapClient->GetStorekeeperByBarcode(['barcode' => $barcode]);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => "error", 'description' => 'Не получен код ответа сервера', 'Storekeeper' => null], JSON_UNESCAPED_UNICODE));
            }

            if ($result->return->code != "ok") {
                die(json_encode(['code' => "error", 'description' => $result->return->description, 'Storekeeper' => null], JSON_UNESCAPED_UNICODE));
            }

            $response = $result->return;

            //$response = [
            //    'code' => $result->return->code,
            //    'description' => $result->return->description,
            //    'Cell' => [
            //        'code' => $result->return->Cell->code,
            //        'name' => $result->return->Cell->name,
            //        'barcode' => $result->return->Cell->barcode
            //    ]
            //];

            die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => "error", 'description' => 'Исключение: ' . $fault->getMessage(), 'Storekeeper' => null], JSON_UNESCAPED_UNICODE));
        }
    }
}
