<?php

require_once('../../class/Config.php');

header('Cache-Control: no-store');
header('Content-Type: application/json');

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

if (! isset($_GET['method'])) {
    die(json_encode(['code' => 'error', 'description' => 'Укажите запрашиваемый метод'], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
}

$method = addslashes(trim($_GET['method']));

$soapClientOptions = [
    'login' => Config::REMOTE_DB_USER,
    'password' => Config::REMOTE_DB_PASSWORD,
    'cache_wsdl' => WSDL_CACHE_NONE,
    'features' => SOAP_SINGLE_ELEMENT_ARRAYS
];

switch ($method) {
    case "startPickingForOrder":
        try {
            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $postData = file_get_contents('php://input');
            $Request = json_decode($postData, false);

            $result = $SoapClient->startPickingForOrder([
                'orderBarcode' => $Request->orderBarcode,
                'pickerBarcode' => $Request->pickerBarcode
            ]);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера', 'barcode' => ''], JSON_UNESCAPED_UNICODE));
            }

            die(json_encode($result->return, JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage(), 'barcode' => ''], JSON_UNESCAPED_UNICODE));
        }
        break;
    case "finishPickingForOrder":
        try {
            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $postData = file_get_contents('php://input');
            $Request = json_decode($postData, false);

            $result = $SoapClient->finishPickingForOrder(['orderBarcode' => $Request->orderBarcode, 'pickerBarcode' => $Request->pickerBarcode]);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            die(json_encode($result->return, JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
        break;
    case "cancelPickingForOrder":
        try {
            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $postData = file_get_contents('php://input');
            $Request = json_decode($postData, false);

            $result = $SoapClient->cancelPickingForOrder(['orderBarcode' => $Request->orderBarcode, 'pickerBarcode' => $Request->pickerBarcode]);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            die(json_encode($result->return, JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
        break;
    case "addRowToPicking":
        // addRowOfProductMovement(documentBarcode, storekeeperBarcode, RowOfProductMovement)
        try {
            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $postData = file_get_contents('php://input');
            $Request = json_decode($postData, false);

            //$documentBarcode = '000000005';
            //$storekeeperBarcode = '000000001';
            //$RowOfProductMovement = [
            //    'productBarcode' => '000419315',
            //    'orderBarcode' => '19120700001',
            //    'sourceCellBarcode' => '00000015PRI01101',
            //    'destinationCellBarcode' => '00000015OTG01101',
            //    'plannedQuantity' => '3',
            //    'actualQuantity' => '3'
            //];

            $documentBarcode = $Request->documentBarcode;
            $storekeeperBarcode = $Request->storekeeperBarcode;
            $RowOfProductMovement = $Request->RowOfProductMovement;

            $result = $SoapClient->addRowToPicking([
                'documentBarcode' => $documentBarcode,
                'storekeeperBarcode' => $storekeeperBarcode,
                'RowOfProductMovement' => $RowOfProductMovement
            ]);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            die(json_encode($result->return, JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
        break;
    case "updateOrderPackageSet":
        // addRowOfProductMovement(documentBarcode, storekeeperBarcode, RowOfProductMovement)
        try {
            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $postData = file_get_contents('php://input');
            $Request = json_decode($postData, false);

            //$documentBarcode = '000000005';
            //$storekeeperBarcode = '000000001';
            //$RowOfProductMovement = [
            //    'productBarcode' => '000419315',
            //    'orderBarcode' => '19120700001',
            //    'sourceCellBarcode' => '00000015PRI01101',
            //    'destinationCellBarcode' => '00000015OTG01101',
            //    'plannedQuantity' => '3',
            //    'actualQuantity' => '3'
            //];

            $orderBarcode = $Request->orderBarcode;
            $storekeeperBarcode = $Request->storekeeperBarcode;
            $PackageSet = $Request->PackageSet;

            $result = $SoapClient->updateOrderPackageSet([
                'orderBarcode' => $orderBarcode,
                'storekeeperBarcode' => $storekeeperBarcode,
                'PackageSet' => $PackageSet
            ]);

            //preDump($result);

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            die(json_encode($result->return, JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
        break;
    case "getOrderListForPicking":
        try {
            $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

            $postData = file_get_contents('php://input');
            $Request = json_decode($postData, false);

            //$documentBarcode = '000000005';
            //$storekeeperBarcode = '000000001';
            //$RowOfProductMovement = [
            //    'productBarcode' => '000419315',
            //    'orderBarcode' => '19120700001',
            //    'sourceCellBarcode' => '00000015PRI01101',
            //    'destinationCellBarcode' => '00000015OTG01101',
            //    'plannedQuantity' => '3',
            //    'actualQuantity' => '3'
            //];

            //$Filter = [
            //    'contractor' => ''
            //];

            $Filter = $Request->Filter;

            $result = $SoapClient->getOrderListForPicking([
                'Filter' => $Filter
            ]);

            //preDump($result);
            /*
 object(stdClass)#2 (1) {
  ["return"]=>
  object(stdClass)#3 (3) {
    ["code"]=>
    string(2) "ok"
    ["description"]=>
    string(0) ""
    ["OrderList"]=>
    object(stdClass)#4 (1) {
      ["Order"]=>
      array(1) {
        [0]=>
        object(stdClass)#5 (3) {
          ["date"]=>
          string(10) "2020-04-29"
          ["number"]=>
          string(21) "tms-доставка3"
          ["barcode"]=>
          string(11) "20042400001"
        }
      }
    }
  }
}
             */

            if (
                ! isset($result->return)
                || ! isset($result->return->code)
            ) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен код ответа сервера'], JSON_UNESCAPED_UNICODE));
            }

            $return = $result->return;

            if (! isset($return->OrderList)) {
                die(json_encode(['code' => 'error', 'description' => 'Не получен список заказов'], JSON_UNESCAPED_UNICODE));
            }

            if (isset($return->OrderList->Order)) {
                if (is_array($return->OrderList->Order)) {
                    $OrderList = $return->OrderList->Order;
                    $return->OrderList = $OrderList;
                }
                else {
                    $Order = $return->OrderList->Order;
                    $return->OrderList = [$Order];
                }
            }
            else {
                $return->OrderList = [];
            }

            die(json_encode($return, JSON_UNESCAPED_UNICODE));
        }
        catch (SoapFault $fault) {
            die(json_encode(['code' => 'error', 'description' => 'Исключение: ' . $fault->getMessage()], JSON_UNESCAPED_UNICODE));
        }
        break;
    default:
        die(json_encode(['code' => 'error', 'description' => 'Метод ' . $method . ' не поддерживается.'], JSON_UNESCAPED_UNICODE));
}
