<?php

require_once('../../class/Config.php');
require_once('../../class/Good.php');

function preDump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

if (
isset($_GET['method'])
&& $_GET['method'] == "product.get"
) {
// Ничего не кешировать
// https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%8D%D1%88%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5
    header('Cache-Control: no-store');
    header('Content-Type: application/json');

    ini_set('soap.wsdl_cache_enabled', 0);
    ini_set('soap.wsdl_cache_ttl', 0);

    //use ru\trde;

    $barcode = '';
    if (isset($_GET['barcode'])) {
        $barcode = htmlspecialchars($_GET['barcode']); // 000419315
    }
    else {
        die('Отсканируйте заказ!');
    }

    try {
        $soapClientOptions = [
        'login' => 'api-cabinet',
        'password' => 'Qsc@123*Zaq1',
        'cache_wsdl' => WSDL_CACHE_NONE
        ];

        $SoapClient = new SoapClient('https://' . Config::REMOTE_DB_HOST . '/' . Config::REMOTE_DB_NAME . '/ws/wms?wsdl', $soapClientOptions);

        $result = $SoapClient->GetProduct(['barcode' => $barcode]);

        //preDump($result);
        /*
         * <pre>object(stdClass)#2 (1) {
  ["return"]=>
  object(stdClass)#3 (3) {
    ["code"]=>
    string(2) "ok"
    ["description"]=>
    string(0) ""
    ["Product"]=>
    object(stdClass)#4 (5) {
      ["owner"]=>
      string(21) "ООО "КоМаБи""
      ["barcode"]=>
      string(9) "000419315"
      ["name"]=>
      string(21) "Сервер Asterisk"
      ["balance"]=>
      int(-5)
      ["StockItems"]=>
      array(4) {
        [0]=>
        object(stdClass)#5 (2) {
          ["Cell"]=>
          object(stdClass)#6 (4) {
            ["code"]=>
            string(6) "000016"
            ["name"]=>
            string(8) "Угол"
            ["address"]=>
            string(10) "PL-01-1-01"
            ["barcode"]=>
            string(15) "00000015PL01101"
          }
          ["quantity"]=>
          float(1)
        }
        [1]=>
        object(stdClass)#7 (2) {
          ["Cell"]=>
          object(stdClass)#8 (4) {
            ["code"]=>
            string(6) "000061"
            ["name"]=>
            string(11) "OTG-01-1-01"
            ["address"]=>
            string(11) "OTG-01-1-01"
            ["barcode"]=>
            string(16) "00000015OTG01101"
          }
          ["quantity"]=>
          float(-6)
        }
        [2]=>
        object(stdClass)#9 (2) {
          ["Cell"]=>
          object(stdClass)#10 (4) {
            ["code"]=>
            string(6) "000062"
            ["name"]=>
            string(11) "FS1-01-1-01"
            ["address"]=>
            string(11) "FS1-01-1-01"
            ["barcode"]=>
            string(16) "00000015FS101101"
          }
          ["quantity"]=>
          float(8)
        }
        [3]=>
        object(stdClass)#11 (2) {
          ["Cell"]=>
          object(stdClass)#12 (4) {
            ["code"]=>
            string(6) "000065"
            ["name"]=>
            string(11) "PRI-01-1-01"
            ["address"]=>
            string(11) "PRI-01-1-01"
            ["barcode"]=>
            string(16) "00000015PRI01101"
          }
          ["quantity"]=>
          float(-8)
        }
      }
    }
  }
}
</pre>{"code":"ok","description":"","Product":{"barcode":"000419315","name":"Сервер Asterisk","owner":"ООО \"КоМаБи\"","balance":-5}}

        <pre>object(stdClass)#2 (1) {
  ["return"]=>
  object(stdClass)#3 (3) {
    ["code"]=>
    string(2) "ok"
    ["description"]=>
    string(0) ""
    ["Product"]=>
    object(stdClass)#4 (5) {
      ["owner"]=>
      string(29) "Зарипова Л.Т. ИП "
      ["barcode"]=>
      string(9) "000449929"
      ["name"]=>
      string(47) "Инкубатор НОРМА-ПК (белый)"
      ["balance"]=>
      int(24)
      ["StockItems"]=>
      object(stdClass)#5 (2) {
        ["Cell"]=>
        object(stdClass)#6 (4) {
          ["code"]=>
          string(6) "000016"
          ["name"]=>
          string(8) "Угол"
          ["address"]=>
          string(11) "POL-01-1-01"
          ["barcode"]=>
          string(16) "00000015POL01101"
        }
        ["quantity"]=>
        float(24)
      }
    }
  }
}
</pre>{"code":"ok","description":"","Product":{"barcode":"000449929","name":"Инкубатор НОРМА-ПК (белый)","owner":"Зарипова Л.Т. ИП ","balance":24,"StockItems":[]}}
         */

        if (
        isset($result->return)
        && isset($result->return->code)
        ) {
            if ($result->return->code === "ok") {
                //$Order = new ru\trde\Order();

                if (
                isset($result->return)
                && isset($result->return->Product)
                ) {
                    $WsProduct = $result->return->Product;

                    //$Order->initFromWsOrder($WsOrder);
                }
                else {
                    die(json_encode(['code' => 1, 'description' => 'Товар найден, но данные не получены', 'Product' => null], JSON_UNESCAPED_UNICODE));
                }

                $StockItems = [];

                if (isset($result->return->Product->StockItems)) {
                    if (is_array($result->return->Product->StockItems))
                        $StockItems = $result->return->Product->StockItems;
                    else if (isset($result->return->Product->StockItems->quantity))
                        $StockItems[] = $result->return->Product->StockItems;
                }

                $response = [
                'code' => $result->return->code,
                'description' => $result->return->description,
                'Product' => [
                'barcode' => $result->return->Product->barcode,
                'name' => $result->return->Product->name,
                'owner' => $result->return->Product->owner,
                'balance' => $result->return->Product->balance,
                'StockItems' => $StockItems
                ]
                ];

                //preDump($response);

                die(json_encode($response, JSON_UNESCAPED_UNICODE)); // если не ставить флаг JSON_UNESCAPED_UNICODE, то кириллица выйдет в кодах типа \u0442
            }
            else {
                die(json_encode(['code' => 1, 'description' => $result->return->description, 'Product' => null], JSON_UNESCAPED_UNICODE));
            }
        }
        else {
            die(json_encode(['code' => 1, 'description' => 'Не получен код ответа сервера', 'Product' => null], JSON_UNESCAPED_UNICODE));
        }
    }
    catch (SoapFault $fault) {
        die(json_encode(['code' => 1, 'description' => 'Исключение: ' . $fault->getMessage(), 'Product' => null], JSON_UNESCAPED_UNICODE));
    }
}
