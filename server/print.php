<?php


    $utf8 = "^XA
^CV1

";

    $zpl_large_string = "^XA
^PW600
^LL700
^FO40,200,0
^AE,N,15,10
^TBN,300,400
^FDThis is a test string to see if the text is wrapped inside the text block at all
^FS
^XZ";

    $zpl_barcode = "^XA
^FT100,550
^A0B,40,30
^FDOrder^FS

^FT250,550
^BY4
^A0N,40,30
^BCB,100,Y,N,N,A
^FD28060000000390^FS

^FX Габариты
^FT350,550
^A0B,40,30
^FDlenght: 50^FS

^FT350,370
^A0B,40,30
^FDwidth: 50^FS

^FT350,190
^A0B,40,30
^FDheight: 50^FS

^FX Вес
^FT450,550
^A0B,40,30
^FDweight: 10 kg^FS

^FX Адрес
^FT550,550
^A0B,40,30
^FBB,300,400
^XZ";

$zpl_point_barcode = "^XA
^FT250,550
^BY4
^A0N,40,30
^BCB,100,Y,N,N,A
^FD997259^FS
^XZ";

$zpl_point_barcode_land = "^XA
^FT200,550
^BY4
^A0N,40,30
^BCB,100,Y,N,N,A
^FD28060000000433
^FS
^XZ";

// 997259
// 997435

// ^FD" . iconv('UTF-8', 'CP866', 'Test Тест') . "^FS

    $print_data = "^XA
^FO 0,10
^GB632,0,2^FS
^FO0,25
^FB632,1,0,C,0
^ASN,70,70
^FDWAR INC.^FS
^FO0,100
^GB632,0,2^FS
^FO0,120
^FB632,1,0,C,0
^ASN,60,60
^FDGoose^FS
^FO0,180
^FB632,1,0,C,0
^ASN,60,60
^FDWild^FS
^FO0,240
^GB632,0,2^FS
^FO120,260
^BY2
^BCN,70,N,N,N
^FDSECRECTCODE^FS -
^XZ";

    $list = "^XA
^HWE:*.*
^XZ";

    $pgl = "^CONFIG
SFCC;94
END

^CONFIG
LEFT MARGIN;5
END

^PAPER;LENGTH 80
^PAPER;WIDTH 60
^PAPER;PORTRAIT

^CREATE;ds-label
FONT;FACE 92250
ALPHA
AF2;100;DARK;POINT;8;6;45;40
AF4;100;DARK;POINT;13;6;45;40
AF60;100;DARK;POINT;18;6;45;40
AF61;100;POINT;22;6;30;30
STOP

FONT;FACE 93779
ALPHA
POINT;5;6;10;0;Some data 1:
POINT;10;6;10;0;Some data 2:
POINT;15;6;10;0;Delivery Address:
POINT;20;6;10;0;NSC Name:
POINT;24;6;10;0;CODE barcode:
POINT;34;6;10;0;CODE number:
AF63;17;DARK;POINT;37;6;46;23
POINT;39;6;10;0;PID:
AF64;26;DARK;POINT;41;6;28;14
POINT;43;6;10;0;Label Code:
AF65;8;POINT;44;6;15;0
POINT;46;6;10;0;Date:
AF66;10;POINT;47;6;15;0
STOP

BARCODE
C3/9;X1;H10;BF10;17;26;8
STOP
END

^EXECUTE;ds-label
^AF2;ALVSBORGSH. YYY-XXX
^AF4;IMMINGHAM PORT IMPORTS
^AF60;THE CITY
^AF61;Great Britain
^BF10;11111111111111111
^AF63;11111111111111111
^AF64;AAAAAAY1T1MMA7290B11111111
^AF65;0067PXXX
^AF66;2008-10-15
^NORMAL

^CONFIG RESET END";

// ЗАГРУЗИТЬ ФОНТЫ
// https://shouldertheboulder.com/article/downloading-fonts-to-printers-the-easy-way

// Онлайн-этикетка для ZPL
// http://labelary.com/viewer.html

// РАБОТАЕТ

//// 1. ПОЛУЧИТЬ РАЗМЕР ФАЙЛА
//$filename = 'PTAstraSans-Regular.ttf';  // Шрифт без засечек
////$filename = 'PTAstraSerif-Regular.ttf';  // Шрифт с засечками
//
//echo 'filesize of ' . $filename . ' is ' . filesize($filename) . ' (в байтах).<br />';
//
//// Получить файл как строку в шестнадцатеричном формате.
//// https://stackoverflow.com/questions/7128469/php-viewing-a-file-as-hex
//
//$hex = unpack("h*", file_get_contents($filename));
//
//var_dump($hex);
//$hex = strtoupper(current($hex));
//
//echo 'HEX: ' . $hex;

//echo 'HEX: ' . strtoupper($hex);

$data = 'A B C - А Б В';
//$data = mb_convert_encoding('A B C - А Б В', "Windows-1251");

// ^CI - Select International Set

//3AKA3
//BEC// Транслитерация строк.


try {
//    $font_and_data = "^XA
//~DUR:ASTRSANS,114808,$hex
//^FT50,100
//^CI28
//^A@,20,20,ASTRSANS
//^FD$data^FS
//^XZ";

    $work = "^XA
^CF0,20
^CI28
^FDТестирование^FS
^XZ";

    $print_dir = "^XA
^HWR:*.*
^XZ";

// ^FT - Field Typeset
// Определяет позицию поля относительно позиции метки, определённой командой ^LH.
// ^FTx,y
// x - смещение по оси x (в точках)
// y - смещение по оси y (в точках)

// ^BY - Bar Code Field Default
// Используется для указания размеров и соотношения штрихов.
// ^BYw[,r,h]
// w (width) - ширина штриха
// r (ratio) - отношение толстого штриха к тонкому (по умолчанию 3)
// h (height) - высота штриха

// ^A - Scalable/Bitmapped Font
// ^A - определяет шрифт, используемый текстовым полем. Определяет шрифт дл текущего поля ^FD. Шрифт, определённый ^A
// используется только один раз и если значение ^A не будет указано снова, то будет использоваться значение по умолчанию
// для ^CF
// ^Af[,o,h,w]
// f (font) - указывает на шрифт в принтере (загруженный, EPROM, ранее сохранённые, A-Z0-9)
// 0 (orientation) - N (normal) - 0, R (rotated) - 90, I (inverted) - 180, B (read from bottom up) - 270. (по умолчанию берётся из ^FW)
// h (height) - высота символа в точках (по умолчанию берётся из ^CF)
// w (width) - ширина символа в точках (по умолчанию берётся из ^CF)

// ^BC - Code 128 Bar Code (Subsets A, B, and C)
// Штрихкод code128. Используется для цифробуквенной идентификации продукции.
// ^BCo,h,f,g,e,m
// o (orientation) - N (0), R (90), I (180), B (270)
// h (height) - высота штрихкода (в точках)
// f - печатать ли интерпретируемую строку Y (да), N (нет) (по умолчанию Y)
// g - печатать интерпретируемую строку над штрихкодом Y (да), N (нет) (по умолчанию N)
// e - печатать контрольный символ Y (да), N (нет) (по умолчанию N)
// m (mode) - режим N (режим не выбран), U (UCC), A (automatic), D (UCC/EAN) (по умолчанию N)

// ^FD Field Data
//

    $fp = pfsockopen("192.168.4.56", 9100);
    ////fputs($fp, $print_data);
    ////fputs($fp, $zpl_large_string);
    fputs($fp, $zpl_point_barcode_land);
    ////fputs($fp, $list);
    ////while (($buffer = fgets($fp, 4096)) !== false) {
    ////    echo $buffer;
    ////}
    ////$buffer = fgetc($fp, 4096);
    ////while (false !== ($char = fgetc($fp))) {
    ////    echo "$char\n";
    ////}
    fclose($fp);
    //
    ////echo $buffer;
    //echo 'Successfully Printed';
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
