const WebSocket = require('ws')

let port = 3000

const server = new WebSocket.Server({port: port})

server.on('connection', ws => {
    ws.send('Добро пожаловать в чат КНД')

    ws.on('message', message => {
        if (message === 'exit') {
            ws.close()
        }
        else {
            server.clients.forEach(client => {
                if (client.readyState === WebSocket.OPEN) {
                    client.send(message)
                }
            })
        }
    })
})

console.log(`Сервер для чата слушает порт ${port}.`)
