<?php

require_once 'class/Config.php';

// Получаем из GET код контрагента и пытаемся его идентиицировать
// Если контрагент не опознан, то возвращаем ошибку авторизации
// Ключ Гермес - 10254B5C8CCC643EE150B48044C37D50
if (
    ! isset($_GET['action'])
    || empty($_GET['action'])
    || (
        $_GET['action'] != 'printOrderLabel'
        && $_GET['action'] != 'printCellLabel'
    )
) {
    //returnError('В запросе не указан параметр \'key\', необходимый для идентификации.');
    //die('Не указано желаемое действие');
}

$postData = file_get_contents('php://input');
$Request = json_decode($postData, false);

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'printOrderLabel') {
    }

    if ($_GET['action'] == 'printCellLabel') {

        header("Location: print_zpl.php");
        exit;
    }
}

// Все команды PGL вводятся в верхнем регистре.
// Все параметры разделяются символом ";". Без пробелов между параметрами.

// Любой штрихкод

$barcode_er = "~CREATE;128B
BOX
2;1.5;10;5;20
STOP
ALPHA
6;2;2;2;*3897*
10;2;0;0;*Sender*
11;2;0;0;*Receiver*
12;2;0;0;*Phone*
13;2;0;0;*Address*
STOP
BARCODE
C128B;CCW;DARK;4;35
*1907280001*
PDF;B
STOP
END
~EXECUTE;128B;1
~NORMAL";

$barcode_order = "~CREATE;128B
BOX
2;1.5;10;5;20
STOP
ALPHA
3;11;0;0;*1/2*
6;2;2;2;*3897*
10;2;0;0;*Sender*
AF1;100;10;10;0;0;
11;2;0;0;*Receiver*
AF2;100;11;10;0;0;
12;2;0;0;*Phone*
AF3;100;12;10;0;0;
13;2;0;0;*Address*
AF4;100;13;10;0;0;
STOP
BARCODE
C128B;CCW;BF1;DARK;4;40
PDF;B
STOP
END
~EXECUTE;128B;1
~AF1;
~NORMAL";

$barcode_ex = "~CREATE;IGPFORM;864
 SCALE;CHAR;6;10
 LFORM6;72
 BOX
 2;35;16;53;61
 STOP
 CORNER
 3;30;13;57;64;5;7
 STOP
 HORZ
 1;40;16;61
 1;45;16;61
 1;49;16;61
 STOP
 VERT
 1;49;40;45
 1;49;49;53
 STOP
 ALPHA
 31;22;0;0;*SPE Systemhaus GmbH*
 32;22;0;0;*Waldstr. 7*
 33;22;0;0;*63150 Heusenstamm*
 35.9;17;0;0;*SERIAL NUMBER*
 40.3;17;0;0;*PART NUMBER*
 40.3;50;0;0;*MFG. DATE*
 45.3;17;0;0;*DESCRIPTION*
 49.3;17;0;0;*INTERFACE*
 49.3;50;0;0;*VERSION*
 C12;31;16;0;0;*FROM:*
 C15;54;26;0;0;*Call SPE Systemhaus GmbH for*
 C15;55;34;0;0;*06106 860563*
 AF1;7;38.7;19.3;3;3
 AF2;11;43;20;2;2
 AF3;31;47.3;20;2;1
 AF4;33;51.3;20;2;1
 STOP
 BARCODE
 C3/9;H8;BF1;5;DARK;35.7;39
 STOP
 END
 ~EXECUTE;IGPFORM
 ~AF1;*49114*
 ~AF2;*106772-902*
 ~AF3;*INTELLIGENT GRAPHICS PROCESSOR*
 ~AF4;*PARALLEL-CENTRONICS*
 ~BF1;*49114*
 ~NORMAL";

//$barcode = "~CREATE;ORDER
//ISET;‘UTF8’
//ALPHA
//5;2;0;0;*Проверка UTF-8*
//STOP
//BOX
//1;1.6;15;5;25
//STOP
//BARCODE
//C128B;CCW;DARK;4;30
//*2003280001*
//PDF;B
//STOP
//END
//~EXECUTE;ORDER;1
//~NORMAL";

//$barcode = "~CREATE;ORDER
//ALPHA
//AF1;10;5;2;0;0
//STOP
//END
//~EXECUTE;ORDER;1
//~AF1;Check UTF-8
//~NORMAL";

//$barcode = "~CREATE;ORDER
//BARCODE
//C128B;CCW;BF1;15;DARK;3;33
//PDF;B
//STOP
//END
//~EXECUTE;ORDER
//~BF;*1907280001*
//~NORMAL";

// Горизонтальная этикетка
$barcode_hor = "~CREATE;C39
BARCODE
C3/9;DARK;5;10
*01301*
PDF
STOP
END
~EXECUTE;C39;1
~NORMAL";

// Вертикальная этикетка
$barcode = "~CREATE;C39
BARCODE
C3/9;VSCAN;H14;DARK;3;15
*01301*
PDF
STOP
END
~EXECUTE;C39;1
~NORMAL";

try {
    $fp = pfsockopen("192.168.5.58", 9100);

    if ($_GET['action'] == 'printOrderLabel') {
        fputs($fp, $zpl_barcode);
    }
    else if ($_GET['action'] == 'printCellLabel') {
        fputs($fp, $zpl_cell);
    }
    else {
        fputs($fp, $barcode);
    }

    fclose($fp);
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

function transliterate($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

//$address = transliterate('123456, г. Москва, пр-д Серебрякова, д. 2, стр. 1а');
$address = '123456, г. Москва, пр-д Серебрякова, д. 2, стр. 1а';



?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Печать этикеток</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="application/javascript" src="js/audio.js"></script>
    <script type="application/javascript"></script>
</head>
<body>
<a class="button" href="print_zpl.php?action=printCellLabel">Печать этикеток для ячеек</a>
</body>
</html>
