document.addEventListener('load', event => {
    console.log('RTC init')
})

let InfoElement;
let InfoTimeout;
let rtcTask;
let taskStack = [];

let wsConnection;

const ws_connect = () => {
    let configString = localStorage.getItem('config')

    if (! configString) {
        return
    }

    let config = JSON.parse(configString)

    try {
        wsConnection = new WebSocket(config.webSocketUrl)
    }
    catch (ex) {
        console.log(ex.message)
    }
}

const ws_getConnection = () => {
    if (wsConnection)
        return wsConnection
    else
        return ws_connect()
}

const rtc_init = () => {
    ws_connect();

    if (! wsConnection) {
        //todo Надо сообщать пользователю о невозможности подключиться к вебсокету
        if (showError)
            showError('Не могу подключиться к вебсокету. Сообщите администратору.')
        return
    }

    InfoElement = document.createElement('div')
    InfoElement.style.display = 'none'
    InfoElement.style.position = 'absolute'
    InfoElement.style.top = '5px'
    InfoElement.style.left = '5px'
    InfoElement.style.backgroundColor = '#f26b5b'
    InfoElement.style.color = '#fff'
    InfoElement.style.borderRadius = '5px'
    InfoElement.style.padding = '10px'
    InfoElement.style.border = '1px solid black'
    InfoElement.style.zIndex = 100
    InfoElement.style.width = 'calc(100% - 10px)'

    InfoElement.addEventListener('click', event => {
        event.target.style.display = 'none'

        if (
            rtcTask
            && rtcTask.data
            && rtcTask.data.order
        ) {
            localStorage.setItem('picking_candidateOrderBarcode', rtcTask.data.order.barcode)
            location = 'picking.php'

            //rtcTask.executor = CurrentStorekeeper

            //wsConnection.send(JSON.stringify(rtcTask))
            taskStack.push(rtcTask)

            localStorage.setItem('candidateTask', JSON.stringify(rtcTask))

            rtcTask = undefined
        }
    })

    document.body.appendChild(InfoElement)

    wsConnection.onopen = () => {
        console.log('Сервер RTC подключен')
    }

    wsConnection.onmessage = message => {
        //if (InfoTimeout) {
        //    clearTimeout(InfoTimeout)
        //}

        console.log(message)
        let task

        try {
            task = JSON.parse(message.data)
            // ЗАГЛУШКА
            task.code = 'ok'

            rtcTask = task
            if (
                rtcTask.status === 'inProgress'
                || rtcTask.status === 'done'
                || rtcTask.status === 'canceled'
            ) {
                return
            }

            console.log(typeof task, task)

            if (task.code === 'ok') {
                let order = rtcTask.data.order

                InfoElement.innerHTML = 'СРОЧНО! Самовывоз ' + order.barcode
                try {
                    console.log(audioContext)
                    //if (audioContext)
                        beep(100, 520, 200)
                }
                catch (ex) {
                    console.log(ex.error)
                }
            }
            else {
                InfoElement.innerHTML = 'Произошёл сбой: ' + task.description
            }
            InfoElement.style.display = 'block'
        }
        catch (ex) {
            task = {
                code: 'error',
                description: ex.message,
                order: '',
                operation: ''
            }
        }
    }

    wsConnection.onclose = event => {
        console.log('Сервер RTC отключен')
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', event.reason)
        console.log(event)  // CloseEvent
    }
}
