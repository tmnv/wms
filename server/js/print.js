const printOrder = function (Order) {
    try {
        if (!Order) {
            Order = CurrentOrder;
        }

        printOrderLabel(Order);
    }
    catch (Exception) {
        showError(Exception.message);
    }
};

const printOrderLabel = function (Order) {
    try {
        if (! Order) {
            Order = CurrentOrder;
        }

        sendOrderToLabelPrinterPrintServer(Order);
    }
    catch (Exception) {
        showError(Exception.message);
    }
};

/*
При попытке печати выпадало исключение:
Mixed Content: The page at 'https://esb.trde.ru/wms/picking.php' was loaded over HTTPS, but requested an insecure
XMLHttpRequest endpoint 'http://192.168.43.66/print_order_zpl.php?action=printOrderLabel'. This request has been
blocked; the content must be served over HTTPS.
 */
const sendOrderToLabelPrinterPrintServer = Order => {
    let configString = localStorage.getItem('config')

    if (! configString) {
        showError('Настройки серверов не указаны.')
        return
    }

    let config = JSON.parse(configString)

    if (
        ! config.printServerUrl
        || config.printServerUrl === ''
    ) {
        showError('Укажите в настройках путь к серверу принтера для печати этикеток.')
        return
    }

    if (config.printServerUrl === 'test') {
        showError('Имитация печати этикеток.')
        return
    }

    try {
        var XHR = new XMLHttpRequest();
        XHR.open('POST', `${config.printServerUrl}/print_order_zpl.php?action=printOrderLabel`)
        XHR.send(JSON.stringify(Order));
        XHR.onload = function () {
            if (XHR.status != 200) {
                showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
            }
            else {
                var Response = JSON.parse(XHR.responseText);

                if (
                    Response.code
                    && Response.code !== "ok"
                ) {
                    showError(Response.description);
                    return;
                }

                console.log("Этикетки по заказу напечатаны.");
            }
        };
        XHR.onerror = function (event) {
            console.log('При отправке заказа на печать произошла ошибка:<br />');
            console.log(event.target.status);
        };
    }
    catch (Exception) {
        showError(Exception.message);
    }
};
