const getTaskStack = () => {
    let taskStackString = localStorage.getItem('taskStack')

    console.log(taskStackString)

    if (! taskStackString) {
        console.log('инициализация стека')
        taskStackString = '[]'
        localStorage.setItem('taskStack', taskStackString)
    }

    return JSON.parse(taskStackString)
}

const pushTaskToStack = task => {
    let taskStack = getTaskStack()
    taskStack.push(task)
    localStorage.setItem('taskStack', JSON.stringify(taskStack))
}

const popTaskFromStack = () => {
    let taskStack = getTaskStack()
    let task = taskStack.pop()
    localStorage.setItem('taskStack', JSON.stringify(taskStack))

    return task
}
