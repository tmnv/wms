
function findCellByBarcode(barcode) {
    console.log("Ищем данные ячейки по штрихкоду " + barcode + ".");
    //TODO: Включить анимацию поиска.

    // Искать ячейку в локальной базе данных.
    findCellInLocalDatabaseByBarcode(barcode);
}

function findCellInLocalDatabaseByBarcode(barcode) {
    console.log("Ищем данные по ячейке в локальной базе данных.");
    var tx = db.transaction(["cells"], "readonly");
    var cells = tx.objectStore("cells");

    var request = cells.get(barcode);

    var Cell;

    request.onsuccess = function (event) {
        console.log("Запрос данных по ячейке успешно выполнен.");
        Cell = event.target.result;
    };

    request.onerror = function(event) {
        console.log("При поиске ячейки в локальной базе данных произошла ошибка.");
        console.log(event);
        showError("При поиске ячейки в локальной базе данных произошла ошибка.");
    };

    tx.oncomplete = function (event) {
        console.log("Транзакция поиска данных ячейки в локальной базе данных успешно завершена.");

        // Если запись ячейки найдена, то используем её, иначе делаем запрос к внешней базе данных.
        if (Cell) {
            console.log("Данные по ячейке:");
            console.log(Cell);

            handleCell(Cell);
        }
        else {
            console.log("Данные по ячейке в локальной базе не получены.");

            // Если запрос ячейки производится при её открытии, то тогда штрихкод текущей ячейки ещё не установлен.
            // Поэтому ищем данные по ячейке во внешней базе.
            if (currentCellBarcode == null) {
                findCellInRemoteDatabaseByBarcode(barcode);
            }
            else {
                showError("Полученный штрихкод не соответствует ни текущему товару, ни открытой ячейке.");
            }
        }
    };
}

function findCellInRemoteDatabaseByBarcode(barcode) {
    console.log("Ищем данные по ячейке во внешней базе данных.");
    var Cell = null;
    var XHR = new XMLHttpRequest();

    XHR.open('GET', 'cell.php?barcode=' + barcode + '&method=cell.get');

    XHR.send();

    XHR.onload = function () {
        if (XHR.status != 200) {
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            console.log(Response);

            if (Response.code !== "ok") {
                showError(Response.description);

                return;
            }

            console.log("Данные по ячейке полученные из внешней базы:");
            Cell = Response.Cell;
            console.log(Cell);

            // Необходимо сохранить ячейку в локальной базе
            saveCellInLocalDatabase(Cell);
        }
    };
}

function saveCellInLocalDatabase(Cell) {
    console.log("Сохраняю полученные данные ячейки в локальной базе данных.");

    var tx = db.transaction(["cells"], "readwrite");
    var cells = tx.objectStore("cells");

    var request = cells.put(Cell);

    request.onerror = function (event) {
        console.log(event);
        showError("При попытке записи ячейки произошла ошибка.");
    };

    tx.oncomplete = function (event) {
        console.log("Транзакция сохранения данных в локальной базе успешно выполнена.");
        handleCell(Cell);
    };
}
