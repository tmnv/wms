
function findProductByBarcode(barcode) {
    console.log("Начало поиска данных о товаре.");

    //TODO: Возможна ситуация, когда сканируют другой товар, не закончив с текущим. Продумать что делать в такой ситуации.
    // Варианты:
    // - закрыть ячейку с предыдущим товаром и переоткрыть её с новым товаром
    // - выдать ошибку (пока работает это вариант)

    // Если идёт процесс инициализаии значений, то штрихкод текущего товара может быть указан, а сам текущий товар нет.
    if (initializationInProgress) {
        console.log("Инициирую товар по его штрихкоду.");
        findProductInLocalDatabaseByBarcode(barcode);
    }
    // Если штрихкод текущего товара не указан, то это первое сканирование товара после открытия ячейки.
    else if (currentProductBarcode == null) {
        console.log("Не нашёл текущий штрихкод товара. Похоже, что это первый товар.");

        findProductInLocalDatabaseByBarcode(barcode);
    }
    // Если полученный штрихкод равен текущему штрихкоду товара, то повторно искать его в базе не требуется.
    else if (currentProductBarcode == barcode) {
        console.log("Продолжаю перемещать товар " + currentProduct.name + ".");
        handleProduct(currentProduct);
    }
    // Иначе это должен быть только штрихкод ячейки, которую хотят закрыть.
    //else if (currentCellBarcode != barcode) {
    //    showError();
    //}
    else {
        console.log("Похоже, что это попытка закрыть ячейку. Перехожу к поиску ячейки.");
        findCellByBarcode(barcode);
    }
}

function findProductInLocalDatabaseByBarcode(barcode) {
    console.log("Производится поиск данных о товаре в локальной базе.");
    var tx = db.transaction(["products"], "readonly");
    var products = tx.objectStore("products");

    var request = products.get(barcode);

    var Product;

    request.onsuccess = function (event) {
        console.log("Завершил поиск данных о товаре в локальной базе данных.");
        Product = event.target.result;
    };

    request.onerror = function(event) {
        console.log(event);
        showError("При поиске товара в локальной базе данных произошла ошибка.");
    };

    tx.oncomplete = function (event) {
        console.log("Транзакция поиска данных о товаре в локальной базе данных успешно завершена.");
        // Если запись товара найдена, то используем её, иначе делаем запрос к внешней базе данных.
        if (Product) {
            console.log("Данные о товаре получены.");
            console.log(Product);

            handleProduct(Product);

            //currentProduct = Product;
            //
            //if (initializationInProgress) {
            //    console.log("Инициализация данных продолжается.");
            //
            //    currentCellItemAvailableQuantity = getSourceCellItemAvailableQuantityForProductBarcode(barcode);
            //
            //    initCurrentCellItemQuantity();
            //}
            //else {
            //    handleProduct(Product);
            //}
        }
        else {
            console.log("Данные о товаре в локальной базе данных не найдены.");
            findProductInRemoteDatabaseByBarcode(barcode);
        }
    };
}

function findProductInRemoteDatabaseByBarcode(barcode) {
    console.log("Поиск товара во внешней базе данных.");
    var Product = null;
    var XHR = new XMLHttpRequest();

    //XHR.open('GET', 'product.php?barcode=' + barcode + '&method=product.get&testing');
    XHR.open('GET', 'product.php?barcode=' + barcode + '&method=product.get');

    console.log("Отправляю запрос к внешней базе данных.");
    try {
        XHR.send();
    }
    catch (error) {
        console.log("XHR error");
        console.log(error);
    }

    XHR.onload = function () {
        if (XHR.status != 200) {
            console.log(XHR);
            showError('ОШИБКА XHR: ' + XHR.status + ': ' + XHR.statusText);
        }
        else {
            var Response = JSON.parse(XHR.responseText);

            if (
                Response.code !== "ok"
                && Response.code !== 0
            ) {
                console.log("Товар во внешней базе данных не найден.");
                showError(Response.description);

                return;
            }

            console.log("Товар найден.");
            console.log("Ответ от сервера:");
            console.log(Response);
            Product = {
                "name": Response.Product.name,
                "barcode": Response.Product.barcode
            };
            console.log("Сохраняю:");
            console.log(Product);

            // Необходимо сохранить товар в локальной базе
            saveProductInLocalDatabase(Product);
        }
    };

    // Ищем товар в локальной базе
    // Если не нашли, то ищем во внешней базе
    // Если не нашли, то, возможно, это штрихкод ячейки, которую хотят закрыть.

    // Заглушка на время тестирования
    //if (barcode == '000419315')
    //    //Product = barcode;
    //    Product = {"name": "Сервер Asterisk", "barcode": barcode};
    //else if (barcode == '000419316') {
    //    Product = {"name": "Маршрутизатор Asus", "barcode": barcode};
    //}
    //
    //if (Product) {
    //    saveProductInLocalDatabase(Product);
    //}
    //else {
    //    showError("Товар не найден.");
    //}
}

function saveProductInLocalDatabase(Product) {
    console.log("Сохранение товара в локальной базе данных.");

    var tx = db.transaction(["products"], "readwrite");
    var products = tx.objectStore("products");

    var request = products.put(Product);

    request.onerror = function (event) {
        console.log(event);
        showError("При попытке записи товара произошла ошибка.");
    };

    tx.oncomplete = function (event) {
        console.log("Товар сохранён в локальной базе данных.");
        handleProduct(Product);
    };
}
