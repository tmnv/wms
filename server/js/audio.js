// Генерация звуков
//
// Примеры:
// beep(100, 520, 200) - Стандартный сигнал сканера.
// beep(999, 220, 400) - Сигнал ошибки.

// https://odino.org/emit-a-beeping-sound-with-javascript/
// Хром ругается на то, что контекст загрузился до полной загрузки страницы
let audioContext = new AudioContext()    // Браузеры ограничивают количество аудио контекстов, поэтому лучше использовать их повторно.

function beep(vol, freq, duration) {
    //if (! audioContext)
    //    audioContext = new AudioContext()

    var oscillator = audioContext.createOscillator();
    var gain = audioContext.createGain();
    oscillator.connect(gain);
    oscillator.frequency.value = freq;
    oscillator.type = "square";
    gain.connect(audioContext.destination);
    gain.gain.value = vol * 0.01;
    oscillator.start(audioContext.currentTime);
    oscillator.stop(audioContext.currentTime + duration * 0.001);
}

function playScannerBeep() {
    beep(100, 520, 200)
}

function playErrorBeep() {
    beep(999, 220, 400)
}
