// TODO: В коде класса есть части, которые создают проблемы при работе на ТСД. Смотри комменты.
class Task {
    constructor(obj) {
        obj = obj || {};

        this.id = obj.id || this.generateId();
        this.dateOfCreation = obj.dateOfCreation ? new Date(obj.dateOfCreation) : new Date();
        this.dateOfChange = obj.dateOfChange ? new Date(obj.dateOfChange) : this.dateOfCreation;
        this.data = obj.data || {
            'order': {
                'barcode': ''
            }
        };
        // TODO: Убрать хардкод.
        this.operation = 'Комплектация';
        this.executor = obj.executor || {
            'name': ''
        };
        this.status = obj.status || 'new';
    }

    generateId() {
        let date = new Date()

        // TODO: ТСД не воспринимает padStart().
        return '' +
            date.getFullYear() +
            (date.getMonth() + 1).toString().padStart(2, '0') +
            date.getDate().toString().padStart(2, '0') +
            date.getHours().toString().padStart(2, '0') +
            date.getMinutes().toString().padStart(2, '0') +
            date.getSeconds().toString().padStart(2, '0') +
            date.getMilliseconds()
    }

    setOrderBarcode(barcode) {
        this.data.order.barcode = barcode;
    }

    getFormattedDateOfCreation() {
        return Common.formatDate(this.dateOfCreation);
    }

    getFormattedDateOfChange() {
        // let formattedDate = Common.formatDate(this.dateOfChange);
        // console.log(formattedDate);
        // let totalSeconds = Math.round((this.dateOfChange - this.dateOfCreation) / 1000);
        // console.log('totalSeconds', totalSeconds);
        // let seconds = totalSeconds % 60;
        // console.log('seconds', seconds)
        // let minutes = Math.trunc(totalSeconds / 60);
        // console.log('minutes', minutes)
        //
        // return `${formattedDate} (${minutes} мин ${seconds} сек)`;

        return Common.formatDate(this.dateOfChange);
    }

    toJSON() {
        return {
            "id": this.id,
            "dateOfCreation": this.dateOfCreation,
            "dateOfChange": this.dateOfChange,
            "data": this.data,
            "executor": this.executor,
            "status": this.status
        };
    }
}
