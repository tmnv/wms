<?php

/**
 * Приём запроса на получение текущего веса грузового места, которое размещено на весовой
 * платформе. В данный момент используются весы от компании Мидл.
 */

$firstMicrotime = microtime(true);

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once('class/SerialPort.php');
require_once('class/Middle2Driver.php');

function makeXmlStringResponse($status, $weight, $error, $profilingData = '') {
    // Для работы с DOM нужен установленный модуль php-xml.
    $DOM = new DOMDocument('1.0', 'UTF-8');

    $ResponseElement = $DOM->appendChild($DOM->createElement('Response'));

    $statusElement = $DOM->createElement('status');
    $statusElement->appendChild($DOM->createTextNode($status));

    $ResponseElement->appendChild($statusElement);

    $weightElement = $DOM->createElement('weight');
    $weightElement->appendChild($DOM->createTextNode($weight));

    $ResponseElement->appendChild($weightElement);

    $errorElement = $DOM->createElement('error');
    $errorElement->appendChild($DOM->createTextNode($error));

    $ResponseElement->appendChild($errorElement);

    $profilingElement = $DOM->createElement('profilingData');
    $profilingElement->appendChild($DOM->createTextNode($profilingData));

    $ResponseElement->appendChild($profilingElement);

    $DOM->formatOutput = true;

    return $DOM->saveXML();
}

function makeJsonStringResponse($status, $weight, $error, $profilingData = '') {
    $response = [
        "status" => $status,
        "weight" => $weight,
        "error" => $error,
        "profilingData" => $profilingData
    ];

    return json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}

header("Access-Control-Allow-Origin: *");

try {
    $device = "/dev/ttyUSB0";

$profilingData = 'before set serial port ' . (microtime(true) - $firstMicrotime);

    $Port = new SerialPort($device);

$profilingData = 'before configure serial port ' . (microtime(true) - $firstMicrotime);
// Время съедается здесь!

    $Port->configure();

$profilingData = 'before set driver ' . (microtime(true) - $firstMicrotime);

    $Driver = new Middle2Driver($Port);

$profilingData = 'before weighing ' . (microtime(true) - $firstMicrotime);

    $weight = $Driver->getWeight();
    $errors = $Driver->getErrors();
    $status = count($errors) == 0 ? 'ok' : 'error';
    $error = implode(';', $errors);

$profilingData = 'after weighing ' . (microtime(true) - $firstMicrotime);

    if (
        isset($_GET['type'])
        && $_GET['type'] == 'json'
    ) {
        header('Content-Type: application/json');
        echo makeJsonStringResponse($status, $weight, $error, $profilingData);
    }
    else {
        header('Content-Type: application/xml');
        echo makeXmlStringResponse($status, $weight, $error, $profilingData);
    }
}
catch (Exception $Ex) {
    if (
        isset($_GET['type'])
        && $_GET['type'] == 'json'
    ) {
        header('Content-Type: application/json');
        echo makeJsonStringResponse('error', 0, $Ex->getMessage());
    }
    else {
        header('Content-Type: application/xml');
        echo makeXmlStringResponse('error', 0, $Ex->getMessage());
    }
}
