<?php

class SerialPort {
    protected $errors = [];
    
    protected $device = "";
    protected $speed = 9600;
    protected $startBitsLenght = 1;
    protected $dataBitsLength = 8;
    protected $parity = false;
    protected $stopBitsLength = 1;
    
    protected $pointer = false;
    
    public function __construct($device) {
        $this->device = $device;
    }
    
    public function addError($description) {
        $this->errors[] = $description;
    }
    
    public function getErrors() {
        return $this->errors;
    }
    
    public function configure() {
        //return exec('stty -F ' . $this->device . ' ' . $this->speed . ' -parenb cs' . $this->dataBitsLength . ' -cstopb time 2');
    }
    
    public function open() {
        if (! file_exists($this->device)) {
            $this->addError('Серийный порт ' . $this->device . ' отстуствует. Проверьте кабель подключающий весы к хосту.');
            
            return;
        }
        
        if ($this->pointer == false) {
            $this->pointer = @fopen($this->device, 'r+b');   // r+ - открывает файл на чтение и запись. b - передача данных в бинарном виде.
            
            if ($this->pointer == false)
                $this->addError('Невозможно открыть серийный порт. Обратитесь к системному администратору.');
        }
    }

    public function isOpened() {
        return $this->pointer != false;
    }
    
    public function read() {
        //var_dump($this); $this->close(); return '';
        $result = "";
        if ($this->pointer != false) {
            //while ($buffer = fgets($this->pointer) !== false) {
            //    $result += $buffer;
            //}
            $result = fgets($this->pointer);
        }
    
        return $result;
    }
    
    public function write($binaryString) {
        if ($this->pointer != false)
            @fwrite($this->pointer, $binaryString);
    }
    
    public function close() {
        if ($this->pointer != false)
            @fclose($this->pointer);
    }
}
