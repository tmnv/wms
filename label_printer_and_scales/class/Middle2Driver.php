<?php

class Middle2Driver {
    const ACTION_GET_WEIGHT = 0x0a;
    const ACTION_SET_TARE = 0x0c;
    const ACTION_SET_ZERO = 0x0d;
    const ACTION_GET_STATUS = 0x0e;
    
    protected $errors = [];
    
    protected $Port = null;
    
    public function __construct($Port) {
        if ($Port instanceof SerialPort)
            $this->Port = $Port;
        else
            $this->addError('Полученное значение не является экземпляром SerialPort.');
    }
    
    // В командом режиме (П5 = 4) пока не получается считываться вес. Перевёл весы в непрерывную передачу после стабилизации веса (П5 = 3).
    public function getWeight() {
        $Port = $this->Port;
        $weight = 0;
        
        //echo 'Открываю порт.<br />';
        $Port->open();
        //var_dump($Port);
        
        if ($Port->isOpened()) {
            // Командный режим весов отключен.
            //echo 'Запрашиваю вес.<br />';
            //$Port->write(ACTION_GET_WEIGHT);
            //$this->addError('Начало взвешивания');
            
            //echo 'Считываю ответ от весов. <br />';
            $weight = $Port->read();
            
            //$this->addError('Вес ' . $weight);
            if (empty($weight))
                $weight = $Port->read();
            
            if (empty($weight))
                $weight = $Port->read();
               
            $weight = strtolower($weight);

            // В режиме непрерывной передачи вес передаётся как строка вида wn00003.7kg
            // Отрезаем префиксы ww (брутто), wn (нетто) либо wt (тара) и kg
            $weight = preg_replace(['/ww/', '/wn/', '/wt/', '/kg/'], '', $weight);
            
            //echo 'Закрываю порт.<br />';
            $Port->close();
        }
        else {
            $this->addError('Порт не открылся.');
        }
        
        return floatval($weight);
    }
    
    public function addError($description) {
        $this->errors[] = $description;
    }
    
    public function getErrors() {
        return array_merge($this->errors, $this->Port->getErrors());
    }
}
