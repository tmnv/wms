# СУС, WMS (Система Управления Складом, Warehouse Management System)
Набор программ под разные платформы для управления работой склада.

## Установка необходимых пакетов
Необходимо поставить веб-сервер Apache2, интерпретатор PHP и модуль PHP для Apache2:

    $ sudo apt install apache2 php libapache2-mod-php

## Создание конфигурации Apache2
Перейти в директорию /etc/apache2/sites-available
 
    $ cd /etc/apache2/sites-available

Создать файл wms.conf:

    $ sudo touch wms.conf

Открыть файл и написать конфигурацию для виртуального хоста:

    <VirtualHost *:80>
        ServerName wms
        
        DocumentRoot /PATH/TO/wms/server
        
        <Directory "/PATH/TO/wms/server">
            Options -Indexes +FollowSymLinks
            DirectoryIndex index.php index.html
            AllowOverride All
            Require all granted
            Allow from All
        </Directory>
        
        ErrorLog /var/log/apache2/wms_error.log
        LogLevel warn
        
        CustomLog /var/log/apache2/wms_access.log combined
    </VirtualHost>
    
## Задействовать конфигурационный файл сайта

    $ sudo a2ensite wms
    
## Управление сервером Apache2
Проверка статуса сервера

    $ sudo systemctl status apache2
    
Запуск / перезапуск / остановка сервера

    $ sudo systemctl start apache2
    $ sudo systemctl restart apache2
    $ sudo systemctl stop apache2
    
Перечитывание конфигурации сервера

    $ sudo systemctl reload apache2

## Доменное имя для разработки
Зайти в директорию /etc

    $ cd /etc
    
Открыть файл hosts в редакторе Nano

    $ sudo nano hosts

Указать в нём имя сервера для разработки

    127.0.0.1       wms
    
## Открыть браузер и проверить доступность сервера WMS

    http://wms
    
Проверка вставки цитаты
> Не верьте всему, что пишут в Интернете. В.И. Ленин

